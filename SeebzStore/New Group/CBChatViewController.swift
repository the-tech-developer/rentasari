//
//  CBChatViewController.swift
//  CreativeBeings
//
//  Created by tecH on 24/05/19.
//  Copyright © 2019 tecHangouts. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//import FirebaseAnalytics




class CBChatViewController: GenericViewController {

    @IBOutlet weak var viewSendTxt: UIView!
    @IBOutlet weak var txtMsg: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    
    
}
//
//    var chatFetchIncomming : ChatFetchIncomming? = nil
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        chatFetchApi()
//     //  viewSendTxt.bindToKeyboard()
//        txtMsg.inputAccessoryView = viewSendTxt
//
//        IQKeyboardManager.shared.enable = false
//        IQKeyboardManager.shared.enableAutoToolbar = false
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.enableAutoToolbar = true
//
//    }
//
//    func sendMessageApi(){
//        if self.txtMsg.text == "" || self.txtMsg.text == "Write a message..."{
//            Alert.showSimple("Please write your message")
//            return
//        }
//      // let stremoj = txtMsg.text?.encodeEmoji
////        let parameters  = [
////            "company_user_id": Cookies.userInfo()?.id ?? 0,
////            "message" : txtMsg.text!
////            ] as [String : Any]
////
////        print("parameter",parameters)
////        WebServices.post(url: .send_message_to_super_admin, jsonObject: parameters, method: .post, completionHandler: { (json) in
////
////            if isSuccess(json: json) {
////                self.txtMsg.text = "Write a message..."
////                self.chatFetchApi()
////            }
////
////        }) { (json) in
////            print("Error",json)
////        }
//    }
//
//    func chatFetchApi(){
//
////        let parameters = ["company_user_id": Cookies.userInfo()?.id ?? 0]
////        WebServices.post(url: .message_listing, jsonObject: parameters, method: .post, completionHandler: { (json) in
////
////            if isSuccess(json: json)
////            {
////            //    let dd = LeoSwiftCoder()
////             //   dd.leoClassMake(withName: "ChatFetchIncomming", json: json)
////                self.chatFetchIncomming = ChatFetchIncomming(dict: json)
////              //  self.lblTitle.text = self.chatDetailIncomming?.receiver_details?[0].user_name ?? ""
////                // self.tableView.reloadData()
////                // if self.chatDetailIncomming?.body?.count ?? 0 > 0 {
////                // DispatchQueue.main.async {
////                self.tableView.reloadData()
////                if self.chatFetchIncomming?.body?.count ?? 0 > 0 {
////                    self.tableView.scrollToRow(at: IndexPath(row: (self.chatFetchIncomming?.body?.count ?? 0) - 1, section: 0), at: .bottom, animated: true)
////                }
////
////                //  }
////                //  }
////
////            }
////
////        }) { (json) in
////           // Alert.showSimple(message(json: json))
////            print("Error",json)
////        }
//    }
//
//    @IBAction func actionSend(_ sender: Any) {
//        sendMessageApi()
//    }
//
//
//
//
//
//
//}
//extension UIView{
//    func bindToKeyboard(){
//        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//    }
//
//    func unbindToKeyboard(){
//        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
//    }
//
//    @objc
//    func keyboardWillChange(notification: Notification) {
//
//
//        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
//        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
//        let curFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
//        let deltaY = targetFrame.origin.y - curFrame.origin.y + 50
//
//        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
//            self.frame.origin.y+=deltaY
//
//        },completion: nil)
//
//    }
//}
//
//extension CBChatViewController: UITableViewDataSource , UITableViewDelegate {
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//       return 2//self.chatFetchIncomming?.body?.count ?? 0
//
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
//    {
////        if let new = self.chatFetchIncomming?.body?[indexPath.row]{
////          //  if new.receiver != "\(Cookies.userInfo()?.id ?? 0)"{
////            if new.receiver != Cookies.userInfo()?.id ?? 0{
////                let cell = tableView.dequeueReusableCell(withIdentifier: "SendChatTblCell") as! SendChatTblCell
////                cell.lblMsg.text = new.msg
////
////                return cell
////            }
////            else{
////                let cell = tableView.dequeueReusableCell(withIdentifier: "RecieveChatTblCell") as! RecieveChatTblCell
////
////                cell.lblMsg.text = new.msg//.decodeEmoji
////
////
////                return cell
////            }
////        } else {
//
//        if indexPath.row == 0{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "SendChatTblCell") as! SendChatTblCell
//
//
//
//                   return cell
//
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "RecieveChatTblCell") as! RecieveChatTblCell
//
//
//
//                   return cell
//        }
//
//       // }
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//
//}
//}
//extension String {
//    var encodeEmoji: String{
//        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
//            return encodeStr as String
//        }
//        return self
//    }
//    var decodeEmoji: String{
//        let data = self.data(using: String.Encoding.utf8);
//        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
//        if let str = decodedStr{
//            return str as String
//        }
//        return self
//    }
//}
