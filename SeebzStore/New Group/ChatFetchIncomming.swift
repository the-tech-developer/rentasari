//
//  ChatFetchIncomming.swift
//  CreativeBeings
//
//  Created by tecH on 31/05/19.
//  Copyright © 2019 tecHangouts. All rights reserved.
//

import Foundation
import UIKit

class ChatFetchIncomming {
var serverData : [String: Any] = [:]
var success : Int?
var message : String?
var body : [Body]?
init(dict: [String: Any]){
 self.serverData = dict
 
if let success = dict["success"] as? Int {
 self.success = success
 }
if let message = dict["message"] as? String {
 self.message = message
 }
if let body = dict["body"] as? [[String : Any]] {
self.body = []
for object in body {
let some =  Body(dict: object)
self.body?.append(some)

}
}
}
    
class Body {
var serverData : [String: Any] = [:]
var status : String?
var msg : String?
var created_at : String?
var receiver : Int?
var sender : Int?
var id : Int?
init(dict: [String: Any]){
 self.serverData = dict
 
if let status = dict["status"] as? String {
 self.status = status
 }
if let msg = dict["msg"] as? String {
 self.msg = msg
 }
if let created_at = dict["created_at"] as? String {
 self.created_at = created_at
 }
if let receiver = dict["receiver"] as? Int {
 self.receiver = receiver
 }
if let sender = dict["sender"] as? Int {
 self.sender = sender
 }
if let id = dict["id"] as? Int {
 self.id = id
 }
}
}
}
