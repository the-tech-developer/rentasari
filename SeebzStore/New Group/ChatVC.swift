
import UIKit
import Photos
import CoreLocation
import IQKeyboardManagerSwift
import Nuke
import IBAnimatable
import SocketIO
import Alamofire


class ChatVC: UIViewController,  UINavigationControllerDelegate  {
    
    var comingProduct: HomeScreenDatum?
    
    
    
    var barBottomConstraint: NSLayoutConstraint!
    
    var bottomInset: CGFloat = 0.0
    
    //MARK: Properties
    
    var isFrom = String()
    var timer = Timer()
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    
    var ProductDetailList: HomeScreenProductDetail?
    weak var homeVC: HomeViewController?
    weak var myWishlistVC: MyWishlistVC?
    var categoryList = [CategoryListIncoming.Body]()
    var product : ProductListIncoming.Body!
    var favoriteProduct : FavoriteListIncoming.Body!
    var selectedIndex = Int()
    var productListIncoming = [ProductListIncoming.Body]()
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    @IBOutlet weak var layBottomInputView: NSLayoutConstraint!
    var layBottomInputViewContstant : CGFloat = 0
    
    let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 0
    var canSendLocation = true
    var currentUserTemp : String  = "user1"
    var userId = NSNumber()
    var chatFetchIncomming : ChatFetchIncomming? = nil
    @IBOutlet weak var chatName: UILabel!
    
    var chatId = ""
    var messages = [MessageListIncoming.Body]()
    var chatList : ChatListIncoming.Body?
    var ProductDetailList1 : HomeScreenProductDetail?

    var isScreenComeFrom : ScreenComeFrom = .none

    
    //MARK: Methods
    func customization() {
        layBottomInputViewContstant = layBottomInputView.constant
        let desiredOffset = CGPoint(x: 0, y: -20)
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight
        self.navigationItem.setHidesBackButton(true, animated: false)
        let icon = UIImage.init(named: "back")?.withRenderingMode(.alwaysOriginal)
    }
    
    
    // MARK: ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization()
        self.gettingMessageList()
        let params = ["product_id":chatList?.product_id]
        self.gettingScreenDetail(params:params)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
//        self.gettingMessageList()
        // self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        let notifier = NotificationCenter.default
        notifier.addObserver(self,
                             selector: #selector(ChatVC.showKeyboard(notification:)),
                             name: UIWindow.keyboardWillShowNotification,
                             object: nil)
        notifier.addObserver(self,
                             selector: #selector(ChatVC.hideKeyboard(notification:)),
                             name: UIWindow.keyboardWillHideNotification,
                             object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    
    func gettingScreenDetail(params: [String:Any]) {
        Hud.show(message: "Loading...", view: self.view)
        self.screenDetailApi1(url: .home_screen_product_details, jsonObject: params)
    }
    
    
    
    func screenDetailApi1(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post) {
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                
                if url == .home_screen_product_details {
                    
                    self.ProductDetailList = HomeScreenProductDetail(dict: dict)
                    
//                    if let info = self.ProductDetailList?.body?[0] {
//
//
//                        self.collectionView.reloadData()
//
//                        self.dayTitleLbl.text = info.product_ads?.title.leoSafe()
//                        self.dayAddressLbl.text = info.product_ads?.location.leoSafe()
//                        self.descriptionTextView.text = info.product_ads?.description.leoSafe()
//                        self.dayAMountLbl.text = "$" + "\(info.product_ads?.price.leoSafe() ?? 0)"
//                        self.UserNameLbl.text = info.product_ads?.username.leoSafe().capitalized
//                        self.UserEmailLbl.text = info.product_ads?.email.leoSafe()
//                        self.DateLbl.text = info.product_ads?.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
//                        for cat in self.categoryList {
////                            if cat.cat_id.leoSafe() == self.product.cat_id.leoSafe() {
////                                self.dayTypeLbl.text = cat.name.leoSafe()
////                                break
////                            }
//                        }
//                        if info.product_images?.count == 1{
//                            self.pageControl.isHidden = true
//                            self.rightBtn.isHidden = true
//                            self.leftBtn.isHidden = true
//
//                        }else{
//                            self.pageControl.isHidden = false
//                            self.rightBtn.isHidden = false
//                            self.leftBtn.isHidden = false
//
//                        }
//                        self.pageControl.numberOfPages = (self.ProductDetailList?.body?[0].product_images!.count)!
//
//                        if self.comingProduct?.isFavroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//                        } else {
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        }
//                        if self.comingProduct?.adType.leoSafe() == 1 {
//                            self.pendingDayLbl.isHidden = true
//                            self.daywishLbl.text = "Sell"
//                        } else {
//                            self.pendingDayLbl.isHidden = false
//                            self.pendingDayLbl.text = self.comingProduct?.startDate?.leoDateString(toFormat: "dd/mm")  //"\(self.product.ad_days.leoSafe()) Days"
//                            self.daywishLbl.text = "Rent/day"
//                        }
//
//                    }
                    Singleton.shared.ProductDetailList = self.ProductDetailList
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
    

    // MARK: IBACTIONS
    @IBAction func actionPop(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createAOfferBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "OfferDetailVC") as! OfferDetailVC
//         vc.homeVC = self.homeVC
        //            vc.selectedIndex = indexPath.row
//                    vc.categoryList = self.categoryList
//                    vc.product = self.product
        vc.isScreenComeFrom = .homeVC
        vc.ProductDetailList = self.ProductDetailList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func backToVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ButtonBackClicked(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    //Hides current viewcontroller
    @objc func dismissSelf() {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
         //   self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
         //   self.bottomConstraint.constant = -55
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    @IBAction func showMessage(_ sender: Any) {
       self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
   
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextField.text {
            if text.count != 0 {
                var dict = [String:Any]()
                dict["chat_id"] = chatId
                dict["user_id"] = Cookies.userInfo()!.user_id
                dict["message"] = text
                SocketIOManager.sharedInstance.emitSendMessage(dict: dict)
            } else {
                Alert.showSimple("Please enter message")
            }
        }
    }
  
    @objc func hideKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            //let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = 0
            self.tableView.scrollIndicatorInsets.bottom = 0
            layBottomInputView.constant = 0
        }
    }
    
    @objc func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let height = (frame.cgRectValue.height) //- (-25)
            self.tableView.contentInset.bottom = height
             layBottomInputView.constant = height
   
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.chatFetchIncomming?.body?.count ?? 0 > 0 {
                self.tableView.scrollToRow(at: IndexPath(row: (self.chatFetchIncomming?.body?.count ?? 0) - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    
    // MARK:- SOCKET IMPLEMENTATION
    func checkSocket(completion: @escaping((Bool) -> ())) {
        print(SocketIOManager.sharedInstance.socket?.status as Any)
        if SocketIOManager.sharedInstance.socket != nil {
            if SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.disconnected || SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.connecting {
        
                SocketIOManager.sharedInstance.establishConnection()
                //self.selectedCase.case_id.leoSafe()
                SocketIOManager.sharedInstance.connectedToSocketWithCompletionHandler { (ConnectedResponce) in
                    completion(true)
                }
            } else {
                completion(true)
            }
        }
    }
    
    func emitChatList(chatId: String, senderId: String, receiverId: String, productId: String) {
        var dict = [String:Any]()
        dict["chat_id"] = chatId
        SocketIOManager.sharedInstance.emitChatRoom(dict: dict)
        
        SocketIOManager.sharedInstance.onceChatRoom(chatId: chatId) { (chatResp) in
            print("Chat Resp : \(chatResp)")
            
            Hud.hide(view: self.view)
            if isSuccess(json: chatResp) {
                if let body = MessageListIncoming(dict: chatResp).body {
                    self.messages = body
                    self.tableView.reloadData()
                    self.tableView.scrollToBottom()
                }
            } else {
                Alert.showSimple(message(json: chatResp))
            }
        }
        
        SocketIOManager.sharedInstance.onSendMessage(chatId: chatId) { (sendMsgResp) in
            print("Send Msg Resp : \(sendMsgResp)")
            
            self.inputTextField.text = ""
            Hud.hide(view: self.view)
            if isSuccess(json: sendMsgResp) {
                if let body = MessageListIncoming(dict: sendMsgResp).body,
                    let firstElement = body.first {
                    self.messages.append(firstElement)
                    self.tableView.reloadData()
                    self.tableView.scrollToBottom()
                }
            } else {
                Alert.showSimple(message(json: sendMsgResp))
            }
        }
    }
    
    func gettingMessageList() {
        Hud.show(message: "", view: self.view)
        self.checkSocket { (isConnected) in
            if isConnected {
                
                if self.comingProduct != nil {
                    // Comes from detail product screen
                    
                    var dict = [String:Any]()
                    dict["sender_id"] = Cookies.userInfo()!.user_id
                    dict["receiver_id"] = self.comingProduct?.userID.leoSafe()
                    dict["product_id"] = self.comingProduct?.productID.leoSafe()
                    SocketIOManager.sharedInstance.emitCreateChatId(dict: dict)
                    SocketIOManager.sharedInstance.onCreateChatId(user_id: "") { (chatIdResp) in
                        
                        print("Chat Id Response : \(chatIdResp)")
                        
                        if let body = chatIdResp["body"] as? String {
                            self.chatId = body
                            self.emitChatList(chatId: body, senderId: Cookies.userInfo()!.user_id, receiverId: "\(self.comingProduct!.userID.leoSafe())", productId: "\(self.comingProduct!.productID.leoSafe())")
                        } else {
                            Hud.hide(view: self.view)
                        }
                    }
                    
                    //self.emitChatRoomForGettingChatId()
                } else {
                    // Comes from chat screen
                    guard let chat = self.chatList else { return }
                    self.chatId = chat.chat_id.leoSafe()
                    let senderId = Cookies.userInfo()!.user_id
                    self.emitChatList(chatId: self.chatId, senderId: senderId, receiverId: String(chat.receiver_id.leoSafe()), productId: String(chat.product_id.leoSafe()))
                }
            }
        }
    }
    
    func sendMessage(dict: [String:Any]) {
        self.checkSocket { (isConnected) in
            if isConnected {
                Hud.show(message: "", view: self.view)
                SocketIOManager.sharedInstance.emitSendMessage(dict: dict)
            }
        }
    }
    
}


// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension ChatVC : UITableViewDataSource , UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = self.messages[indexPath.row]
        
        if String(message.user_id.leoSafe()) == Cookies.userInfo()!.user_id {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SendChatTblCell") as! SendChatTblCell
            cell.configure(message: messages[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RecieveChatTblCell") as! RecieveChatTblCell
            cell.configure(message: messages[indexPath.row])
            return cell
        }
    }
}


// MARK:- TEXT FIELD DELEGATE METHODS
extension ChatVC: UITextFieldDelegate {
    
    //MARK: NotificationCenter handlers
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "Write a message..." {
            textField.text = ""
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.layoutIfNeeded()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
}



class RecieveChatTblCell : UITableViewCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var userImgRecieve: UIImageView!

    func configure(message: MessageListIncoming.Body) {
        lblMsg.text = message.message.leoSafe()
        let time = message.created_at.leoSafe().convertUTCDateToLocal(format: "hh:mm a")
        lblTime.text = time
    }
}

class SendChatTblCell : UITableViewCell {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var userImgSend: UIImageView!
    
    func configure(message: MessageListIncoming.Body) {
        lblMsg.text = message.message.leoSafe()
        let time = message.created_at.leoSafe().convertUTCDateToLocal(format: "hh:mm a")
        lblTime.text = time
    }
}
