//
//  MessageViewController.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit
import SocketIO

class MessageViewController: UIViewController, KeyboardHandler {
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var barBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var actionButtons: [UIButton]!
    
    var bottomInset: CGFloat {
        return view.safeAreaInsets.bottom + 50
    }
    
    var selectedProduct: ProductListIncoming.Body?
    var messages = [ThMessagesable]()
    
    
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObservers() {[weak self] state in
            guard state else { return }
            self?.tableView.scroll(to: .bottom, animated: true)
        }
        lblTitle.text = ""
        
        self.gettingMessageList()
    }
    
    
    // MARK:- IBACTIONS
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionFolder(_ sender: UIPhotosButton) {
    }
    
    @IBAction func sendMessagePressed(_ sender: UIButton) {
        inputTextField.resignFirstResponder()
        guard let text = inputTextField.text, !text.isEmpty else { return }
        
        
        let dict = ["chat_id": "", "user_id": Cookies.userInfo()!.user_id, "message": text]
        self.sendMessage(dict: dict)
        
        inputTextField.text = nil
    }
    
    @IBAction func sendImagePressed(_ sender: UIButton) {
        
    }
    
    
    // MARK:- SOCKET IMPLEMENTATION
    func checkSocket(completion: @escaping((Bool) -> ())) {
        print(SocketIOManager.sharedInstance.socket?.status as Any)
        if SocketIOManager.sharedInstance.socket != nil {
            if SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.disconnected || SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.connecting {
        
                SocketIOManager.sharedInstance.establishConnection()
                //self.selectedCase.case_id.leoSafe()
                SocketIOManager.sharedInstance.connectedToSocketWithCompletionHandler { (ConnectedResponce) in
                    completion(true)
                }
            } else {
                completion(true)
            }
        }
    }
    
    func gettingMessageList() {
        self.checkSocket { (isConnected) in
            if isConnected {
                var dict = [String:Any]()
                dict["sender_id"] = Cookies.userInfo()!.user_id
                dict["receiver_id"] = Cookies.userInfo()!.user_id
                dict["product_id"] = Cookies.userInfo()!.user_id
                SocketIOManager.sharedInstance.emitChatRoom(dict: dict)
                
                SocketIOManager.sharedInstance.onceChatRoom(chatId: "") { (chatResp) in
                    
                    print("Chat Resp : \(chatResp)")
                    
                    let leo = LeoSwiftCoder()
                    leo.leoClassMake(withName: "MessageListIncoming", json: chatResp)
                    
                }
                
                SocketIOManager.sharedInstance.onSendMessage(chatId: "") { (sendMsgResp) in
                    
                    print("Send Msg Resp : \(sendMsgResp)")
                    
                    let leo = LeoSwiftCoder()
                    leo.leoClassMake(withName: "MessageListIncoming", json: sendMsgResp)
                    
                }
            }
        }
    }
    
    func sendMessage(dict: [String:Any]) {
        self.checkSocket { (isConnected) in
            if isConnected {
                SocketIOManager.sharedInstance.emitSendMessage(dict: dict)
            }
        }
    }
}


//MARK: UITableView Delegate & DataSource
extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.typeTh == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: message.senderTH == 1 ? "MessageTableViewCell" : "UserMessageTableViewCell") as! MessageTableViewCell
            cell.set(message)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier:  message.senderTH == 1 ? "MessageAttachmentTableViewCell" : "UserMessageAttachmentTableViewCell") as! MessageAttachmentTableViewCell
        cell.delegate = self
        cell.set(message)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /*guard tableView.isDragging else { return }
         cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
         UIView.animate(withDuration: 0.3, animations: {
         cell.transform = CGAffineTransform.identity
         })*/
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK: UItextField Delegate
extension MessageViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
}


//MARK: MessageTableViewCellDelegate Delegate
extension MessageViewController: MessageTableViewCellDelegate {
    
    func messageTableViewCellUpdate() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

