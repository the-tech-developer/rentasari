//
//  MessageTableViewCell.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit
protocol MessageTableViewCellDelegate: class {
    func messageTableViewCellUpdate()
}
protocol ThMessagesable {
    var messageTh : String {get}
    var typeTh: Int {get}
    var senderNameTh: String {get}
    var created_atTh: String {get}
    var user_typeTh: Int {get}
    var idTH: Int {get}
    var senderTH : Int {get}
}
import Nuke
class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var profilePic: UIImageView?
    @IBOutlet weak var messageTextView: UITextView?
    @IBOutlet weak var timeLabel: UILabel!
    func set(_ message: ThMessagesable) {
        messageTextView?.text = message.messageTh
        //        guard let imageView = profilePic else { return }
        //        guard let userID = message.ownerID else { return }
        //        profilePic?.image = #imageLiteral(resourceName: "tick")
        //        ProfileManager.shared.userData(id: userID) { user in
        //            guard let urlString = user?.profilePicLink else { return }
        //            imageView.setImage(url: URL(string: urlString))
        //        }
    }
}

class MessageAttachmentTableViewCell: MessageTableViewCell {
    
    @IBOutlet weak var attachmentImageView: UIImageView!
    @IBOutlet weak var attachmentImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var attachmentImageViewWidthConstraint: NSLayoutConstraint!
    weak var delegate: MessageTableViewCellDelegate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //attachmentImageView.cancelDownload()
        attachmentImageView.image = nil
        //        attachmentImageViewHeightConstraint.constant = 250 / 1.3
        //        attachmentImageViewWidthConstraint.constant = 250
        
        
    }
    
    override func set(_ message: ThMessagesable) {
        super.set(message)
        
        if message.typeTh == 1{
            
        }else   if message.typeTh == 2{
            DispatchQueue.main.async {
                if let url : URL = URL(string: "http://budo.staging.co.in\(message.messageTh)") {
                    Nuke.loadImage(with: url, into: self.attachmentImageView)
                }
                
            }
        }
        
        
        
        //        switch message.typeTh {
        //        case .location:
        //            attachmentImageView.image = UIImage(named: "locationThumbnail")
        //        case .photo:
        //            //guard let urlString = message.profilePicLink else { return }
        //            attachmentImageView.image = message.profilePic
        //            attachmentImageViewHeightConstraint.constant = 250 / 1.3
        //            attachmentImageViewWidthConstraint.constant = 250
        //            delegate?.messageTableViewCellUpdate()
        //            /*attachmentImageView.setImage(url: URL(string: urlString)) {[weak self] image in
        //                guard let image = image, let weakSelf = self else { return }
        //                guard weakSelf.attachmentImageViewHeightConstraint.constant != image.size.height, weakSelf.attachmentImageViewWidthConstraint.constant != image.size.width else { return }
        //                if max(image.size.height, image.size.width) <= 250 {
        //                    weakSelf.attachmentImageViewHeightConstraint.constant = image.size.height
        //                    weakSelf.attachmentImageViewWidthConstraint.constant = image.size.width
        //                    weakSelf.delegate?.messageTableViewCellUpdate()
        //                    return
        //                }
        //                weakSelf.attachmentImageViewWidthConstraint.constant = 250
        //                weakSelf.attachmentImageViewHeightConstraint.constant = image.size.height * (250 / image.size.width)
        //                weakSelf.delegate?.messageTableViewCellUpdate()
        //            }*/
        //        default: break
        //        }
    }
}
