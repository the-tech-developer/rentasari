//
//  CreateViewController.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//


import UIKit
import IBAnimatable
import Nuke

enum AdType {
    case sell
    case rent
}

class CreateADDViewCell: UICollectionViewCell {
    // MARK:- OUTLETS
    @IBOutlet weak var roundView: AnimatableView!
    @IBOutlet weak var ctgImg: UIImageView!
    @IBOutlet weak var ctgLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ctgLbl.translatesAutoresizingMaskIntoConstraints = false
        ctgLbl.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5).isActive = true
        ctgLbl.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5).isActive = true
        ctgLbl.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        ctgLbl.topAnchor.constraint(equalTo: ctgImg.bottomAnchor, constant: 8).isActive = true
        ctgLbl.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    func configure(category: CategoryListIncoming.Body, index: Int) {
        ctgLbl.text = category.name.leoSafe().capitalized
        if index == 0 {
            ctgLbl.isHidden = false
        } else {
            ctgLbl.isHidden = true
        }
        if let url = URL(string: imageBaseUrl + category.cat_img.leoSafe()) {
            Nuke.loadImage(with: url, into: ctgImg)
        }
    }
    
}

class ExtendType: NSObject {
    var name: String?
    var id: Int?
    
    init(name: String, id: Int) {
        self.name = name
        self.id = id
    }
}

extension ExtendType : LeoElementable {
    
    var leoText: String {
        return name!
    }
    
    var leoId: Int {
        return id!
    }
}

class CreateViewController:  UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var sellView: AnimatableView!
    @IBOutlet weak var rentView: AnimatableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var topLbl: UILabel!
    @IBOutlet weak var sellBtn: UIButton!
    @IBOutlet weak var rentBtn: UIButton!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var describeTF: UITextField!
    @IBOutlet weak var priceTF: UITextField!
    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var endDateTF: UITextField!
    @IBOutlet weak var startTimeTF: UITextField!
    @IBOutlet weak var endTimeTF: UITextField!
    @IBOutlet weak var extendDateTF: LeoAnyElementTextField!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var refundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    // MARK:- VARIABLES
    var categoryList = [CategoryListIncoming.Body]()
    var closureDidGetCategoryListing: ((_ categories: [CategoryListIncoming.Body]) -> Void)?
    var adType : AdType = .sell
    var selectedCatId = 0
    var selectedCatName = ""
    var selectedExtendTypeId = 0
    let extendType = [ExtendType(name: "Do you want to allow user to request an extension for rental?", id: 0), ExtendType(name: "Yes", id: 1), ExtendType(name: "No", id: 2)]
    var addAdList : AddAdList!
    var product = [ProductListIncoming.Body]()
    var isScreenComeFrom : ScreenComeFrom = .createAd

    var index = 0
    //MARK:- LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(product)
        topLbl.text = "Create Add"
        self.showSellViewGradient()
        
        // REMOVE FIRST ELEMENT (FIRST ELEMENT : ALL CATEGORY)
        if self.categoryList.count != 0 {
            self.categoryList.remove(at: 0)
            if let first = self.categoryList.first {
                selectedCatId = first.cat_id.leoSafe()
                selectedCatName = first.name.leoSafe()
            }
        }
        
        // SET PRICE & REFUND HEIGHT TO 0 FOR SELL
        self.dateView.isHidden = true
        self.priceView.isHidden = true
        self.refundViewHeightConstraint.constant = 0
        self.priceViewHeightConstraint.constant = 0
        self.contentViewHeightConstraint.constant = 833 - 213
        
        if isScreenComeFrom == .editAd {
            
            
            if product != nil {
                
                if product[index].ad_type == 2 {
                    // Rent
                    self.rentBtnAction(UIButton())
                    startDateTF.text = product[index].start_date!.leoDateString(toFormat: "dd/MM/yyyy")
                    endDateTF.text = product[index].end_date!.leoDateString(toFormat: "dd/MM/yyyy")
                }else{
                    self.sellBtnAction(UIButton())
                }
                
                titleTF.text = product[index].title.leoSafe().capitalized
                describeTF.text = product[index].productDescription.leoSafe()
                priceTF.text = "\(product[index].price.leoSafe())"
                topLbl.text = "Edit Add"
                //
            }
            
        } else if isScreenComeFrom == .createAd {
            
            
        }
        
        extendDateTF.configure(withElements: extendType)
        
        if let tabBarVC = self.navigationController?.topViewController as? TabBarVC {
            tabBarVC.closureDidGetCategoryListing = { categories in
                self.categoryList = categories
                self.collectionView.reloadData()
            }
        }
    }
    
    
    //MARK:- ACTION
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createADDaction(_ sender: UIButton){
        guard let userInfo = Cookies.userInfo() else { return }
        
        if !titleTF!.length() {
            Alert.showSimple("Please enter title.")
            return
        }
        if !describeTF!.length(){
            Alert.showSimple("Please enter description.")
            return
        }
        if !priceTF!.length(){
            Alert.showSimple("Please enter price.")
            return
        }
        
        if isScreenComeFrom == .createAd{
            if adType == .sell {
                
                addAdList = AddAdList(ad_type: "1", title: titleTF.text.leoSafe(), description: describeTF.text.leoSafe(), price: priceTF.text.leoSafe(), days: "", isExtend: "0", images: nil, address: "", latitude: 0, longitude: 0, user_id: userInfo.user_id, cat_id: "\(selectedCatId)", cat_name: selectedCatName, email: userInfo.email, product_id: "")
            } else {
                
                if !startDateTF!.length(){
                    Alert.showSimple("Please enter start date.")
                    return
                }
                if !endDateTF!.length(){
                    Alert.showSimple("Please enter end date.")
                    return
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let startDateStr = dateFormatter.date(from: startDateTF.text.leoSafe())
                let endDateStr = dateFormatter.date(from: endDateTF.text.leoSafe())
                let dateComponentFormatter = DateComponentsFormatter()
                let differenceDate = dateComponentFormatter.difference(from: startDateStr!, to: endDateStr!)
                
                var is_extend : Int!
                for type in extendType {
                    if type.name.leoSafe().lowercased() == extendDateTF.text.leoSafe().lowercased() {
                        is_extend = type.id.leoSafe()
                        break
                    }
                }
                
                addAdList = AddAdList(ad_type: "2", title: titleTF.text.leoSafe(), description: describeTF.text.leoSafe(), price:  priceTF.text.leoSafe(), days: differenceDate!, isExtend: "\(is_extend)", images: nil, address: "", latitude: 0, longitude: 0, user_id: userInfo.user_id, cat_id: "\(selectedCatId)", cat_name: selectedCatName, email: userInfo.email, product_id: "")
            }
        }else{
            
            if adType == .sell {
                
                addAdList = AddAdList(ad_type: "1", title: titleTF.text.leoSafe(), description: describeTF.text.leoSafe(), price: priceTF.text.leoSafe(), days: "", isExtend: "0", images: nil, address: "", latitude: 0, longitude: 0, user_id: userInfo.user_id, cat_id: "\(selectedCatId)", cat_name: selectedCatName, email: userInfo.email, product_id: "")
            } else {
                
                if !startDateTF!.length(){
                    Alert.showSimple("Please enter start date.")
                    return
                }
                if !endDateTF!.length(){
                    Alert.showSimple("Please enter end date.")
                    return
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let startDateStr = dateFormatter.date(from: startDateTF.text.leoSafe())
                let endDateStr = dateFormatter.date(from: endDateTF.text.leoSafe())
                let dateComponentFormatter = DateComponentsFormatter()
                let differenceDate = dateComponentFormatter.difference(from: startDateStr!, to: endDateStr!)
                
                var is_extend : Int!
                for type in extendType {
                    if type.name.leoSafe().lowercased() == extendDateTF.text.leoSafe().lowercased() {
                        is_extend = type.id.leoSafe()
                        break
                    }
                }
                
                addAdList = AddAdList(ad_type: "2", title: titleTF.text.leoSafe(), description: describeTF.text.leoSafe(), price:  priceTF.text.leoSafe(), days: differenceDate!, isExtend: "\(is_extend)", images: nil, address: "", latitude: 0, longitude: 0, user_id: userInfo.user_id, cat_id: "\(selectedCatId)", cat_name: selectedCatName, email: userInfo.email, product_id: "")
            }
        }
        
        
        
        
        let uploadimageVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadimageVC") as! UploadimageVC
        uploadimageVC.addAdList = self.addAdList
        if isScreenComeFrom == .editAd {
            
            uploadimageVC.product = self.product
            uploadimageVC.isScreenComeFrom = .editAd
        }else if  isScreenComeFrom == .createAd {
            uploadimageVC.isScreenComeFrom = .createAd
        }
        navigationController?.pushViewController(uploadimageVC, animated: true)
    }
    
    func showSellViewGradient() {
        self.sellView.opacity = 1.0
        self.rentView.opacity = 0
        self.sellBtn.setTitleColor(.white, for: .normal)
        self.rentBtn.setTitleColor(.black, for: .normal)
    }
    
    func showRentViewGradient() {
        self.rentView.opacity = 1
        self.sellView.opacity = 0
        self.rentBtn.setTitleColor(.white, for: .normal)
        self.sellBtn.setTitleColor(.black, for: .normal)
    }
    
    @IBAction func sellBtnAction(_ sender: UIButton){
        self.adType = .sell
        self.dateView.isHidden = true
        self.priceView.isHidden = true
        self.scrollView.isScrollEnabled = false
        
        self.showSellViewGradient()
        
        self.refundViewHeightConstraint.constant = 0
        self.priceViewHeightConstraint.constant = 0
        self.contentViewHeightConstraint.constant = 833 - 213
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func rentBtnAction(_ sender: UIButton){
        self.adType = .rent
        self.dateView.isHidden = false
        self.priceView.isHidden = false
        self.scrollView.isScrollEnabled = true
        self.showRentViewGradient()
        self.refundViewHeightConstraint.constant = 155
        self.priceViewHeightConstraint.constant = 45
        self.contentViewHeightConstraint.constant = 833
        
        UIView.animate(withDuration: 0.8) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionExtendDate(_ sender: UIButton) {
        extendDateTF.becomeFirstResponder()
    }
}

// MARK:- COLLECTION VIEW DATA SOURCE & DELEGATE METHODS
extension CreateViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreateADDViewCell", for: indexPath) as! CreateADDViewCell
        cell.configure(category: categoryList[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCategory = self.categoryList[indexPath.row]
        self.selectedCatId = selectedCategory.cat_id.leoSafe()
        self.selectedCatName = selectedCategory.name.leoSafe().capitalized
        
        
        // HIDE ALL THE CELL LABELS
        for i in 0..<self.categoryList.count {
            let index = IndexPath(row: i, section: 0)
            if let selectedCell = collectionView.cellForItem(at: index) as? CreateADDViewCell {
                selectedCell.ctgLbl.isHidden = true
            }
        }
        
        // SHOW SELECTED CELL LABEL
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? CreateADDViewCell {
            selectedCell.ctgLbl.isHidden = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.size.width - 20) / 3
        let height = collectionView.frame.size.height - 20
        return CGSize(width: width, height: height)
    }
}
