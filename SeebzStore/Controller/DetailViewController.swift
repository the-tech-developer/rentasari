//
//  DetailViewController.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire

class DetailViewController: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet var topLBl: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var dayTitleLbl: UILabel!
    @IBOutlet var dayTypeLbl: UILabel!
    @IBOutlet var dayAddressLbl: UILabel!
    @IBOutlet var remainingdayLbl: UILabel!
    @IBOutlet var dayAMountLbl: UILabel!
    @IBOutlet var daywishLbl: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var UserNameLbl: UILabel!
    @IBOutlet var UserEmailLbl: UILabel!
    @IBOutlet var btnChat: UIButton!
    @IBOutlet var btnMakeDeal: UIButton!
    @IBOutlet var DateLbl: UILabel!
    @IBOutlet var seeProfileBtn: AnimatableButton!
    @IBOutlet var favoriteButton: UIButton!
    @IBOutlet var pendingDayLbl: UILabel!
    @IBOutlet weak var leftBtn: UIButton!
    @IBOutlet weak var rightBtn: UIButton!
    
    
    // MARK:- VARIABLES
    weak var homeVC: HomeViewController?
    weak var rentProfile: RentProfileVC?
    weak var myWishlistVC: MyWishlistVC?
    weak var detailVC: DetailViewController?
    var categoryList = [CategoryListIncoming.Body]()
    var product : ProductListIncoming.Body!
    var favoriteProduct : FavoriteListIncoming.Body!
    var selectedIndex = Int()
    var productListIncoming = [ProductListIncoming.Body]()
    //  var homeScreenProductDetail = [HomeScreenProductDetail]()
    var ProductDetailList: HomeScreenProductDetail?
    var product_id = Int()
    
    
    var isScreenComeFrom : ScreenComeFrom = .none

    var productDetails: ProductDetail?
    var comingProduct: HomeScreenDatum?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageControl.isHidden = false
//        let params = ["product_id":product_id]
//        self.gettingScreenDetail(params:params )
//
//        if self.product != nil {
//            //
//            let params = ["product_id":product_id]
//            //
//            self.gettingScreenDetail(params:params )
//            //            self.gettingScreenDetail()
//        } else if self.favoriteProduct != nil {
////            let params = ["product_id":product_id]
////            self.gettingScreenDetail(params:params )
//            self.showFavDataOnUI()
//            self.collectionView.reloadData()
//        }
//
        if isScreenComeFrom == .homeVC{
            let params = ["product_id":product_id]
            self.gettingScreenDetail(params:params)
        }else if isScreenComeFrom == .wishList{
            let params = ["product_id":product_id]
            self.gettingScreenDetail(params:params)
            self.showFavDataOnUI()
          //            self.collectionView.reloadData()
        }
        self.collectionView.reloadData()
        
        
        
        //        self.collectionView.reloadData()
    }
//    override func viewWillAppear(_ animated: Bool) {
//        if isScreenComeFrom == .homeVC{
//            let params = ["product_id":product_id]
//            self.gettingScreenDetail(params:params)
//        }else if isScreenComeFrom == .wishList{
//            self.showFavDataOnUI()
//     //   self.collectionView.reloadData()
//        }
//        self.collectionView.reloadData()    }
    
    
    //
    //    private func showDataOnUI() {
    //
    //        if let product = self.product {
    //            dayTitleLbl.text = product.title.leoSafe()
    //            dayAddressLbl.text = product.location.leoSafe()
    ////            descriptionTextView.text = product.description.leoSafe()
    //            dayAMountLbl.text = "$\(product.price.leoSafe())"
    //            UserNameLbl.text = product.username.leoSafe().capitalized
    //            UserEmailLbl.text = product.email.leoSafe()
    //            DateLbl.text = product.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
    //
    ////            for cat in categoryList {
    ////                if cat.cat_id.leoSafe() == product.leoSafe() {
    ////                    dayTypeLbl.text = cat.name.leoSafe()
    ////                    break
    ////                }
    ////            }
    //
    //            if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
    //                favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
    //            } else {
    //                favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
    //            }
    //            if product.ad_type.leoSafe() == 1 {
    //                self.pendingDayLbl.isHidden = true
    //                daywishLbl.text = "Sell"
    //            } else {
    //                self.pendingDayLbl.isHidden = false
    //                self.pendingDayLbl.text = "\(product.ad.leoSafe()) Days"
    //                daywishLbl.text = "Rent/day"
    //            }
    //        }
    //    }
    
    private func showFavDataOnUI() {
        self.collectionView.reloadData()

        if let product = self.favoriteProduct {
            dayTitleLbl.text = product.title.leoSafe()
            dayAddressLbl.text = product.location.leoSafe()
            descriptionTextView.text = product.description.leoSafe()
            dayAMountLbl.text = "$\(product.price.leoSafe())"
            UserNameLbl.text = product.username.leoSafe().capitalized
            UserEmailLbl.text = product.email.leoSafe()
            DateLbl.text = product.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
            
            for cat in categoryList {
                if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                    dayTypeLbl.text = cat.name.leoSafe()
                    break
                }
            }
            
            if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
                favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
            } else {
                favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
            }
            if product.ad_type.leoSafe() == 1 {
                self.pendingDayLbl.isHidden = true
                daywishLbl.text = "Sell"
            } else {
                self.pendingDayLbl.isHidden = false
                self.pendingDayLbl.text =  "\(product.start_date?.leoDateString(toFormat: "dd/MM") )" + "\(product.end_date?.leoDateString(toFormat: "dd/MM"))"   //"\(product.ad_days.leoSafe()) Days"
                daywishLbl.text = "Rent/day"
            }
        }
    }
    
    // MARK:- API IMPLEMENTATION
    func gettingScreenDetail(params: [String:Any]) {
        Hud.show(message: "Loading...", view: self.view)
        self.screenDetailApi(url: .home_screen_product_details, jsonObject: params)
    }
    
    
    
    func screenDetailApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post) {
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                
                if url == .home_screen_product_details {
                    
                    self.ProductDetailList = HomeScreenProductDetail(dict: dict)
                    
                    if let info = self.ProductDetailList?.body?[0] {
                        
                        
                        self.collectionView.reloadData()
                        
                        self.dayTitleLbl.text = info.product_ads?.title.leoSafe()
                        self.dayAddressLbl.text = info.product_ads?.location.leoSafe()
                        self.descriptionTextView.text = info.product_ads?.description.leoSafe()
                        self.dayAMountLbl.text = "$" + "\(info.product_ads?.price.leoSafe() ?? 0)"
                        self.UserNameLbl.text = info.product_ads?.username.leoSafe().capitalized
                        self.UserEmailLbl.text = info.product_ads?.email.leoSafe()
                        self.DateLbl.text = info.product_ads?.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
                        for cat in self.categoryList {
//                            if cat.cat_id.leoSafe() == self.product.cat_id.leoSafe() {
//                                self.dayTypeLbl.text = cat.name.leoSafe()
//                                break
//                            }
                        }
                        if info.product_images?.count == 1{
                            self.pageControl.isHidden = true
                            self.rightBtn.isHidden = true
                            self.leftBtn.isHidden = true
                            
                        }else{
                            self.pageControl.isHidden = false
                            self.rightBtn.isHidden = false
                            self.leftBtn.isHidden = false
                            
                        }
                        self.pageControl.numberOfPages = (self.ProductDetailList?.body?[0].product_images!.count)!
                        
                        if self.comingProduct?.isFavroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
                        } else {
                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
                        }
                        if self.comingProduct?.adType.leoSafe() == 1 {
                            self.pendingDayLbl.isHidden = true
                            self.daywishLbl.text = "Sell"
                        } else {
                            self.pendingDayLbl.isHidden = false
                            self.pendingDayLbl.text = self.comingProduct?.startDate?.leoDateString(toFormat: "dd/mm")  //"\(self.product.ad_days.leoSafe()) Days"
                            self.daywishLbl.text = "Rent/day"
                        }
                        
                    }
                    Singleton.shared.ProductDetailList = self.ProductDetailList
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
    
    
    func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                
                if url == .addFavroite {
                    
                    if self.isScreenComeFrom == .homeVC{
                       if self.comingProduct?.isFavroite == FavoriteStatus.isUnfavorite.rawValue{
                            self.comingProduct?.isFavroite = 1
                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
                        }else {
                            self.comingProduct?.isFavroite = 0
                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
                        }
                        self.homeVC?.tableview.reloadData()
                    }else if self.isScreenComeFrom == .wishList{
                        if self.favoriteProduct?.is_favroite == FavoriteStatus.isUnfavorite.rawValue{
                             self.favoriteProduct?.is_favroite = 1
                             self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
                         }else {
                             self.favoriteProduct?.is_favroite = 0
                             self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
                         }
                        self.myWishlistVC?.tableView.reloadData()
                    }
                    
//                    if let homeVC = self.homeVC {
//                        if homeVC.productListIncoming[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
//                            homeVC.productListIncoming[self.selectedIndex].is_favroite = 1
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        } else {
//                            homeVC.productListIncoming[self.selectedIndex].is_favroite = 0
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        }
//                        homeVC.tableview.reloadData()
//                    }
//                    if let myWishlistVC = self.myWishlistVC {
//                        if myWishlistVC.favoriteList[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
//                            myWishlistVC.favoriteList[self.selectedIndex].is_favroite = 1
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        } else {
//                            myWishlistVC.favoriteList[self.selectedIndex].is_favroite = 0
//                            self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//                        }
//                        myWishlistVC.tableView.reloadData()
//
//                    }
                    
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
    
    //MARK:- ACTION
    @IBAction func swipeLeftAction(_ sender: Any){
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        
        if isScreenComeFrom == .homeVC{
            if nextItem.row < (self.ProductDetailList?.body?[0].product_images![0].product_image?.count)! && nextItem.row >= 0{
                self.collectionView.scrollToItem(at: nextItem, at: .right, animated: true)
                
            }
        }else if isScreenComeFrom == .wishList{
            if nextItem.row < (self.favoriteProduct.product_image?.count)! && nextItem.row >= 0{
                self.collectionView.scrollToItem(at: nextItem, at: .right, animated: true)
                
            }
        }
     
    }
    
    @IBAction func swipeRightAction(_ sender: Any){
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if isScreenComeFrom == .homeVC{
            if nextItem.row < (self.ProductDetailList?.body?[0].product_images![0].product_image?.count)! {
                self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
                
            }
        }else if isScreenComeFrom == .wishList{
            if nextItem.row < (self.favoriteProduct.product_image?.count)! {
                self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)
                
            }
        }
        
    
        
    }
    
    @IBAction func clickFavAction(_ sender: Any){
        guard let userInfo = Cookies.userInfo() else { return }
        
        if isScreenComeFrom == .homeVC{
            var params = [String:Any]()
    //        let info = ProductDetailList?.body
            let info = comingProduct?.isFavroite.leoSafe()
            if self.comingProduct?.isFavroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
                params = ["user_id": userInfo.user_id, "ad_id": self.comingProduct?.adID.leoSafe(), "status": FavoriteStatus.isFavorite.rawValue] as [String : Any]
                self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
            } else {
                params = ["user_id": userInfo.user_id, "ad_id": self.comingProduct?.adID.leoSafe(), "status": FavoriteStatus.isUnfavorite.rawValue] as [String : Any]
                self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
            }
            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
//            let paramss = ["product_id":product_id]
//            self.gettingScreenDetail(params:paramss)
//            self.collectionView.reloadData()

            
        }else if isScreenComeFrom == .wishList{
            var params = [String:Any]()
    //        let info = ProductDetailList?.body
            let info = favoriteProduct?.is_favroite.leoSafe()
            if self.comingProduct?.isFavroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
                params = ["user_id": userInfo.user_id, "ad_id": self.favoriteProduct.ad_id.leoSafe(), "status": FavoriteStatus.isFavorite.rawValue] as [String : Any]
                self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
            } else {
                params = ["user_id": userInfo.user_id, "ad_id": self.favoriteProduct.ad_id.leoSafe(), "status": FavoriteStatus.isUnfavorite.rawValue] as [String : Any]
                self.favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
            }
            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
//            self.showFavDataOnUI()
//            self.collectionView.reloadData()
            
        }
   
        
       
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    @IBAction func btnChat(_ sender: Any) {
    //        let vc = self.storyboard?.instantiateViewController(identifier: "ChatVC") as! ChatVC
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
    
    @IBAction func btnMakeDeal(_ sender: Any) {
        
        
        let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "OfferDetailVC") as! OfferDetailVC
        if isScreenComeFrom == .homeVC{
            tabBarVC.ProductDetailList = self.ProductDetailList
            tabBarVC.isScreenComeFrom = .homeVC
        }else if isScreenComeFrom == .wishList{
            tabBarVC.favoriteProduct = favoriteProduct
            tabBarVC.isScreenComeFrom = .wishList


        }
        self.navigationController?.pushViewController(tabBarVC, animated: true)
    }
    
    @IBAction func shareAction(_ sender: Any) {
        if isScreenComeFrom == .homeVC{
            let adTitle = ProductDetailList?.body?[0].product_ads?.title.leoSafe()
            let activityViewController = UIActivityViewController(activityItems: [adTitle], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }else if isScreenComeFrom == .wishList{
            let adTitle = favoriteProduct.title.leoSafe()
            let activityViewController = UIActivityViewController(activityItems: [adTitle], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            self.present(activityViewController, animated: true, completion: nil)
        }
     
    }
    
    @IBAction func seeProfileAction(_ sender: Any) {
    }
    
    @IBAction func chatAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChatVC") as! ChatVC
        vc.comingProduct = comingProduct
        vc.product = self.product
        vc.ProductDetailList = self.ProductDetailList
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK:- EXTENSION CLASS
extension DetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
   
            return ProductDetailList?.body?[0].product_images?.count ?? 0

        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailCell", for: indexPath) as! DetailCell
        //        cell.pageController
        //        cell.pageControl.numberOfPages = (ProductDetailList?.body?[0].product_images!.count)!
//        if self.ProductDetailList?.body?[0].product_images != nil {
            cell.configure(product: (self.ProductDetailList?.body?[0].product_images![indexPath.row])!)
//        }
//        if self.favoriteProduct != nil {
//            cell.detailBackImg.showImageUsingURL(urlEndPointStr: self.favoriteProduct.product_image.leoSafe())
////            cell.configure(product: (self.ProductDetailList?.body?[0].product_images![indexPath.row])!, favProduct: favoriteProduct)
//        }
        return cell
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.currentPage = indexPath.row
    }
    
    
    
}

class DetailCell: UICollectionViewCell {
    
    @IBOutlet var detailBackImg: UIImageView!
    @IBOutlet var rentRightBtn: UIButton!
    @IBOutlet var rentLeftBtn: UIButton!
    @IBOutlet var rentFavBtn: UIButton!
    @IBOutlet var pageController: UIPageControl!
    
    @IBAction func pageControl(_ sender: Any) {
    }
    var ProductDetailList: HomeScreenProductDetail?
    
    func configure(product: HomeScreenProductDetail.Body.Product_Images, favProduct: FavoriteListIncoming.Body? = nil) {
        //        let productt = product.body?[0]
//        if product != nil {
            detailBackImg.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
//        } else if favProduct != nil {
//            detailBackImg.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
//        }
        
    }
    //    func configure(product: HomeScreenProductDetail.Body? = nil, favProduct: FavoriteListIncoming.Body? = nil) {
    //        if product != nil {
    //            detailBackImg.showImageUsingURL(urlEndPointStr: product?.product_images?[0].product_image.leoSafe() ?? 0)
    //        } else if favProduct != nil {
    //            detailBackImg.showImageUsingURL(urlEndPointStr: favProduct!.product_image.leoSafe())
    //        }
    //
    //    }
}

