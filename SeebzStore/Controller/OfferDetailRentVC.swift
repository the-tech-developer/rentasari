//
//  OfferDetailRentVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class OfferDetailRentVC: UIViewController {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblTop: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var offerImageView: AnimatableImageView!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var offerNameLbl: UILabel!
    @IBOutlet weak var offerRentBuyLbl: UILabel!
    @IBOutlet weak var offerAddressLbl: UILabel!
    weak var homeVC: HomeViewController?
     weak var myWishlistVC: MyWishlistVC?
    var product : ProductListIncoming.Body!
    var favoriteProduct : FavoriteListIncoming.Body!
     var categoryList = [CategoryListIncoming.Body]()
     var selectedIndex = Int()
     var productListIncoming = [ProductListIncoming.Body]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.product != nil {
                  self.showDataOnUI()
              } else if self.favoriteProduct != nil {
                  self.showFavDataOnUI()
              }
    }
    
    
    
    
       private func showDataOnUI() {
           if let product = self.product {
               offerNameLbl.text = product.title.leoSafe()
               offerAddressLbl.text = product.location.leoSafe()
//               descriptionTextView.text = product.description.leoSafe()
               offerRentBuyLbl.text = "$\(product.price.leoSafe())"
//               UserNameLbl.text = product.username.leoSafe().capitalized
//               UserEmailLbl.text = product.email.leoSafe()
//               DateLbl.text = product.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
               self.offerImageView.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
               for cat in categoryList {
                   if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                       typeLbl.text = cat.name.leoSafe()
                       break
                   }
               }
//
//               if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//                   favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//               } else {
//                   favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//               }
//               if product.ad_type.leoSafe() == 1 {
//                   self.pendingDayLbl.isHidden = true
//                   daywishLbl.text = "Sell"
//               } else {
//                   self.pendingDayLbl.isHidden = false
//                   self.pendingDayLbl.text = "\(product.ad_days.leoSafe()) Days"
//                   daywishLbl.text = "Rent/day"
//               }
           }
       }
    private func showFavDataOnUI() {
           if let product = self.favoriteProduct {
               offerNameLbl.text = product.title.leoSafe()
               offerAddressLbl.text = product.location.leoSafe()
//               descriptionTextView.text = product.description.leoSafe()
               offerRentBuyLbl.text = "$\(product.price.leoSafe())"
//               UserNameLbl.text = product.username.leoSafe().capitalized
//               UserEmailLbl.text = product.email.leoSafe()
//               DateLbl.text = product.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
                self.offerImageView.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
               for cat in categoryList {
                   if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                       typeLbl.text = cat.name.leoSafe()
                       break
                   }
               }
//
//               if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//                   favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//               } else {
//                   favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//               }
//               if product.ad_type.leoSafe() == 1 {
//                   self.pendingDayLbl.isHidden = true
//                   daywishLbl.text = "Sell"
//               } else {
//                   self.pendingDayLbl.isHidden = false
//                   self.pendingDayLbl.text = "\(product.ad_days.leoSafe()) Days"
//                   daywishLbl.text = "Rent/day"
//               }
           }
       }
       
    
    @IBAction func btnBack(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNext(_ sender: Any) {
    }
    
}
