//
//  ReturnDetailVC.swift
//  seebzStore
//
//  Created by IOS on 12/02/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class ReturnDetailVC: UIViewController {
    
    @IBOutlet var returnDetailProductImage: AnimatableImageView!
    
    @IBOutlet var returnDetailProductTitle: UILabel!
    @IBOutlet var returnDetailProductAdress: UILabel!
    @IBOutlet var returnDteTextField: UITextField!
    @IBOutlet var returnDetailProductUserName: UILabel!
    @IBOutlet var returnDetailProductCategory: UILabel!
    @IBOutlet var returnDetailProductRentBuy: UILabel!
    @IBOutlet var returnDetailProductPrice: UILabel!
    
    @IBOutlet var returnAdress1Lbl: UILabel!
    @IBOutlet var returnAdress2Lbl: UILabel!
    @IBOutlet var returnAdress3Lbl: UILabel!
    @IBOutlet var returnAdress4Lbl: UILabel!
    @IBOutlet var returnAdress5Lbl: UILabel!
    @IBOutlet var EditBttn: UIButton!
    
    @IBOutlet var RemainingDate: AnimatableButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func EditAction(_ sender: Any) {
    }
    

    @IBAction func extendedDateAction(_ sender: Any) {
    }
    

}
