//
//  RentedProductVC.swift
//  seebzStore
//
//  Created by Deep Baath on 20/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class RentedProductVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet var topLbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
    }
    
    @IBAction func searchAction(_ sender: Any) {
    }
    
}

//MARK:- EXTENSION CLASS
extension RentedProductVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 154
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "rentedCell", for: indexPath) as! rentedCell
         
         return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
}

//MARK:- OUTLETS CELL
class rentedCell: UITableViewCell{
    
    @IBOutlet var rentLeftImg: AnimatableImageView!
    @IBOutlet var rentTitleLbl: UILabel!
    @IBOutlet var rentCostDayLbl: UILabel!
    @IBOutlet var rentTypeLbl: UILabel!
    @IBOutlet var rentAddressLbl: UILabel!
}
    
    
