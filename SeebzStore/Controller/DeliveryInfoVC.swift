//
//  DeliveryInfoVC.swift
//  seebzStore
//
//  Created by Deep Baath on 19/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire

enum AddressComingFrom {
    case fromProfile
    case fromUnknowun
}

class DeliveryInfoVC: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextBtn: UIButton!
    
    var comingFrom : AddressComingFrom = .fromUnknowun
    
    var delivery: DeliveryListIncoming?
    //    var Delivery: DeliveryListIncoming?
    var myDeliveryDetail = [DeliveryListIncoming.Body?]()
    var ProductDetailList: HomeScreenProductDetail?
    var selectedAddressIndex = 0
    @IBOutlet weak var selectDeliveryLabel: UILabel!
    var addressSelectedROundButton : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch comingFrom {
        case .fromProfile:
            nextBtn.isHidden = true
        case .fromUnknowun:
            nextBtn.isHidden = false
        }
        
        if myDeliveryDetail != nil{
            selectDeliveryLabel.isHidden = false
        }else{
            selectDeliveryLabel.isHidden = true
        }
        selectDeliveryLabel.isHidden = true
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.gettingMyDeliveryDetail()
    }
    
    
    
    
    func gettingMyDeliveryDetail() {
        if let userInfo = Cookies.userInfo() {
            Hud.show(message: "Loading...", view: self.view)
            let params = ["user_id": userInfo.user_id]
            self.addAPI(url: .MyAddress, jsonObject: params)
        }
    }
    
    @objc func editBttonTap(sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC")  as! AddAddressVC
        vc.comeFrom = "Edit_Deliveryinfo"
        
        vc.myDeliveryDetail = [myDeliveryDetail[sender.tag]!]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func deleteBttonTap(sender: UIButton){
        showPermissionAlertWithBtn(title: "RentASari", message: "Are you sure want to delete", preferredStyle: .alert, senderTag: sender.tag)
    }
    @objc func roundButtonTap(sender: AnimatableButton){
        
        
    }
    
    
    
    
    
    // MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func NextAction(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardVC")  as! SavedCardVC
        vc.selectedAddressIndex = self.selectedAddressIndex

        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func addNewAddressAction(_ sender: Any) {
        //        self.addAPI()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC")  as! AddAddressVC
        
        
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func deleteApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            if let success = dict["success"] as? Int {
                if success == 200 {
                    self.gettingMyDeliveryDetail()
                    
                }else{
                    Alert.showSimple(message(json: dict))
                }
                //            if isSuccess(json: dict){
                //
                //
                //
                ////                if url == .delete_address{
                ////
                ////                    if let body = DeliveryListIncoming(dict: dict).body {
                ////                        if body.count != 0 {
                ////                            self.myDeliveryDetail = body
                ////                            self.tableView.reloadData()
                ////
                ////                            } else {
                ////
                ////                            }
                ////                        } else {
                ////
                ////                    }
                ////                }
                //
                //            }
                //            else{
                
            }
        }) { (error) in
            
            Hud.hide(view: self.view)
        }
        
    }
    
    
    
    func addAPI(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                
                if url == .MyAddress {
                    
                    
                    
                    if let body = DeliveryListIncoming(dict: dict).body {
                        if body.count != 0 {
                            self.myDeliveryDetail = body
                            self.tableView.reloadData()
                            Singleton.shared.myDeliveryDetail = self.myDeliveryDetail
                        } else {
                            self.myDeliveryDetail = body
                            self.tableView.reloadData()
                        }
                    } else {
                        
                    }
                }
            }else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
            
        }) { (error) in
            
            Hud.hide(view: self.view)
        }
    }
    
    func showPermissionAlertWithBtn(title:String = "",message:String,preferredStyle: UIAlertController.Style? = .alert , senderTag: Int)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            print("Ok button tapped")
            
            let param = ["user_id" : self.myDeliveryDetail[senderTag]!.user_id, "address_id" : self.myDeliveryDetail[senderTag]!.address_id]
            self.deleteApi(url: .delete_address, jsonObject: param as [String : Any])
            
            
        }
        alertController.addAction(OKAction)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped")
            
        }
        alertController.addAction(cancelAction)
        
        // Present Dialog message
        UIApplication.topViewController()?.present(alertController, animated: true, completion:nil)
    }
}

//MARK:- EXTENSION
extension DeliveryInfoVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myDeliveryDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryCell", for: indexPath) as! DeliveryCell
        
        
        cell.configure(myDelivery: myDeliveryDetail[indexPath.row]!)
        cell.editButton.addTarget(self, action: #selector(editBttonTap(sender:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(deleteBttonTap(sender:)), for: .touchUpInside)
        //        cell.deleteBtn.addTarget(self, action: #selector(roundButtonTap(sender:)), for: .editingChanged)
        cell.editButton.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        selectDeliveryLabel.isHidden = true
        
        //        cell.selectRoundButton.tag = indexPath.row
        
        
        
        
        
        
        
        let  Address_Id =  self.myDeliveryDetail[indexPath.row]!.address_id ?? 0
        //                        self.delivery?.body?[indexPath.row].user_id ?? 0
        if self.addressSelectedROundButton.contains(Address_Id){
            
            cell.selectRoundButton.backgroundColor = .black
        }else{
            cell.selectRoundButton.backgroundColor = .white
        }
        
        //        if addressSelectedROundButton.count != 0{
        //            placeOrderBtn.isHidden = false
        //            height_PaymentBtn.constant = 45
        //        }else{
        //            placeOrderBtn.isHidden = true
        //            height_PaymentBtn.constant = 0
        //        }
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = tableView.cellForRow(at: indexPath) {
            if self.myDeliveryDetail.count ?? 0 > 0 {
                
                //  cell.btnRadioSelectedOrNot.addTarget(self, action: #selector(self.tapped_selectedAddressBtn(sender:)), for: .touchUpInside)
                
                //                          cell.btnRadioSelectedOrNot.tag = indexPath.row
                //                         let  Address_Id =   self.customerAddressIncoming?.body?[indexPath.row].id ?? 0
                //                  if self.addressSelectedBtn_Id.contains(Address_Id){
                //
                //                                     cell.btnRadioSelectedOrNot.backgroundColor = .black
                //                      }else{
                //                                  cell.btnRadioSelectedOrNot.backgroundColor = .white
                //                  }
                
                
                
                
            
                self.selectedAddressIndex = indexPath.row
                
                if let exer_Info = myDeliveryDetail[indexPath.row] {
                    
                    let  Address_Id =   exer_Info.address_id
                    
                    
                    //   addressSelectedROundButton.append(Address_Id!)
                    
                    if addressSelectedROundButton.contains(Address_Id ?? 0){
                        
                        addressSelectedROundButton.remove(at: addressSelectedROundButton.firstIndex(of: Address_Id!)!)
                        // cell.btnRadioSelectedOrNot.backgroundColor = .black
                    }else{
                        // cell.btnRadioSelectedOrNot.backgroundColor = .white
                        addressSelectedROundButton.removeAll()
                        addressSelectedROundButton.append(Address_Id!)
                    }
                    self.tableView.reloadData()
                    
                    
                    
                    //                        myDeliveryDetail.removeAll()
                    //                        myDeliveryDetail.append(exer_Info)
                    
                    //                        if exer_Info.isSelected == true {
                    //                            myDeliveryDetail.removeAll()
                    //                            for item in self.myDeliveryDetail {
                    //                                item.isSelected = false
                    //                            }
                    //                            exer_Info.isSelected = false
                    //                        } else {
                    //                            for item in self.myDeliveryDetail {
                    //                                item.isSelected = false
                    //                            }
                    //                            exer_Info.isSelected = true
                    //
                    //                        }
                    //                        print("selectDefault",myDeliveryDetail)
                    //
                    //                    }
                    
                }
                tableView.reloadData()
            }
        }
    }
}




class DeliveryCell: UITableViewCell {
    
    // MARK:- OUTLETS
    
    @IBOutlet weak var houseNumLbl: UILabel!
    @IBOutlet weak var townLbl: UILabel!
    @IBOutlet weak var streetNoLbl: UILabel!
    @IBOutlet weak var areaLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var selectRoundButton: AnimatableButton!
    
    
    
    weak var deliveryVC : DeliveryInfoVC?
    var delivery: DeliveryListIncoming?
    var myDeliveryDetail = [DeliveryListIncoming.Body]()
    
    
    var callBackSelectRoundButton: ((DeliveryListIncoming.Body) -> Void)?
    
    
    func configure(myDelivery: DeliveryListIncoming.Body) {
        self.delivery?.body? = [myDelivery]
        houseNumLbl.text = "\(myDelivery.name.leoSafe())"
        areaLbl.text = "\(myDelivery.city.leoSafe())" + "/" + "\(myDelivery.state.leoSafe())"
        townLbl.text = "\(myDelivery.address_line_1.leoSafe())"
        streetNoLbl.text = "\(myDelivery.address_line_2.leoSafe())"
        countryLbl.text = "\(myDelivery.phone_no.leoSafe())"
        
    }
    
    
    @IBAction func deleteAction(_ sender: UIButton) {
        
        
    }
    
    
    
    
    @IBAction func editAction(_ sender: Any) {
        
        //        let vc = self.storyboard.instantiateViewController(withIdentifier: "AddAddressVC")  as! AddAddressVC
        //
        //            DeliveryInfoVC.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    @IBAction func roundButtonAction(_ sender: Any) {
        //        selectRoundButton.backgroundColor = UIColor.black
        
    }
    
}
