//
//  OfferDetailVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class OfferDetailVC: UIViewController {
    
    
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var sellRentLabel: UILabel!
    @IBOutlet weak var backAction: UIButton!
    @IBOutlet weak var offerAmountTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var dateHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var productImageView: AnimatableImageView!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var dateView: UIView!
    
    weak var tabBarVC: TabBarVC?
    var favoriteProduct : FavoriteListIncoming.Body!

    var ProductDetailList: HomeScreenProductDetail?
    var isScreenComeFrom : ScreenComeFrom = .none

    override func viewDidLoad() {
        super.viewDidLoad()
        dateHeightConstraint.constant = 0
        dateView.isHidden = true
        showDataOnUI()
  }
    
     private func showDataOnUI() {
        if isScreenComeFrom == .homeVC{
            if let product = Singleton.shared.ProductDetailList?.body?[0] {
                
                    productNameLabel.text = product.product_ads?.title.leoSafe()
                    adressLabel.text = product.product_ads?.location.leoSafe()
                    priceLabel.text = "$\(product.product_ads?.price.leoSafe() ?? 0)"
                    contactNameLabel.text = product.product_ads?.username.leoSafe().capitalized
                    categoryLabel.text = product.product_ads?.category_name.leoSafe()
                productImageView.showImageUsingURL(urlEndPointStr: product.product_images?[0].product_image ?? "")
                if product.product_ads?.ad_type.leoSafe() == 1 {
                    
                    dateHeightConstraint.constant = 0
                    dateView.isHidden = true
                                       self.sellRentLabel.text = "Sell"
                                              } else {
                    dateHeightConstraint.constant = 155
                    dateView.isHidden = false
    //                let dateFormatter = DateFormatter()
    //                           dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    //                           dateFormatter.dateFormat = "yyyy-MM-dd"
    //                           let startDateStr = dateFormatter.date(from: startDateTextField.text.leoSafe())
    //                           let endDateStr = dateFormatter.date(from: endDateTextField.text.leoSafe())
    //                           let dateComponentFormatter = DateComponentsFormatter()
    //                           let differenceDate = dateComponentFormatter.difference(from: startDateStr!, to: endDateStr!)
                                         
                                       self.sellRentLabel.text = "Rent/day"
                                              }
                    
            }
        }else if isScreenComeFrom == .wishList{
            if let product = self.favoriteProduct {
                
                    productNameLabel.text = product.title.leoSafe()
                    adressLabel.text = product.location.leoSafe()
                    priceLabel.text = "$\(product.price.leoSafe() ?? 0)"
                    contactNameLabel.text = product.username.leoSafe().capitalized
                if product.cat_id == 1{
                    categoryLabel.text = "Cloths"

                }else if product.cat_id == 2{
                    categoryLabel.text = "Shoes"

                }else if product.cat_id == 3{
                    categoryLabel.text = "Jewellery"

                }
                productImageView.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
                if product.ad_type.leoSafe() == 1 {
                    
                    dateHeightConstraint.constant = 0
                    dateView.isHidden = true
                                       self.sellRentLabel.text = "Sell"
                                              } else {
                    dateHeightConstraint.constant = 155
                    dateView.isHidden = false
    //                let dateFormatter = DateFormatter()
    //                           dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    //                           dateFormatter.dateFormat = "yyyy-MM-dd"
    //                           let startDateStr = dateFormatter.date(from: startDateTextField.text.leoSafe())
    //                           let endDateStr = dateFormatter.date(from: endDateTextField.text.leoSafe())
    //                           let dateComponentFormatter = DateComponentsFormatter()
    //                           let differenceDate = dateComponentFormatter.difference(from: startDateStr!, to: endDateStr!)
                                         
                                       self.sellRentLabel.text = "Rent/day"
                                              }
                    
            }
        }

    }
    
    
    
    
    
    
     func offerCreateApi(){
        var params = [String: Any]()
        
        if isScreenComeFrom == .homeVC{
            guard let product = Singleton.shared.ProductDetailList?.body?[0] else { return }

            
              params["user_id"] = Cookies.userInfo()?.user_id
              params["selling_user_id"] = product.product_ads?.user_id
              params["product_id"] = product.product_ads?.product_id.leoSafe()
              params["ad_type"] = product.product_ads?.ad_type.leoSafe()
              params["amount"] = offerAmountTextField.text.leoSafe()
              params["description"] = descriptionTextField.text.leoSafe()
//              params["offer_description"] = descriptionTextField.text.leoSafe()
              params["start_date"] = product.product_ads?.start_date.leoSafe()
              params["end_date"] = product.product_ads?.end_date.leoSafe()
        }else if isScreenComeFrom == .wishList{
            guard let product = self.favoriteProduct else { return }

            
              params["user_id"] = Cookies.userInfo()?.user_id
              params["selling_user_id"] = product.user_id
              params["product_id"] = product.product_id.leoSafe()
              params["ad_type"] = product.ad_type.leoSafe()
              params["amount"] = offerAmountTextField.text.leoSafe()
              params["description"] = descriptionTextField.text.leoSafe()
//              params["offer_description"] = descriptionTextField.text.leoSafe()
              params["start_date"] = product.start_date.leoSafe()
              params["end_date"] = product.end_date.leoSafe()
        }
       
        
             Hud.show(message: "Loading...", view: self.view)
            WebServices.post(url: .Create_offer, jsonObject: params, method: .post, completionHandler: { (dict) in
                print("Response")
                  Hud.hide(view: self.view)
                
                    if let success = dict["success"] as? Int {
                        if success == 200 {
                            
                let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                                    
                            
                self.navigationController?.pushViewController(tabBarVC, animated: true)
    //
                        }
                    } else
                {
                    Alert.showSimple(message(json: dict))
                }
            
            }) { (error) in
                  Hud.hide(view: self.view)
                
            }
            
        }
    
    
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func nextAction(_ sender: Any) {
        self.offerCreateApi()
//
        
    }
    
    
}

   
