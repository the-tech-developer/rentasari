//
//  RentProfileVC.swift
//  seebzStore
//
//  Created by Deep Baath on 20/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire
import DropDown

class RentProfileVC: UIViewController {

    //MARK:- OUTLETS
    @IBOutlet var userImg: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userNAmeLbl: UILabel!
    @IBOutlet var userDateLbl: UILabel!
    @IBOutlet var userTextView: UITextView!
    @IBOutlet var addTypeLbl: UILabel!
    @IBOutlet var tableVIew: UITableView!
    @IBOutlet var moreBttn: UIButton!
    
    var dropDown = DropDown()
    var ProductDetailList: HomeScreenProductDetail?
    var categoryList = [CategoryListIncoming.Body]()
    var productListIncoming = [ProductListIncoming.Body]()
    var notificationProductDetail: NotificationInnerProductDetailModel?
    var purchasingUserDetail: PurchasingUserDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let params = ["purchase_user_id": notificationProductDetail?.body?[0].product_ads!.booking_by]
        self.gettingPurchasingUserDetail(params: params as [String : Any])
        
        
        

                dropDown.anchorView = moreBttn
                dropDown.dataSource = ["Share Profile", "Report"]
                dropDown.width = 200
                dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                   print("Selected item: \(item) at index: \(index)")
                    
                    
                    
                    
                    
                    if index == 0 {
                        //Edit
                        self.dropDown.show()
                        let adTitle = self.purchasingUserDetail?.body?[0].user_Detail
                                    let activityViewController = UIActivityViewController(activityItems: [adTitle], applicationActivities: nil)
                                    activityViewController.popoverPresentationController?.sourceView = self.view
                                    self.present(activityViewController, animated: true, completion: nil)
                   }
                    else{
                        print(index)
                 //       self.myAdsVC?.showPermissionAlertWithBtn(title: "RentASari", message: "Are you sure want to delete", preferredStyle: .alert, senderTag: delete_indexPath)
                    }
                }

    }
    
    
    
     // MARK:- API IMPLEMENTATION
        
        func gettingPurchasingUserDetail(params: [String:Any]) {
            Hud.show(message: "Loading...", view: self.view)
            self.purchasingUserDetailApi(url: .see_Profile_Of_Purchasing_User, jsonObject: params)

        }
            
            
            
        func purchasingUserDetailApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post) {

                WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
                         print(dict)
                         Hud.hide(view: self.view)
                         
                         if isSuccess(json: dict) {
                             // SUCCESS
                             
                             if url == .see_Profile_Of_Purchasing_User {
                                
                                
//                                   let leo = LeoSwiftCoder()
//                                   leo.leoClassMake(withName: "PurchasingUserDetailModel", json: dict)
                                
                                self.purchasingUserDetail =  PurchasingUserDetailModel(dict: dict)
//
                       if let info = self.purchasingUserDetail?.body?[0] {


                        self.tableVIew.reloadData()

//                         self.dayAMountLbl.text = "$" + "\(info.product_ads?.real_price.leoSafe() ?? 0)"
                        self.userName.text = info.user_Detail?.username.leoSafe().capitalized
                        self.userNAmeLbl.text = info.user_Detail?.email.leoSafe()
                        self.userDateLbl.text = info.user_Detail?.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
                        self.userTextView.text = info.user_Detail?.description.leoSafe()
//
//                        for cat in self.categoryList {
//                            if cat.cat_id.leoSafe() == self.product.cat_id.leoSafe() {
//                                self.dayTypeLbl.text = cat.name.leoSafe()
//                                           break
//                                       }
//                                   }
//    //
//    //                    if self.product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//    //                        self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//    //                               } else {
//    //                        self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//    //                               }
//                        if info.product_ads?.ad_type.leoSafe() == 1 {
//                                       self.remainingdayLbl.isHidden = true
//                            self.dayRentLbl.text = "Sell"
//                                   } else {
//                                       self.remainingdayLbl.isHidden = false
//    //                        self.remainingdayLbl.text = "\(info.product_ads?.ad_days.leoSafe()) Days"
//                            self.dayRentLbl.text = "Rent/day"
//                                   }
//
//                        if info.product_images?.count == 1{
//                            self.pageControl.isHidden = true
//                            self.rentRightBtn.isHidden = true
//                            self.rentLeftBtn.isHidden = true
//
//
//                        }else{
//                            self.pageControl.isHidden = false
//                            self.rentRightBtn.isHidden = false
//                            self.rentLeftBtn.isHidden = false
//                        }
//                        self.pageControl.numberOfPages = (self.notificationProductDetail?.body?[0].product_images!.count)!

           }
                             }
                         }
                         else {
                             // FAILURE
                             Alert.showSimple(message(json: dict))
                         }
                     }) { (errorJson) in
                         Hud.hide(view: self.view)
                         print(errorJson)
                     }
            }
        
    
    
    
    //MARK:- ACTION
    @IBAction func backACtion(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickDotAction(_ sender: Any) {
        self.dropDown.show()
   
    }
}
    
  //MARK:- EXTENSION CLASS

extension RentProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return purchasingUserDetail?.body?[0].product_Detail?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "rentedProfileCell", for: indexPath) as! rentedProfileCell
        cell.configure(userDetail: (purchasingUserDetail?.body?[0].product_Detail![indexPath.row])!)
         
         return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailVC.rentProfile = self
        detailVC.selectedIndex = indexPath.row
        detailVC.categoryList = self.categoryList
        detailVC.ProductDetailList = self.ProductDetailList
        detailVC.product = self.productListIncoming[indexPath.row]
        detailVC.product_id =  productListIncoming[indexPath.row].product_id ?? 0
       self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

class rentedProfileCell: UITableViewCell{
    
   //MARK:- OUTLETS
    @IBOutlet var userProductImg: AnimatableImageView!
    @IBOutlet var userProductTitleLbl: UILabel!
    @IBOutlet var userNameCLbl: UILabel!
    @IBOutlet var userproTypeLbl: UILabel!
    @IBOutlet var userLocationLBl: UILabel!
    @IBOutlet var userPriceLbl: UILabel!
    @IBOutlet var userSellRentLbl: UILabel!
    @IBOutlet var userFavBtn: UIButton!
    
    
    
    
    
    
    func configure(userDetail: PurchasingUserDetailModel.Body.Product_Detail) {
        
        userProductTitleLbl.text = userDetail.title.leoSafe().firstUppercased
        userLocationLBl.text = userDetail.location.leoSafe()
        userPriceLbl.text = "$\(userDetail.price.leoSafe())"
        userNameCLbl.text = userDetail.username.leoSafe()
        userproTypeLbl.text = userDetail.name.leoSafe()
       
           
           
        self.userProductImg.showImageUsingURL(urlEndPointStr: (userDetail.product_image.leoSafe()))
           
//           if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//               favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//           } else {
//               favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//           }
        if userDetail.ad_type.leoSafe() == 1 {
               userSellRentLbl.text = "Buy"
           } else {
               userSellRentLbl.text = "Rent"
           }
           
//           for cat in categoryList {
//               if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
//                   CtgName.text = cat.name.leoSafe()
//                   break
//               }
//           }
       }
}
