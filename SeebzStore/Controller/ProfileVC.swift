//
//  ProfileVC.swift
//  seebzStore
//
//  Created by IOS on 13/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire


class tableviewcell: UITableViewCell {
    //MARK:- OUTLETS
    
    @IBOutlet weak var prfImg: UIImageView!
    @IBOutlet weak var prfLbl: UILabel!
}

class ProfileVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var nameLable: UILabel!
    
    
    
    let profilelbl = ["AboutMe", "Order History", "Saved Card", "Delivery Information", "Help", "Reset Password"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        guard let userInfo = Cookies.userInfo() else { return }
        nameLable.text = ("\(userInfo.firstaname.capitalized)") //+ " " + "\(userInfo.lastname.capitalized)")
    }
    
    //MARK:- ACTION
    @IBAction func actionLogout(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "Ok", style: .default) { (_) in
            Cookies.removeUserInfo()
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(actionOk)
        alert.addAction(actionCancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK:- API IMPLEMENTATION
    func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                if url == .Reset_password {
                    Alert.showSimple("Password reset successfully") {
                    }
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
}



// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profilelbl.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! tableviewcell
        //        cell.prfImg.image = Images[indexPath.row]
        cell.prfLbl.text = profilelbl[indexPath.row]
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AboutMeVC") as! AboutMeVC
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryVC") as! OrderHistoryVC
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
            //        else if indexPath.row == 2{
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            //
            //            self.navigationController?.pushViewController(vc, animated: true)
            //
            //        }
        else if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardVC") as! SavedCardVC
            vc.comingFromCard = .fromProfile
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 3{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryInfoVC") as! DeliveryInfoVC
            vc.comingFrom = .fromProfile
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else if indexPath.row == 5{
            self.showResetPwdAlert()

//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }
//        else if indexPath.row == 6{
//            self.showResetPwdAlert()
//        }
        
    }
    
    func showResetPwdAlert() {
        let alertController = UIAlertController(title: "Reset Password", message: "", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField { (firstTextField : UITextField!) -> Void in
            firstTextField.placeholder = "Old Password"
            firstTextField.isSecureTextEntry = true
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "New Password"
            textField.isSecureTextEntry = true
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Confirm Password"
            textField.isSecureTextEntry = true
            
        }
        let saveAction = UIAlertAction(title: "Submit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            
            let firstTextField = alertController.textFields![0]
            let secondTextField = alertController.textFields![1]
            let thirdTextField = alertController.textFields![2]
            
            if !firstTextField.length() {
                Alert.showSimple("Please enter old password.") {
                    self.showResetPwdAlert()
                }
                return
            }
            if !secondTextField.length() {
                Alert.showSimple("Please enter new password.") {
                    self.showResetPwdAlert()
                }
                return
            }
            if !thirdTextField.length() {
                Alert.showSimple("Please enter confirm password.") {
                    self.showResetPwdAlert()
                }
                return
            }
            if secondTextField.text.leoSafe() != thirdTextField.text.leoSafe() {
                Alert.showSimple("New and confirm password doesn't matched.") {
                    self.showResetPwdAlert()
                }
                return
            }
            
            let params = ["user_id": Cookies.userInfo()!.user_id, "old_pwd": firstTextField.text.leoSafe(), "new_pwd": secondTextField.text.leoSafe()]
            self.apiCalled(url: .Reset_password, jsonObject: params, method: .post)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
