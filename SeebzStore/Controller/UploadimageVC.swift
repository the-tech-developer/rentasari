//
//  UploadimageVC.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Nuke

class CollectionViewCell: UICollectionViewCell{
    
    //MARK:- OUTLETS
    @IBOutlet weak var anitView: AnimatableView!
    @IBOutlet weak var UploadImg: AnimatableImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var uploadImageBtn: UIPhotosButton!
    var product = [ProductListIncoming.Body]()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
  
class CollectionViewCell2: UICollectionViewCell {
    //MARK:- OUTLETS COLLECTION VIEW
    @IBOutlet weak var AnitView: AnimatableView!
    @IBOutlet weak var uploadImg: UIImageView!
    @IBOutlet weak var DelBtn: UIButton!
    @IBOutlet weak var ExpandLbl: UIButton!
    
    func configure(image: UIImage) {
        uploadImg.image = image
    }
}

class UploadimageVC: UIViewController {
    
    // MARK:- OUTLETS
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK:- VARIABLES
    var addAdList : AddAdList!
    var selectedImages = [UIImage]()
    var product = [ProductListIncoming.Body]()
    var isScreenComeFrom : ScreenComeFrom = .none

   
    // MARK:- LIFE CYCLE METHDOS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.reloadData()

    }
    
    // MARK:- IBACTIONS
    @IBAction func actionBack(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
     @IBAction func btnNext(_ sender : UIButton) {
        if isScreenComeFrom == .createAd{
            if selectedImages.count != 0 {
                     let locationVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
                locationVC.isScreenComeFrom = .createAd
                     addAdList.images = selectedImages
                     locationVC.addAdList = addAdList
                     self.navigationController?.pushViewController(locationVC, animated: true)
                 } else {
                     
                     Alert.showSimple("Please select atleast one image.")
                 }
        }else if isScreenComeFrom == .editAd{
            
                let locationVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
            locationVC.isScreenComeFrom = .editAd
                addAdList.images = selectedImages
//            addAdList.address =
            locationVC.product = self.product
                locationVC.addAdList = addAdList
                self.navigationController?.pushViewController(locationVC, animated: true)
            
        }
        
     
    }
    
    // MARK:- CORE FUNCTIONALITIES
   
}
      
// MARK:- COLLECTION VIEW DATA SOURCE & DELEGATE METHODS
extension UploadimageVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.collectionView.frame.width - 20) / 3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isScreenComeFrom == .editAd {
            if selectedImages.count == 0 {
                      return (self.product[0].product_image?.count ?? 0 + 1)

            }else{
                return (self.selectedImages.count + 1)

            }
//            if product[0].product_image != nil {
//            }
        } else if isScreenComeFrom == .createAd  {
            if selectedImages.count != 0 {
                return (self.selectedImages.count + 1)
            }
        }
           return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as!
            CollectionViewCell
            
            if isScreenComeFrom == .editAd {
                cell.product = self.product
                cell.uploadImageBtn.closureDidFinishPickingAnUIImage = { image in
                    if let image = image {
                        self.selectedImages.append(image)
                        self.collectionView.reloadData()
                    }
                }
               //cell.UploadImg.showImageUsingURL(urlEndPointStr: self.prod)
               
            } else if isScreenComeFrom == .createAd {
                
                cell.uploadImageBtn.closureDidFinishPickingAnUIImage = { image in
                    if let image = image {
                        self.selectedImages.append(image)
                        self.collectionView.reloadData()
                    }
                }
            }
            return cell
        } else {
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell2", for: indexPath) as! CollectionViewCell2
            
            if isScreenComeFrom == .editAd {
                
                if selectedImages.count == 0{
                    if let imageUrl = URL(string: imageBaseUrl + "\(product[0].product_image.leoSafe())") {
                        Nuke.loadImage(with: imageUrl, into: cell2.uploadImg)
                    }
                }else{
                    if let imageUrl = URL(string: imageBaseUrl + "\(product[0].product_image.leoSafe())") {
                        Nuke.loadImage(with: imageUrl, into: cell2.uploadImg)
                    }
                        cell2.DelBtn.tag = indexPath.row - 1
                        cell2.DelBtn.addTarget(self, action: #selector(deleteBtnTapped(button:)), for: .touchUpInside)
                        cell2.configure(image: selectedImages[indexPath.row - 1])
                }
                
              
                    
                        
                        
//
//                    if selectedImagesFromGallary.count != 0{
//                                //   cell2.UploadImg.image = selectedImagesFromGallary[indexPath.row]
//                        cell2.uploadImg.image = selectedImagesFromGallary[indexPath.row]

//                                   }
                    
//                }
            } else if isScreenComeFrom == .createAd{
                
        
            cell2.DelBtn.tag = indexPath.row - 1
            cell2.DelBtn.addTarget(self, action: #selector(deleteBtnTapped(button:)), for: .touchUpInside)
            cell2.configure(image: selectedImages[indexPath.row - 1])
//                if selectedImagesFromGallary.count != 0{
//                                              //   cell2.UploadImg.image = selectedImagesFromGallary[indexPath.row]
//                                      cell2.uploadImg.image = selectedImagesFromGallary[indexPath.row]
//
//                                                 }
        }
             return cell2
        }
        
    }
    
    @objc func deleteBtnTapped(button: UIButton) {
        let index = button.tag
        selectedImages.remove(at: index)
        self.collectionView.reloadData()
    }
}




