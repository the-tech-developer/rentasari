//
//  ShipFromViewController.swift
//  seebzStore
//
//  Created by MAC on 23/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import DropDown

class ShipFromViewController: UIViewController {
    
    @IBOutlet weak var enterName: SkyFloatingLabelTextField!
    @IBOutlet var enterPhoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet var enterCompanyName: SkyFloatingLabelTextField!
    @IBOutlet var enterAddressLineOne: SkyFloatingLabelTextField!
    @IBOutlet var enterAddressLineTwo: SkyFloatingLabelTextField!
    @IBOutlet var enterCity: SkyFloatingLabelTextField!
    @IBOutlet var enterState: SkyFloatingLabelTextField!
    @IBOutlet var enterPostalCode: SkyFloatingLabelTextField!
    @IBOutlet var enterCountryCode: SkyFloatingLabelTextField!
    @IBOutlet var nextButton: AnimatableButton!
    @IBOutlet var selectCountryCodeBtn: UIButton!
    
    var shipFrom: Ship?
    let dropDown = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enterState.text = "ON"
        enterPostalCode.text = "M4B 2Y6"
        enterCity.text = "Toronto"
        self.navigationController?.isNavigationBarHidden = true
        nextButton.addTarget(self, action: #selector(tappedNextButton), for: .touchUpInside)
        selectCountryCodeBtn.addTarget(self, action: #selector(tappedSelectCountryCodeBtn), for: .touchUpInside)
    }
    
    @objc
    func tappedSelectCountryCodeBtn(_ sender: UIButton) {
     /*   dropDown.dataSource = IsoCountries.allCountries.map({$0.name})
        dropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.enterCountryCode.text = IsoCountries.allCountries[index].alpha2
            self.dropDown.hide()
        }
        
        dropDown.dismissMode = .manual
        dropDown.width = self.view.frame.width / 1.5
        dropDown.show()
 */
        self.enterCountryCode.text = "US"
    }
    
    @objc
    func tappedNextButton(_ sender: UIButton) {
        if (enterName.text ?? "") != "" {
            
            if (enterPhoneNumberTextField.text ?? "") != "" {
                
                if (enterCompanyName.text ?? "") != ""  {
                    
                    if (enterAddressLineOne.text ?? "") != "" {
                    
                        if (enterCity.text ?? "") != "" {
                            
                            if (enterState.text ?? "") != "" {
                                
                                if (enterPostalCode.text ?? "") != "" {
                                    
                                    if (enterCountryCode.text ?? "") != "" {
                                        
                                        
                                        shipFrom = Ship(companyName: "Demo", name: "chandan", phone: "1111111111", addressLine1: "ASDASD", stateProvince: (enterState.text ?? ""), postalCode: (enterPostalCode.text ?? ""), countryCode: "US", addressResidentialIndicator: "no", cityLocality: (enterCity.text ?? ""))
                                        
                                        let vc = self.storyboard?.instantiateViewController(identifier: "ShipToViewController") as! ShipToViewController
                                        
                                        //vc.serviceCode = self.serviceCode
                                        vc.shipFrom = self.shipFrom
                                        
                                        self.navigationController?.pushViewController(vc, animated: true)
                                        
                                    } else {
                                        self.showDefaultAlert(Message: "Please enter the country code.")
                                    }
                                    
                                } else {
                                    self.showDefaultAlert(Message: "Please enter postal code.")
                                }
                                
                            } else {
                                self.showDefaultAlert(Message: "Please enter the state name.")
                            }
                            
                        } else {
                            self.showDefaultAlert(Message: "Please enter the city name.")
                        }
                        
                        
                    } else {
                        self.showDefaultAlert(Message: "Please enter the address")
                    }
                    
                } else {
                    self.showDefaultAlert(Message: "Please enter the company name.")
                }
                
            } else {
                self.showDefaultAlert(Message: "Please enter the mobile number.")
            }
            
        } else {
            self.showDefaultAlert(Message: "Please enter the name.")
        }
    }
}
