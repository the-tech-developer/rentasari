//
//  PackageWeightViewController.swift
//  seebzStore
//
//  Created by MAC on 23/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import DropDown

class PackageWeightViewController: UIViewController {
    
    @IBOutlet weak var enterWeightTextField: SkyFloatingLabelTextField!
    @IBOutlet var enterWeightUnitsTextField: SkyFloatingLabelTextField!
    @IBOutlet var weightUnitsButton: UIButton!
    @IBOutlet var nextButton: AnimatableButton!
    
    //dimensions
    @IBOutlet var enterUnits: SkyFloatingLabelTextField!
    @IBOutlet var enterLengths: SkyFloatingLabelTextField!
    @IBOutlet var enterWidth: SkyFloatingLabelTextField!
    @IBOutlet var enterHeight: SkyFloatingLabelTextField!
    
    //Customs
    @IBOutlet var contentInBoxlabel: SkyFloatingLabelTextField!
    @IBOutlet var valueOFBoxlabel: SkyFloatingLabelTextField!
    @IBOutlet var descriptionOfBoxLabel: SkyFloatingLabelTextField!
    @IBOutlet var countryOfOriginLabel: SkyFloatingLabelTextField!
    @IBOutlet var quantityOfBoxesLabel: SkyFloatingLabelTextField!
    
    @IBOutlet var harmonizedTrafficCodeLabel: SkyFloatingLabelTextField!
    let dropDown = DropDown()
    var weight: Weight?
    var shipTo: Ship?
    var serviceCode: String = ""
    var shipFrom: Ship?
    var packageCode: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        weightUnitsButton.addTarget(self, action: #selector(tappedWeightUnitButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(tappednextButton), for: .touchUpInside)
    }
    
    @objc
    func tappedWeightUnitButton(_ sender: UIButton) {
        dropDown.dataSource = ["ounce" , "pound" , "kilogram" , "gram"]
        dropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.enterWeightUnitsTextField.text = item
        }
        
        dropDown.width = self.view.frame.width / 1.5
        dropDown.show()
    }
    
    @objc
    func tappednextButton(_ sender: UIButton) {
        if (enterWeightTextField.text ?? "") != "" {
            
            if (enterWeightUnitsTextField.text ?? "") != "" {
                
                let dimesion = Dimensions(unit: (enterUnits.text ?? ""),
                                          length: Int((enterLengths.text ?? "")) ?? 0,
                                          width: Int((enterWidth.text ?? "")) ?? 0,
                                          height: Int((enterHeight.text ?? "")) ?? 0)
                
                weight = Weight(value: Int(enterWeightTextField.text ?? ""), unit: (enterWeightUnitsTextField.text ?? ""), dimensions: dimesion)
               
                let customItems = CustomsItem(customsItemDescription: (descriptionOfBoxLabel.text ?? ""),
                                              quantity: 1,
                                              value: Int((valueOFBoxlabel.text ?? "")) ?? 0,
                                              harmonizedTariffCode: "1223.2",
                                              countryOfOrigin: "CA")
                
                let customes = Customs(contents: (contentInBoxlabel.text ?? ""),
                                       customsItems: [customItems],
                                       nonDelivery: "treat_as_abandoned")
                
                let shiiping = FinalShipment(shipment: Shipment(serviceCode: self.serviceCode , shipFrom: self.shipFrom ?? nil, shipTo: self.shipTo ?? nil, customs: customes , packages: [PackageWeight(packageCode: self.packageCode, weight: weight, dimensions: dimesion)]))
                
                print(shiiping)
                
                var param : [String: Any] = [:]
                do {
                    param = try JSONSerialization.jsonObject(with: JSONEncoder().encode(shiiping), options: []) as! [String:Any]
                } catch {
                    print(error.localizedDescription)
                }
                Hud.show(message: "Loading..", view: self.view)
                WebServices.hitFedExAPis(withShippingApiType: .label, methodType: .post, parameters: param, completionHandler: { (response) in
                    Hud.hide(view: self.view)
                    if let errorMessage = response["errors"] as? [[String: Any]] {
                        self.showDefaultAlert(Message: (errorMessage.first?["message"] as? String ?? ""))
                    } else {
                        print(response)
                    }
                }) { (failure) in
                    Hud.hide(view: self.view)
                    print(failure)
                }
                
                
            } else {
                self.showDefaultAlert(Message: "Please select the weight units to continue further.")
            }
            
        } else {
            self.showDefaultAlert(Message: "Please enter the weight of the package.")
        }
    }
    
    func createLabel() {
        Hud.show(message: "Loading..", view: self.view)
        
        
        
        WebServices.hitFedExAPis(withShippingApiType: .label, methodType: .get, parameters: nil, completionHandler: { (jsonDataz) in
            
            Hud.hide(view: self.view)
            print(jsonDataz)
            
        }) { (error) in
            Hud.hide(view: self.view)
            print(error)
        }
    }
}
