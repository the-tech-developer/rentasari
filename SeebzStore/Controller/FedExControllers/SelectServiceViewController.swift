//
//  SelectServiceViewController.swift
//  seebzStore
//
//  Created by MAC on 23/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import DropDown

class SelectServiceViewController: UIViewController {
    
    @IBOutlet weak var servicetypetextField: UITextField!
    @IBOutlet weak var selectServieButton: UIButton!
    @IBOutlet weak var nextButton: AnimatableButton!
    @IBOutlet var selectPackageBtn: UIButton!
    @IBOutlet var packageLabel: UITextField!
    
    var shipingType: GetShippingTypes?
    let dropDown = DropDown()
    var shipTo: Ship?
    var shipFrom: Ship?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        getSHiipingTypes()
        self.selectServieButton.addTarget(self, action: #selector(tappedSelectServiceBtn), for: .touchUpInside)
        self.selectPackageBtn.addTarget(self, action: #selector(tappedSelectPackageBtn), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(tappedNextButton), for: .touchUpInside)
    }
    
    func getSHiipingTypes() {
        Hud.show(message: "Loading..", view: self.view)
        WebServices.hitFedExAPis(withShippingApiType: .getCarriers, methodType: .get, parameters: nil, completionHandler: { (jsonDataz) in
        
            Hud.hide(view: self.view)
            print(jsonDataz)
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: jsonDataz["carriers"] as Any, options: .prettyPrinted)
                let decodedJson = try JSONDecoder().decode(GetShippingTypes.self, from: jsonData)
                self.shipingType = decodedJson
                self.shipingType = self.shipingType?.filter({($0.carrierCode ?? "") == "fedex"})
            } catch {
                print(error.localizedDescription)
            }
        }) { (error) in
            Hud.hide(view: self.view)
            print(error)
        }
    }
    
    @objc
    func tappedSelectPackageBtn(_ sender: UIButton) {
        dropDown.dataSource = self.shipingType?.first?.packages?.map({($0.name ?? "")}) ?? []
        dropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.packageLabel.text = item
            
            let carrierCode = self.shipingType?.first?.packages?[index].packageCode ?? ""
            self.packageLabel.accessibilityIdentifier = (self.shipingType?.first?.packages?[index].packageCode ?? "")
        }
        
        dropDown.width = self.view.frame.width / 1.5
        dropDown.show()
    }
    
    @objc
    func tappedSelectServiceBtn(_ sender: UIButton) {
        
        //fedex_international_economy ,
        dropDown.dataSource = self.shipingType?.first?.services?.map({($0.name ?? "")}) ?? []
        dropDown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.servicetypetextField.text = item
            let carrierId = self.shipingType?.first?.services?[index].carrierID
            let carrierCode = self.shipingType?.first?.services?[index].serviceCode
            self.servicetypetextField.accessibilityIdentifier = (self.shipingType?.first?.services?[index].serviceCode ?? "")
            print(carrierId , "  " , carrierCode! , self.shipingType?.first?.services?[index].isMultiPackageSupported! )
        }
        
        dropDown.width = self.view.frame.width / 1.5
       
        dropDown.show()
    }
    
    @objc
    func tappedNextButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "PackageWeightViewController") as! PackageWeightViewController
        vc.serviceCode = self.servicetypetextField.accessibilityIdentifier ?? ""
        vc.shipFrom = self.shipFrom
        vc.shipTo = self.shipTo
        vc.packageCode = self.packageLabel.accessibilityIdentifier ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
/*
  var craeteLabelParams: Shipment?
 
 func createLabel() {
    
     let package = Package(from: Weight(value: 12, unit: "ounce"))
     
     let shipto = Ship(name: "ludhiana", phone: "123456", companyName: "hello", addressLine1: "asdasd", addressLine2: "asdasd", cityLocality: "asdasd", stateProvince: "punjab", postalCode: "12313", countryCode: "123123", addressResidentialIndicator: "asdad")
      let shipFrom = Ship(name: "ludhiana", phone: "123456", companyName: "hello", addressLine1: "asdasd", addressLine2: "asdasd", cityLocality: "asdasd", stateProvince: "punjab", postalCode: "12313", countryCode: "123123", addressResidentialIndicator: "asdad")
     
     craeteLabelParams = Shipment(serviceCode: "fedex_2day", shipTo: shipto, shipFrom: shipFrom, packages: [package])
     
     print(craeteLabelParams)
     
 }
 */
