//
//  TabBarVC.swift
//  Dunamis
//
//  Created by Apple on 11/11/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Alamofire
import IBAnimatable

class TabBarVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var containerVIew: UIView!
    @IBOutlet  var tapButtons: [TapButtons]!
    @IBOutlet var profileLbl: [UILabel]!
    @IBOutlet var profileImg: [UIImageView]!
    @IBOutlet var btnbackgroundView: [UIView]!
    

    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var tabInnerView: AnimatableView!
    
    @IBOutlet weak var bottomHeightConstraint: NSLayoutConstraint!
    
    
    // MARK:- VARIABLES
    weak var tabBarVC: TabBarVC?
    var viewControllers : [UIViewController] = []
    static let initialIndex = 0
    @IBInspectable var previouslySelectedButtonTag: Int = initialIndex
    var categories = [CategoryListIncoming.Body]()
    var productss = [ProductListIncoming.Body]()
    var currentAddress : RASAddress!
    var closureDidCategoriesIncoming : (() -> Void)?
    var closureDidGetCategoryListing: ((_ categories: [CategoryListIncoming.Body]) -> Void)?
    var product: ProductListIncoming.Body!
    
    // MARK:- VIEW CONTROLLER LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiCalled(url: .Category_Listing, jsonObject: [:], method: .get)
        self.setupTabDestinations()
        
        let selectedVC = viewControllers[previouslySelectedButtonTag]
        self.addChild(selectedVC)
        selectedVC.view.frame = containerVIew.bounds
        containerVIew.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)
        
        _ = tapButtons.map({ (button) -> Void in
            button.isSelected = false
            
        })
        
        tapButtons.forEach { (button) in
            button.isSelected = false
            button.btnView = btnbackgroundView[button.tag]
            button.btnLbl = profileLbl[button.tag]
            button.btImg = profileImg[button.tag]
        }
        
        if tapButtons.count > 0 {
            tapButtons[0].isSelected = true
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setupTabDestinations() {
        guard let navMain1 = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
            else { fatalError("Could not find navMain7 in storyboard") }
        navMain1.currentAddress = self.currentAddress
        self.closureDidCategoriesIncoming = {
            if self.closureDidGetCategoryListing != nil {
                self.closureDidGetCategoryListing!(self.categories)
            } else {
                navMain1.categoryList = self.categories
            }
        }
        viewControllers.append(navMain1)
        
        guard let navMain2 = self.storyboard?.instantiateViewController(withIdentifier: "MyAdsViewController") as? MyAdsViewController
            else { fatalError("Could not find navMain7 in storyboard") }
//        isScreenComeFrom = .editAd
       // navMain2.categoryList = self.categories
        // navMain2.product = self.product
        viewControllers.append(navMain2)
        
        guard let navMain3 = self.storyboard?.instantiateViewController(withIdentifier: "CreateViewController")
            else { fatalError("Could not find navMain7 in storyboard") }
        viewControllers.append(navMain3)
        
        guard let navMain4 = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController")
            else { fatalError("Could not find navMain7 in storyboard") }
        viewControllers.append(navMain4)
        
        guard let navMain5 = self.storyboard?.instantiateViewController(withIdentifier: "ProfileVC")
            else { fatalError("Could not find navMain7 in storyboard") }
      
        viewControllers.append(navMain5)
    }
    
    //MARK:- ACTION
    
    @IBAction func tabButtonAction(_ sender: TapButtons){
        
        if sender.tag == 2 {
            if let createVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateViewController") as? CreateViewController {
                //createVC.closureDidGetCategoryListing!(self.categories)
                isScreenComeFrom = .createAd
                self.closureDidGetCategoryListing!(self.categories)
                createVC.categoryList = self.categories
                self.navigationController?.pushViewController(createVC, animated: true)
            }
        } else {
            _ =  tapButtons.map { (button) -> Void in
                button.isSelected = false
            }
            sender.isSelected = true
            
            let previousVC = viewControllers[previouslySelectedButtonTag]
            previousVC.willMove(toParent: nil)
            previousVC.view.removeFromSuperview()
            previousVC.removeFromParent()
            
            // Select new tab
            previouslySelectedButtonTag = sender.tag
            
            // Add new tab VC
            let selectedVC = viewControllers[sender.tag]
            self.addChild(selectedVC)
            selectedVC.view.frame = containerVIew.bounds
            containerVIew.addSubview(selectedVC.view)
            selectedVC.didMove(toParent: self)
        }
    }
    
    
    // MARK:- API IMPLEMENTATION
        func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
            Hud.show(message: "Loading...", view: self.view)
            
            WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
                print(dict)
                 Hud.hide(view: self.view)
                
                if isSuccess(json: dict) {
                    // SUCCESS
                    
                    if url == .Category_Listing {
                        // CATEGORY LISTING
                        if let body = CategoryListIncoming(dict: dict).body {
                            var dict = [String:Any]()
                            dict["name"] = "All"
                            dict["cat_img"] = "all-blk"
                            let allCategory = CategoryListIncoming.Body(dict: dict)
                            self.categories.append(allCategory)
                            
                            body.forEach { (bd) in
                                self.categories.append(bd)
                            }
                            
                            categoriess.removeAll()
                            categoriess = self.categories
                        }
                        
                        self.closureDidCategoriesIncoming!()
                    }
                }
                else {
                    // FAILURE
                   // Alert.showSimple(message(json: dict))
                }
            }) { (errorJson) in
                  Hud.hide(view: self.view)
                print(errorJson)
            }
        }
}

// MARK:- EXTENSION 
extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addShadowToView(shadow_color: UIColor,offset: CGSize,shadow_radius: CGFloat,shadow_opacity: Float,corner_radius: CGFloat) {
        self.layer.shadowColor = shadow_color.cgColor
        self.layer.shadowOpacity = shadow_opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = shadow_radius
        self.layer.cornerRadius = corner_radius
    }
}
