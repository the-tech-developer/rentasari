//
//  ExtendDateVC.swift
//  seebzStore
//
//  Created by Deep Baath on 22/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class ExtendDateVC: UIViewController {
    
    @IBOutlet weak var extendProductImage: AnimatableImageView!
    
    @IBOutlet weak var extendProductTitle: UILabel!
    
    @IBOutlet weak var extendProductRentBuyLabel: UILabel!
    @IBOutlet weak var extendProductPrice: UILabel!
    @IBOutlet weak var extendProductUserName: UILabel!
    
    @IBOutlet weak var extendProductCategory: UILabel!
    
    
    @IBOutlet weak var extendProductAdress: UILabel!
    
    @IBOutlet weak var extendProductStartDate: UITextField!
    
    @IBOutlet weak var extendProductTotalAmount: UILabel!
    @IBOutlet weak var extendProductEndDate: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
   }
    
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func extendedDateAction(_ sender: Any) {
        
    }
    
}

 
