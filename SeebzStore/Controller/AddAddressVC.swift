//
//  AddAddressVC.swift
//  seebzStore
//
//  Created by Deep Baath on 19/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {
    
    
    //MARK:- OUTLETS
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var address1TF: UITextField!
    @IBOutlet weak var address2TF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var saveAdressBtn: UIButton!
    
    
    var delivery: DeliveryListIncoming.Body!
    var myDeliveryDetail = [DeliveryListIncoming.Body]()
    
    
    var comeFrom = ""
     
    override func viewDidLoad() {
        super.viewDidLoad()
        if comeFrom == "Edit_Deliveryinfo"{
                        
            nameTF.text = "\(myDeliveryDetail[0].name ?? "")"
            address1TF.text = "\(myDeliveryDetail[0].address_line_1 ?? "")"
            address2TF.text = "\(myDeliveryDetail[0].address_line_2 ?? "")"
            mobileTF.text = "\(myDeliveryDetail[0].phone_no ?? 0)"
            stateTF.text = "\(myDeliveryDetail[0].state ?? "")"
            city.text = "\(myDeliveryDetail[0].city ?? "")"

        }else{
            
        }

    }
    
    //MARK:- ACTION
    @IBAction func saveAddressAction(_ sender: UIButton){
         self.adressAPI()
        
    }

    @IBAction func backAction(_ sender: UIButton){
         self.navigationController?.popViewController(animated: true)
   }
    
    
    //MARK:- API IMPLEMENTATION
    func adressAPI(){
        if !nameTF!.length(){
              Alert.showSimple("Please enter Your Name.")
                  return
              }
        if !address1TF!.length(){
            Alert.showSimple("Please enter Address line 1.") {
                
            }
                         return
                     }
        if !address2TF!.length(){
                Alert.showSimple("Please enter Address line 2.")
                    return
                            }
        if !mobileTF!.length(){
            Alert.showSimple("Please enter Mobile No.")
                return
                                  }
        if !stateTF!.length(){
                Alert.showSimple("Please enter State.")
                    return
                                         }
        if !city!.length(){
            Alert.showSimple("Please enter City.")
            return
                                               }
        if !mobileTF!.isValidPhoneNumber() {
                    Alert.showSimple("Please enter valid mobile Number")
                    return
                }


         guard let userInfo = Cookies.userInfo() else { return }

        var addressParams = [String: Any]()
        addressParams["user_id"] = userInfo.user_id
        addressParams["name"] = nameTF.text
        addressParams["address_line_1"] = address1TF.text
        addressParams["address_line_2"] = address2TF.text
        addressParams["mobile_num"] = mobileTF.text
        addressParams["state"] = stateTF.text
        addressParams["city"] = city.text
        
        var url : Api?
        if comeFrom == "Edit_Deliveryinfo"{
            url = .edit_address
            addressParams["address_id"] = myDeliveryDetail[0].address_id
            
        }else{
            url = .add_address
        }
        
         Hud.show(message: "Loading...", view: self.view)
        WebServices.post(url: url!, jsonObject: addressParams, method: .post, completionHandler: { (dict) in
            print("Result")
            
              Hud.hide(view: self.view)
            if let success = dict["success"] as? Int {
                             if success == 200 {
                                
              self.navigationController?.popToSpecificVC(viewController: DeliveryInfoVC.self)
                             }
                         } else
                     {
                         Alert.showSimple(message(json: dict))
                     }
        }) { (error) in
              Hud.hide(view: self.view)
            
        }
     }
}
