//
//  OrderHistoryVC.swift
//  seebzStore
//
//  Created by Deep Baath on 22/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView!
 
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- ACTION
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func searchAction(_ sender: UIButton){
           
    }
}

class OrderHistoryCell: UITableViewCell{
    
    //MARK:- OUTLETS
    @IBOutlet var orderImg: UIImageView!
    @IBOutlet var orderTitleLbl: UILabel!
     @IBOutlet var orderAmountLbl: UILabel!
     @IBOutlet var orderTypeLbl: UILabel!
     @IBOutlet var orderDetailLbl: UILabel!
    
}
  
// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension OrderHistoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 141
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell", for: indexPath) as! OrderHistoryCell
//        cell.adsImg.image = PrImg[indexPath.row]
//        cell.AdnameLbl.text = PrName[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "WishlistDetailVC") as! WishlistDetailVC
           
        
                 
           navigationController?.pushViewController(VC, animated: true)
    }
}
    
 
