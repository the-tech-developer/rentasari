//
//  WishlistDetailVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class WishlistDetailVC: UIViewController {
    
    // MARK:- OUTLETS
    @IBOutlet var topLBl: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var dayTitleLbl: UILabel!
    @IBOutlet var dayTypeLbl: UILabel!
    @IBOutlet var dayAddressLbl: UILabel!
    @IBOutlet var remainingdayLbl: UILabel!
    @IBOutlet var dayAMountLbl: UILabel!
    @IBOutlet var daywishLbl: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var wishUserNameLbl: UILabel!
    @IBOutlet var wishUserEmailLbl: UILabel!
    @IBOutlet var wishDateLbl: UILabel!
    @IBOutlet var seeProfileBtn: AnimatableButton!
    
    let img2: [UIImage] =
          [UIImage(named: "jewellry-img")!,
          UIImage(named: "jewellry-img")!, UIImage(named: "jewellry-img")!, UIImage(named: "jewellry-img")!, UIImage(named: "jewellry-img")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    //MARK:- ACTION
    
    @IBAction func swipeLeftAction(_ sender: Any){
        
    }
    
    @IBAction func swipeRightAction(_ sender: Any){
        
    }
    
    @IBAction func clickFavAction(_ sender: Any){
           
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareAction(_ sender: Any) {
    }
    
    @IBAction func seeProfileAction(_ sender: Any) {
    }
    
    @IBAction func returnAction(_ sender: Any) {
     let vc = self.storyboard?.instantiateViewController(identifier: "ReturnDetailVC") as! ReturnDetailVC
               
               
        
                        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func extendedAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "ExtendDateVC") as! ExtendDateVC
                 
              
                       
                 navigationController?.pushViewController(VC, animated: true)
    }
   
}
   
// MARK:- EXTENSION CLASS
extension WishlistDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return img2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "wishDetailCell", for: indexPath) as! wishDetailCell
        
        cell.wishBackImg.image = img2[indexPath.row]
           
           return cell
        
    }
}


class wishDetailCell: UICollectionViewCell {
    @IBOutlet var wishBackImg: UIImageView!
    //@IBOutlet var rentBackImg: UIImageView!
    @IBOutlet var rentRightBtn: UIButton!
    @IBOutlet var rentLeftBtn: UIButton!
    @IBOutlet var rentFavBtn: UIButton!
    
    
}
