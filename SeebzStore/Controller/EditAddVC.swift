//
//  EditAddVC.swift
//  seebzStore
//
//  Created by IOS on 11/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable

class collectionviewCell: UICollectionViewCell {
    
    //MARK:- OUTLETS
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var roundView: AnimatableView!
    @IBOutlet weak var AllLbl: UILabel!
    @IBOutlet weak var EditImg: UIImageView!
    
}
    
    class EditAddVC: UIViewController {

        @IBOutlet weak var collectionView: UICollectionView!
        
        let Images: [UIImage] = [
                
               UIImage(named: "clothes-blk")!,
               UIImage(named: "shoes-fade")!,
               UIImage(named: "jewellry-fade")!,
               ]
}

     // MARK:- COLLECTION VIEW DATA SOURCE & DELEGATE METHODS
extension EditAddVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Images.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! collectionviewCell
        cell.EditImg.image = Images[indexPath.row]
        
        return cell
    }
}
    
