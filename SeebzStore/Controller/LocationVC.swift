//
//  LocationVC.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps

protocol GetAddress: class {
    func gettingAddress(selectedLocation: RASAddress)
}

struct SelectedLocationData {
    let address: String
    let coordinates: CLLocationCoordinate2D
}

class tableViewCell: UITableViewCell {
     // MARK:- OUTLETS
    @IBOutlet weak var ImgLcn: UIImageView!
    @IBOutlet weak var LblLcn: UILabel!
    @IBOutlet weak var LblLcn2: UILabel!
    
    func configure(prediction: GMSAutocompletePrediction) {
        LblLcn.text = prediction.attributedPrimaryText.string
        LblLcn2.text = prediction.attributedFullText.string
    }
}

class LocationVC: UIViewController {
    
    //MARK:- OUTLET
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var searchTextField: UITextField!
    
    weak var delegate: HomeViewController?
    weak var signupVCDelagate: SignupViewController?
    var closureDidGetAddress: ((RASAddress) -> Void)?
    var predictions = [GMSAutocompletePrediction]()
    var selectedAddress : RASAddress?
    var addAdList : AddAdList!
    var product = [ProductListIncoming.Body]()
    var selectedImages = [UIImage]()
    var isScreenComeFrom : ScreenComeFrom = .none

    
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    // MARK:- CORE FUNCTIONS
    
    
    // MARK:- IBACTIONS
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: UIButton){
        
        if isScreenComeFrom == .createAd{
            if let selectedAddress = self.selectedAddress {
                       addAdList.address = selectedAddress.address
                       addAdList.latitude = selectedAddress.location.latitude
                       addAdList.longitude = selectedAddress.location.longitude
                       
                       let confirmYourAddVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmYourAddVC") as! ConfirmYourAddVC
                       confirmYourAddVC.addAdList = self.addAdList
                confirmYourAddVC.isScreenComeFrom = .createAd
                       self.navigationController?.pushViewController(confirmYourAddVC, animated: true)
                   } else {
                       Alert.showSimple("Please add the address.")
                   }
        }else if isScreenComeFrom == .editAd {
            let selectedAddress = self.selectedAddress
            addAdList.address = selectedAddress!.address
            addAdList.latitude = selectedAddress!.location.latitude
            addAdList.longitude = selectedAddress!.location.longitude
            
            let confirmYourAddVC = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmYourAddVC") as! ConfirmYourAddVC
                                  confirmYourAddVC.addAdList = self.addAdList
                                  confirmYourAddVC.product = self.product
            confirmYourAddVC.isScreenComeFrom = .editAd
                                  self.navigationController?.pushViewController(confirmYourAddVC, animated: true)
        }else{
            let selectedAddress = self.selectedAddress
                      addAdList.address = selectedAddress!.address
                      addAdList.latitude = selectedAddress!.location.latitude
                      addAdList.longitude = selectedAddress!.location.longitude
            let confirmYourAddVC = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardVC") as! SavedCardVC
                
            self.navigationController?.pushViewController(confirmYourAddVC, animated: true)
            
            
        }
       
    }
    
    @IBAction func useCurrentLocationViewTapped(_ sender: UITapGestureRecognizer){
        Location.sharedInstance.getCurrentAddress { (isFetchedAddress, currentAddress) in
            if isFetchedAddress {
                if self.isScreenComeFrom == .createAd || self.isScreenComeFrom == .editAd {
                     self.searchTextField.text = currentAddress.address
                     self.searchTextField.resignFirstResponder()
                    self.selectedAddress = currentAddress
                } else if self.isScreenComeFrom == .homeVC || self.isScreenComeFrom == .signUp {
                    
                    self.closureDidGetAddress!(currentAddress)
                    self.searchTextField.text = currentAddress.address
                    self.searchTextField.resignFirstResponder()

                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
}
 
// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension LocationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return predictions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! tableViewCell
        cell.configure(prediction: predictions[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let prediction = predictions[indexPath.row]
        Location.sharedInstance.convertPlaceIntoCoordinates(prediction: prediction, completion: { (gmsPlace, error) in
            if let place = gmsPlace {
                Location.sharedInstance.getSelectedLocationAddress(coordinate: place.coordinate) { (isLocationFetched, address) in
                    if isLocationFetched {
                        
                        if self.isScreenComeFrom == .homeVC || self.isScreenComeFrom == .signUp{
                            self.closureDidGetAddress!(address)
                            self.searchTextField.text = address.address
                            self.searchTextField.resignFirstResponder()

                            self.navigationController?.popViewController(animated: true)
                        } else if self.isScreenComeFrom == .createAd {
                            self.selectedAddress = address
                            self.searchTextField.text = address.address
                            self.searchTextField.resignFirstResponder()
                        }
                    }
                }
            }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}


extension LocationVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if text != "" {
                Location.sharedInstance.searchLocation(text: text, completion: { predictions in
                    if predictions.count != 0 {
                        self.predictions = predictions
                        self.TableView.reloadData()
                    }
                })
            }
        }
        return true
    }
}
