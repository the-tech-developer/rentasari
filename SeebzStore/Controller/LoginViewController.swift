//
//  LoginViewController.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import Alamofire

class LoginViewController: UIViewController {

     // MARK:- OUTLETS
    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passShowBtn: UIButton!
    
    //MARK:- VARIABLES
    var selectedUser = [UserLogin.User]()
    var userDict: UserLogin.User!
    
    // MARK:- LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        self.placeholderChanged()
        if Cookies.userInfo() != nil {
            self.openHomeScreen()
        }
    }
    
    func placeholderChanged() {
        loginTF.placeholderColor("Email address", color: UIColor.lightGray)
        passwordTF.placeholderColor("Password", color: UIColor.lightGray)
    }
    
    // MARK:- ACTION
    @IBAction func passShowAction(_ sender: UIButton){
        
    }
    
    @IBAction func loginBtnAction(_ sender: UIButton){
        callLogin()
    }
    
    
    func openHomeScreen() {
        let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
        self.navigationController?.pushViewController(tabBarVC, animated: true)
    }
    
    
    // MARK:- API IMPLEMENTATION
    func callLogin(){
        if !loginTF!.length(){
            Alert.showSimple("Please enter Email Address."){
            }
            return
        }
        if !passwordTF.length() {
            Alert.showSimple("Please enter valid password")
            return
        }
        
        var loginparams = [String: Any]()
        loginparams["email"] = loginTF.text.leoSafe()
        loginparams["password"] = passwordTF.text.leoSafe()
        
        Hud.show(message: "Loading...", view: self.view)
        
        WebServices.post(url: .login, jsonObject: loginparams, method: .post, completionHandler: { (json) in
            print("Result",json)
            
            Hud.hide(view: self.view)
            if let success = json["success"] as? Int {
                if success == 200 {
                    let userDict = UserLogin(dict: json)
                    Cookies.userInfoSave(dict: userDict.user?.first?.serverData)
                    
                    self.openHomeScreen()
                } else if success == 400 {
                    Alert.showSimple(message(json: json))
                }
            } else {
                Alert.showSimple(message(json: json))
            }
        })
        { (error) in
            Hud.hide(view: self.view)
        }
    }
    func forgotPasswordApi(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
         WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
             print(dict)
             Hud.hide(view: self.view)

             if isSuccess(json: dict) {
                 // SUCCESS
                 if url == .forgot_password {
                     Alert.showSimple("Password reset successfully") {
                     }
                 }
             }
             else {
                 // FAILURE
                 Alert.showSimple(message(json: dict))
             }
         }) { (errorJson) in
             Hud.hide(view: self.view)
             print(errorJson)
         }
     }
    
    @IBAction func forgotPassAction(_ sender: UIButton){
        
            let alertController = UIAlertController(title: "Forgot Password", message: "", preferredStyle: UIAlertController.Style.alert)
            
            alertController.addTextField { (firstTextField : UITextField!) -> Void in
                firstTextField.placeholder = "Enter E-mail"
//                firstTextField.isSecureTextEntry = true
            }
          
            let saveAction = UIAlertAction(title: "Send", style: UIAlertAction.Style.default, handler: { alert -> Void in
                
                let firstTextField = alertController.textFields![0]
                
                let params = ["email": Cookies.userInfo()?.email]
                self.forgotPasswordApi(url: .forgot_password, jsonObject: params, method: .post)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
                (action : UIAlertAction!) -> Void in })
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    
    
    @IBAction func signupBtnAction(_ sender: UIButton){
        let signUpvc = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(signUpvc, animated: true)
    }
    
    @IBAction func actionSecurePassword(_ sender: UIButton) {
        if passwordTF.isSecureTextEntry {
            passwordTF.isSecureTextEntry = false
        } else {
            passwordTF.isSecureTextEntry = true
        }
    }
}
   
