//
//  SettingVC.swift
//  seebzStore
//
//  Created by IOS on 12/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit


class TableviewCell: UITableViewCell {
    
    //MARK:- OUTLETS TABLE VIEW
    @IBOutlet weak var UprLbl: UILabel!
    @IBOutlet weak var DwnLbl: UILabel!
    @IBOutlet weak var notiSwitch: UISwitch!
    @IBOutlet weak var NotificationBtn: UIButton!
}

  class SettingVC: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    let TopLabel = ["Change Country/Region", "Notification Alert", "Change Language"]
    let BtmLabel = ["USA", "Punjabi", "English"]
    

      @IBAction func backAction(_ sender: Any) {
          
          self.navigationController?.popViewController(animated: true)
      }
}


 // MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension SettingVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopLabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableviewCell
         cell.UprLbl.text = TopLabel[indexPath.row]
         cell.DwnLbl.text = BtmLabel[indexPath.row]
        if cell.NotificationBtn != nil {
            cell.NotificationBtn.isHidden = true

        }
              return cell
//        if indexPath.row == 0 {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableviewCell
//        cell.UprLbl.text = TopLabel[indexPath.row]
//        cell.DwnLbl.text = BtmLabel[indexPath.row]
//            cell.NotificationBtn.isHidden = true
//        return cell
//        }else {
//            if indexPath.row == 1 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableviewCell
//                      cell.UprLbl.text = TopLabel[indexPath.row]
//                      cell.DwnLbl.text = BtmLabel[indexPath.row]
//                cell.NotificationBtn.isHidden = false
//                      return cell
//            } else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableviewCell
//                       cell.UprLbl.text = TopLabel[indexPath.row]
//                       cell.DwnLbl.text = BtmLabel[indexPath.row]
//                           cell.NotificationBtn.isHidden = true
//                       return cell
//
//            }
        //}
   }
}

    
 
