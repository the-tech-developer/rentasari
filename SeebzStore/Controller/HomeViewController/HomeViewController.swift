//
//  HomeViewController.swift
//  seebzStore
//
//  Created by IOS on 12/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire
import MapKit
import Nuke

class HomeViewController: UIViewController {
    
    // MARK:- OUTLETS
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var wishlistBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var loactionNmae: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var logoImg: UIImageView!
    
    // MARK:- VARIABLES
    var refreshControl = UIRefreshControl()
    
    var categories: Categories? = []
    
    var homeScreenAllData: HomeScreenData? = []
    var homeScreenAllStoredData: HomeScreenData? = []
    var isScreenComeFrom : ScreenComeFrom = .none

    //search textField
    var searchtextField: UITextField = {
        let textField = UITextField()
        
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 3.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        
        textField.textAlignment = .center
        textField.placeholder = "Type Product Name..."
        textField.textColor = .black
        textField.layer.cornerRadius = 20
        
        return textField
    }()
    
    
     let crossBtnImage = UIImage(named: "cross_grey")
     let searchBtnImage = UIImage(named: "Search")
    
    
    var categoryList = [CategoryListIncoming.Body]()
    var productListIncoming = [ProductListIncoming.Body]()
    var ProductDetailList: HomeScreenProductDetail?
    var currentAddress : RASAddress!
    var selectedIndex = Int()
    weak var tabBarVC: TabBarVC?
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override
    func viewDidLoad() {
        super.viewDidLoad()
        getCategoriesList()
        searchtextField.addTarget(self, action: #selector(searchingDidStarted), for: .editingChanged)
        
        guard let userInfo = Cookies.userInfo() else { return }
        userNameLbl.text = "\(userInfo.firstaname.capitalized)"
        
        if self.currentAddress != nil {
            self.loactionNmae.setTitle(currentAddress.city, for: .normal)
            homeScreenApi()
        } else {
            Location.sharedInstance.getCurrentAddress { (isAddressFetched, currentAddress) in
                self.currentAddress = currentAddress
                self.loactionNmae.setTitle(currentAddress.city, for: .normal)
                self.homeScreenApi()
            }
        }
        
        if let tabBarVC = self.navigationController?.topViewController as? TabBarVC {
            tabBarVC.closureDidGetCategoryListing = { categories in
                categoriess = categories
                self.categoryList = categories
                self.collectionview.reloadData()
            }
        }
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableview.addSubview(refreshControl)
    }
    
    
    override
    func viewWillAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 0.1) {
            if self.tabBarVC != nil {
                self.tabBarVC!.tabView.isHidden = false
                self.tabBarVC!.bottomHeightConstraint.constant = 60
                self.tabBarVC!.tabInnerView.isHidden = false
                //                self.view.layoutIfNeeded()
                self.tabBarVC!.view.layoutIfNeeded()
            }
        }
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tableview.addSubview(refreshControl)
//        self.homeScreenApi()
//        self.tableview.reloadData()
    }
    
    
    func showSearchtextField() {
        searchtextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(searchtextField)
        self.view.bringSubviewToFront(searchtextField)
        searchtextField.alpha = 0
        
        searchtextField.backgroundColor = UIColor.white
        searchtextField.topAnchor.constraint(equalToSystemSpacingBelow: self.view.topAnchor, multiplier: 12).isActive = true
        searchtextField.centerXAnchor.constraint(equalToSystemSpacingAfter: self.view.centerXAnchor, multiplier: 0).isActive = true
        searchtextField.widthAnchor.constraint(equalToConstant: (self.view.bounds.width / 1.2)).isActive = true
        self.searchtextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        searchtextField.layoutIfNeeded()
        
        searchtextField.becomeFirstResponder()
        
        UIView.animate(withDuration: 1) {
            self.searchtextField.alpha = 1
        }
    }
    
    func hidetextField() {
        self.searchtextField.text = ""
        UIView.animate(withDuration: 1, animations: {
            self.searchtextField.alpha = 0
        }) { (isCmpleted) in
            if isCmpleted {
                self.searchtextField.resignFirstResponder()
                self.searchtextField.removeFromSuperview()
            }
        }
    }
    
    
    @objc
    func refresh(sender:AnyObject) {
        if self.currentAddress != nil {
            homeScreenApi()
        } else {
            // FETCHING AND DISPLAYING CURRENT DEVICE LOCAITON ADDRESS
            Location.sharedInstance.getCurrentAddress { (isAddressFetched, currentAddress) in
                self.currentAddress = currentAddress
                self.loactionNmae.setTitle(currentAddress.city, for: .normal)
                self.homeScreenApi()
            }
        }
    }
    
    @objc
    func searchingDidStarted(_ sender: UITextField) {
        self.homeScreenAllData = self.homeScreenAllStoredData
        self.homeScreenAllData = self.homeScreenAllData?.filter({($0.title ?? "").lowercased().contains((sender.text ?? "").lowercased())})
        
        if (sender.text ?? "" ) == "" {
            self.homeScreenAllData = self.homeScreenAllStoredData
        }
        self.tableview.reloadData()
    }
    
    // MARK:- IBACTIONS
    @IBAction
    func searchAction(_ sender: UIButton){
        if sender.imageView?.image == searchBtnImage {
            showSearchtextField()
            print("search")
            sender.setImage(crossBtnImage, for: .normal)
        } else {
            sender.setImage(searchBtnImage, for: .normal)
            print("hide")
            hidetextField()
        }
        
        //        self.logoImg.isHidden = true
        //        self.searchBtn.isHidden = true
        //        self.wishlistBtn.isHidden = true
        //        self.notificationBtn.isHidden = true
        //        self.searchBar.isHidden = false
    }
    
    @IBAction
    func notificationAction(_ sender: UIButton){
        
        let vc = self.storyboard?.instantiateViewController(identifier: "NotificationVC") as! NotificationVC
        vc.productListIncoming = productListIncoming
        vc.categoryList = categoryList
        vc.ProductDetailList = ProductDetailList
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
    func postAction(_ sender: UIButton){ }
    
    @IBAction
    func wishListAction(_ sender: UIButton){
        let vc = self.storyboard?.instantiateViewController(identifier: "MyWishlistVC") as! MyWishlistVC
        vc.categoryList = self.categoryList
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction
    func getLocationAction(_ sender: UIButton){
        isScreenComeFrom = .homeVC
        let locationVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        locationVC.delegate = self
        locationVC.isScreenComeFrom = .homeVC
        locationVC.closureDidGetAddress = { currentAddress in
            self.currentAddress = currentAddress
            
            self.homeScreenApi()
            // self.callHomeListApi()
            self.loactionNmae.setTitle(currentAddress.city, for: .normal)
        }
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
}


// MARK:- COLLECTION VIEW DATA SOURCE & DELEGATE METHODS
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.categories?.count ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! collectionviewcell
        
        switch indexPath.row {
        case 0 :
            cell.ProductImg.image = UIImage(named: "all-blk")
            cell.ProdctLbl.text = "All"
        default:
            cell.category = self.categories?[indexPath.row - 1]
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionview.frame.width / 4 - 6, height: self.collectionview.frame.height - 8)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            homeScreenApi()
        } else {
            self.updateProductListOnTapOnCat(cat_id: (self.categories?[indexPath.row - 1].catID ?? 0))
        }
        
        for i in 0..<self.categoryList.count {
            let otherIndexPath = IndexPath(row: i, section: 0)
            if let selectedCell = collectionView.cellForItem(at: otherIndexPath) as? collectionviewcell {
                selectedCell.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }
        }
        
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? collectionviewcell {
            selectedCell.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
    }
    
    func updateProductListOnTapOnCat(cat_id: Int) {
        guard let userInfo = Cookies.userInfo() else { return }
        var param = [String:Any]()
        param["user_id"] = userInfo.user_id
        param["cat_id"] = cat_id
        getHomeScreenAds(withParams: param)
    }
}


// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.homeScreenAllData?.count ?? 0) == 0 {
            tableView.setErrorMessage(withMessage: "No Data Found.")
        } else {
            tableView.removeErrorMessage()
        }
        
        return (self.homeScreenAllData?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! tableviewCell
        
        cell.homeScreenData = self.homeScreenAllData?[indexPath.row]
        cell.delegate = self
        // cell.configure(product: productListIncoming[indexPath.row], categoryList: self.categoryList)
        //cell.favoriteButton.tag = indexPath.row
        //cell.favoriteButton.addTarget(self, action: #selector(favoriteBtnTapped(button:)), for: .touchUpInside)
        //       r action: #selector(postButtonTap(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailVC.isScreenComeFrom = .homeVC

        //        detailVC.homeVC = self
        //        detailVC.selectedIndex = indexPath.row
        //        detailVC.categoryList = self.categoryList
        //        detailVC.ProductDetailList = self.ProductDetailList
        //        detailVC.product = self.productListIncoming[indexPath.row]
        detailVC.product_id =  (self.homeScreenAllData?[indexPath.row].productID ?? 0)
        detailVC.comingProduct = (self.homeScreenAllData?[indexPath.row])
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}



//MARK:- APIS
extension HomeViewController {
    
    func getCategoriesList() {
        WebServices.post(url: .Category_Listing, jsonObject: [:], method: .get, completionHandler: { (response) in
            
            if isSuccess(json: response) {
                
                if let body = response["body"] as? [[String:Any]] {
                    do {
                        
                        let serializedData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                        let decodedJson = try JSONDecoder().decode(Categories.self, from: serializedData)
                        self.categories = decodedJson
                        self.collectionview.reloadData()
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            } else {
                Alert.showSimple(message(json: response))
            }
        }) { (error) in
            print(error)
            // Alert.showSimple(message(json: dict))
        }
    }
    
    func homeScreenApi() {
        Hud.show(message: "Loading...", view: self.view)
        guard let userInfo = Cookies.userInfo() else { return }
        if currentAddress == nil {
            return
        }
        let params = ["user_id": userInfo.user_id, "latitude": currentAddress.location.latitude, "longitude": currentAddress.location.longitude] as [String : Any]
        
        WebServices.post(url: .home_screen, jsonObject: params, method: .post, completionHandler: { (response) in
            Hud.hide(view: self.view)
            self.refreshControl.endRefreshing()
            if isSuccess(json: response) {
                
                if let body = response["body"] as? [[String:Any]] {
                    do {
                        
                        let serializedData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                        let decodedJson = try JSONDecoder().decode(HomeScreenData.self, from: serializedData)
                        self.homeScreenAllData = decodedJson
                        self.homeScreenAllStoredData = decodedJson
                        self.tableview.reloadData()
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            } else {
                Alert.showSimple(message(json: response))
            }
        }) { (error) in
            Hud.hide(view: self.view)
            self.refreshControl.endRefreshing()
            print(error)
            // Alert.showSimple(message(json: dict))
        }
    }
    
    func getHomeScreenAds(withParams params: [String:Any]) {
        
        self.refreshControl.beginRefreshing()
        WebServices.post(url: .Search_home_screenAds, jsonObject: params, method: .post, completionHandler: { (response) in
            self.refreshControl.endRefreshing()
            if isSuccess(json: response) {
                
                if let body = response["body"] as? [[String:Any]] {
                    do {
                        let serializedData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                        let decodedJson = try JSONDecoder().decode(HomeScreenData.self, from: serializedData)
                        self.homeScreenAllData = decodedJson
                        self.homeScreenAllStoredData = decodedJson
                        self.tableview.reloadData()
                        
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            } else {
                Alert.showSimple(message(json: response))
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            print(error)
        }
    }
    
    // MARK:- API IMPLEMENTATION
    func favApi(url: Api, jsonObject: [String:Any], method: HTTPMethod , addID: Int) {
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                let indexTemp = self.homeScreenAllData?.firstIndex(where: {($0.adID ?? 0) == addID })
                guard let index = indexTemp else { return }
                self.homeScreenAllData?[index].isFavroite = ((self.homeScreenAllData?[index].isFavroite ?? 0) == 0 ? 1 : 0)
                
                print(self.homeScreenAllData?[index])
                
                let indexTemp2 = self.homeScreenAllStoredData?.firstIndex(where: {($0.adID ?? 0) == addID })
                guard let index2 = indexTemp2 else { return }
                self.homeScreenAllStoredData?[index2].isFavroite = ((self.homeScreenAllStoredData?[index2].isFavroite ?? 0) == 0 ? 1 : 0)
                self.homeScreenApi()
                self.tableview.reloadData()
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
}

extension HomeViewController: HomeScreenApiHittingAble {
    func didTappedFavBtn(withParam param: [String : Any], adId: Int) {
        favApi(url: .addFavroite, jsonObject: param, method: .post, addID: adId)
    }
}


/**
 
 
 
 
 
 func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
 WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
 print(dict)
 Hud.hide(view: self.view)
 
 if isSuccess(json: dict) {
 // SUCCESS
 if url == .home_screen {
 if let body = ProductListIncoming(dict: dict).body {
 //                        let leo = LeoSwiftCoder()
 //                        leo.leoClassMake(withName: "MyAds", json: dict)
 
 self.productListIncoming = body
 self.productListIncoming.reverse()
 self.tableview.reloadData()
 self.refreshControl.endRefreshing()
 }
 }
 //                else if url == .addFavroite {
 ////                    self.callHomeListApi()
 //                    if self.productListIncoming[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
 //                        self.productListIncoming[self.selectedIndex].is_favroite = 1
 //
 //                    } else {
 //                        self.productListIncoming[self.selectedIndex].is_favroite = 0
 //
 //                    }
 ////                     self.callHomeListApi()
 //                    self.refreshControl.endRefreshing()
 //                    self.tableview.reloadData()
 //                }
 else if url == .Category_Listing {
 // CATEGORY LISTING
 if let body = CategoryListIncoming(dict: dict).body {
 
 var dict = [String:Any]()
 dict["name"] = "All"
 dict["cat_img"] = "all-blk"
 let allCategory = CategoryListIncoming.Body(dict: dict)
 self.categoryList.append(allCategory)
 
 body.forEach { (bd) in
 self.categoryList.append(bd)
 }
 
 categoriess.removeAll()
 categoriess = self.categoryList
 }
 self.collectionview.reloadData()
 }
 else if url == .Search_home_screenAds {
 if let body = ProductListIncoming(dict: dict).body {
 self.productListIncoming = body
 self.productListIncoming.reverse()
 self.tableview.reloadData()
 self.refreshControl.endRefreshing()
 }
 }
 }
 else {
 // FAILURE
 self.refreshControl.endRefreshing()
 Alert.showSimple(message(json: dict))
 }
 }) { (errorJson) in
 self.refreshControl.endRefreshing()
 Hud.hide(view: self.view)
 print(errorJson)
 }
 }
 
 
 
 //   @objc func postButtonTap(sender: UIButton){
 //    self.gettingScreenDetail()
 ////    let productList = self.productListIncoming[sender.tag]
 ////    let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
 ////           detailVC.homeVC = self
 //////           detailVC.selectedIndex = indexPath.row
 ////           detailVC.categoryList = self.categoryList
 //////           detailVC.product = self.productListIncoming[indexPath.row]
 ////           self.navigationController?.pushViewController(detailVC, animated: true)
 ////    vc. = productList
 //
 //    }
 
 
 //        if selectedProduct.is_favroite == 0 {
 //             params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": FavoriteStatus.isFavorite.rawValue] as [String : Any]
 //            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
 //        }else if selectedProduct.is_favroite == 1{
 //            params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": FavoriteStatus.isUnfavorite.rawValue] as [String : Any]
 //            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
 //        }
 
 
 
 func callHomeListApi() {
 guard let userInfo = Cookies.userInfo() else { return }
 if currentAddress == nil {
 return
 }
 let params = ["user_id": userInfo.user_id, "latitude": currentAddress.location.latitude, "longitude": currentAddress.location.longitude] as [String : Any]
 // self.apiCalled(url: .home_screen, jsonObject: params, method: .post)
 }
 
 @objc func favoriteBtnTapped(button: UIButton) {
     guard let userInfo = Cookies.userInfo() else { return }
     let index = button.tag
     let selectedProduct = self.productListIncoming[index]
     
     var params = [String:Any]()
     
     if selectedProduct.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
         params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": 1 ] as [String : Any]
         self.favApi(url: .addFavroite, jsonObject: params, method: .post)
     } else if selectedProduct.is_favroite.leoSafe() == 1 {
         params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": 0] as [String : Any]
         self.favApi(url: .addFavroite, jsonObject: params, method: .post)
     }
     self.selectedIndex = index
 }
 
 
 
 */
