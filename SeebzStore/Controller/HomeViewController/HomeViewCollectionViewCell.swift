//
//  HomeViewCollectionViewCell.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire
import MapKit
import Nuke

class collectionviewcell: UICollectionViewCell {
    @IBOutlet weak var circleView: AnimatableView!
    @IBOutlet weak var ProductImg: UIImageView!
    @IBOutlet weak var ProdctLbl: UILabel!
    
    
    var category: Category? {
        didSet{
            ProdctLbl.text = (category?.name ?? "").capitalized
            if let url = URL(string: imageBaseUrl + (category?.catImg ?? "")) {
                Nuke.loadImage(with: url, into: ProductImg)
            }
        }
    }
    
    func configure(category: CategoryListIncoming.Body, index: Int) {
        
        if index == 0 {
            self.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        }
        
        ProdctLbl.text = category.name.leoSafe().capitalized
        if category.name.leoSafe() == "All" {
            ProductImg.image = UIImage(named: category.cat_img.leoSafe())
        } else {
            if let url = URL(string: imageBaseUrl + category.cat_img.leoSafe()) {
                Nuke.loadImage(with: url, into: ProductImg)
            }
        }
    }
}
