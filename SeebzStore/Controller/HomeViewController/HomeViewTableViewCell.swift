//
//  HomeViewTableViewCell.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire
import MapKit
import Nuke

protocol HomeScreenApiHittingAble: class {
    func didTappedFavBtn(withParam param: [String: Any] , adId: Int)
}

class tableviewCell: UITableViewCell {
    
    @IBOutlet weak var PrdImage: AnimatableImageView!
    @IBOutlet weak var PrdNameLbl: UILabel!
    @IBOutlet weak var CntImage: UIImageView!
    @IBOutlet weak var CntName: UILabel!
    @IBOutlet weak var CtgImage: UIImageView!
    @IBOutlet weak var CtgName: UILabel!
    @IBOutlet weak var LocationImg: UIImageView!
    @IBOutlet weak var LocationLbl: UILabel!
    @IBOutlet weak var SellView: AnimatableView!
    @IBOutlet weak var PriceLbl: UILabel!
    @IBOutlet weak var SellLbl: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    
    weak var delegate: HomeScreenApiHittingAble?
    var homeScreenData: HomeScreenDatum? {
        didSet {
            PrdNameLbl.text = (homeScreenData?.title ?? "").firstUppercased
            LocationLbl.text = homeScreenData?.location ?? ""
            PriceLbl.text = "$\((homeScreenData?.price ?? 0))"
            CntName.text = "\((homeScreenData?.username ?? ""))"
            CtgName.text = "\((homeScreenData?.status ?? ""))"
            
            print("\n\n\n\n\n\n\n\n\n\n\n\n")
            print((homeScreenData?.adID ?? 0) , homeScreenData?.isFavroite ?? 0)
            print("\n\n\n\n\n\n\n\n\n\n\n\n")
            
            
            
            PrdImage.showImageUsingURL(urlEndPointStr: (homeScreenData?.productImage ?? ""))
            
            if (homeScreenData?.isFavroite ?? 0) == 0 {
                favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
            } else   {
                favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
            }
            if (homeScreenData?.adType ?? 0) == 1 {
                SellLbl.text = "Buy"
            } else {
                SellLbl.text = "Rent"
            }
        }
    }
    
    override func awakeFromNib() {
        self.favoriteButton.addTarget(self, action: #selector(tappedFavButton), for: .touchUpInside)
    }
    
    @objc
    func tappedFavButton(_ sender: UIButton) {
        guard let userInfo = Cookies.userInfo() else { return }
         var params = [String:Any]()
        
        if (homeScreenData?.isFavroite ?? 0) == FavoriteStatus.isUnfavorite.rawValue {
            params = ["user_id": userInfo.user_id, "ad_id": (homeScreenData?.adID ?? 0), "status": 1 ] as [String : Any]
        } else {
            params = ["user_id": userInfo.user_id, "ad_id": (homeScreenData?.adID ?? 0), "status": 0] as [String : Any]
        }
        delegate?.didTappedFavBtn(withParam: params, adId: (homeScreenData?.adID ?? 0))
    }
    
    
    func configure(product: ProductListIncoming.Body, categoryList: [CategoryListIncoming.Body]) {
        PrdNameLbl.text = product.title.leoSafe().firstUppercased
        LocationLbl.text = product.location.leoSafe()
        PriceLbl.text = "$\(product.price.leoSafe())"
        CntName.text = "\(product.username.leoSafe())"
        CtgName.text = "\(product.status.leoSafe())"
        
        
        self.PrdImage.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
        
        
        if product.is_favroite.leoSafe() == 0 {
            favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
        } else   {
            favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
        }
        if product.ad_type.leoSafe() == 1 {
            SellLbl.text = "Buy"
        } else {
            SellLbl.text = "Rent"
        }
        
        for cat in categoryList {
            if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                CtgName.text = cat.name.leoSafe()
                break
            }
        }
    }
}
