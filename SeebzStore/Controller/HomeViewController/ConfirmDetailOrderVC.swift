//
//  ConfirmDetailOrderVC.swift
//  seebzStore
//
//  Created by MAC on 23/12/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit

class ConfirmDetailOrderVC: UIViewController {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var productAddress: UILabel!
    @IBOutlet weak var producrRentOrPayLAbel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var houseNumberDeliveryLabel: UILabel!
    @IBOutlet weak var townDeliveryLabel: UILabel!
    @IBOutlet weak var countryDeliveryLabel: UILabel!
    @IBOutlet weak var stateDeliveryLabel: UILabel!
    @IBOutlet weak var streetDeliveryLabel: UILabel!
    @IBOutlet weak var cvcTextField: UITextField!
    @IBOutlet weak var cardExpiryTextField: UITextField!
    @IBOutlet weak var cardPinLabel: UITextField!
    
    
    
    
    var selectedSaveCardIndex = 0
    var selectedAddressIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        print(Singleton.shared.myDeliveryDetail[selectedAddressIndex])
        print(Singleton.shared.myCardDetail?.body?[selectedSaveCardIndex])
        self.showData()

    }
    func showData(){
        let adress = Singleton.shared.myDeliveryDetail[selectedAddressIndex]
        let card = Singleton.shared.myCardDetail?.body?[selectedSaveCardIndex]
        self.houseNumberDeliveryLabel.text = adress?.name.leoSafe()
        self.townDeliveryLabel.text = adress?.city.leoSafe()
        self.countryDeliveryLabel.text = adress?.address_line_1.leoSafe()
        self.stateDeliveryLabel.text = adress?.address_line_2.leoSafe()
        self.streetDeliveryLabel.text = "\(adress?.phone_no.leoSafe())"
//        self.cardExpiryTextField.text = "\(card?.expire_month ?? 0 + card?.expire_year ?? 0)"
        self.cardPinLabel.text = "\(card?.cart_no.leoSafe())"
        self.cvcTextField.text = "\(card?.cvv)"
        
//        houseNumLbl.text = "\(myDelivery.name.leoSafe())"
//        areaLbl.text = "\(myDelivery.city.leoSafe())" + "/" + "\(myDelivery.state.leoSafe())"
//        townLbl.text = "\(myDelivery.address_line_1.leoSafe())"
//        streetNoLbl.text = "\(myDelivery.address_line_2.leoSafe())"
//        countryLbl.text = "\(myDelivery.phone_no.leoSafe())"
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func payAction(_ sender: Any) {
    }
    
}
