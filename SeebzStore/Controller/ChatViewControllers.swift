//
//  ChatViewControllers.swift
//  seebzStore
//
//  Created by MAC on 24/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit
import SocketIO
import IBAnimatable

class ChatViewController: UIViewController {
    
    // MARK:- OUTLETS
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    var chatLists = [ChatListIncoming.Body]()
    var chatStoredLists = [ChatListIncoming.Body]()
    var searchtextField: UITextField = {
        let textField = UITextField()
        
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 3.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        
        textField.textAlignment = .center
        textField.placeholder = "Type User Name..."
        textField.textColor = .black
        textField.layer.cornerRadius = 20
        
        return textField
    }()
    
    let crossBtnImage = UIImage(named: "cross_grey")
    let searchBtnImage = UIImage(named: "Search")
    
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatListSocket()
        searchtextField.addTarget(self, action: #selector(searchingDidStarted), for: .editingChanged)
        searchButton.setImage(searchBtnImage, for: .normal)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.chatListSocket()

    }
    
    @objc
    func searchingDidStarted(_ sender: UITextField) {
        self.chatLists = self.chatStoredLists
        self.chatLists = self.chatLists.filter({($0.username ?? "").lowercased().contains((sender.text ?? "").lowercased())})
        
        if (sender.text ?? "" ) == "" {
            self.chatLists = self.chatStoredLists
        }
        self.tableView.reloadData()
    }
    
    // MARK:- IBACTIONS
    @IBAction func searchAction(_ sender: UIButton){
        if sender.imageView?.image == searchBtnImage {
            showSearchtextField()
            print("search")
            sender.setImage(crossBtnImage, for: .normal)
        } else {
            sender.setImage(searchBtnImage, for: .normal)
            print("hide")
            hidetextField()
        }
    }
    
    func showSearchtextField() {
        searchtextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(searchtextField)
        self.view.bringSubviewToFront(searchtextField)
        searchtextField.alpha = 0
        
        searchtextField.backgroundColor = UIColor.white
        searchtextField.topAnchor.constraint(equalToSystemSpacingBelow: self.view.topAnchor, multiplier: 12).isActive = true
        searchtextField.centerXAnchor.constraint(equalToSystemSpacingAfter: self.view.centerXAnchor, multiplier: 0).isActive = true
        searchtextField.widthAnchor.constraint(equalToConstant: (self.view.bounds.width / 1.2)).isActive = true
        self.searchtextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        searchtextField.layoutIfNeeded()
        
        searchtextField.becomeFirstResponder()
        
        UIView.animate(withDuration: 1) {
            self.searchtextField.alpha = 1
        }
    }
    
    func hidetextField() {
        self.searchtextField.text = ""
        UIView.animate(withDuration: 1, animations: {
            self.searchtextField.alpha = 0
        }) { (isCmpleted) in
            if isCmpleted {
                self.searchtextField.resignFirstResponder()
                self.searchtextField.removeFromSuperview()
            }
        }
    }
    
    // MARK:- SOCKET IMPLEMENTATION
    func checkSocket(completion: @escaping((Bool) -> ())) {
        if SocketIOManager.sharedInstance.socket != nil {
            if SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.disconnected || SocketIOManager.sharedInstance.socket!.status ==  SocketIOStatus.connecting {
                
                SocketIOManager.sharedInstance.establishConnection()
                //self.selectedCase.case_id.leoSafe()
                SocketIOManager.sharedInstance.connectedToSocketWithCompletionHandler { (ConnectedResponce) in
                    completion(true)
                }
            } else {
                completion(true)
            }
        }
    }
    
    func chatListSocket() {
        print(SocketIOManager.sharedInstance.socket?.status as Any)
        
        self.checkSocket { (isConnected) in
            if isConnected {
                var dict = [String:Any]()
                dict["user_id"] = Cookies.userInfo()!.user_id
                SocketIOManager.sharedInstance.emitChatList(dict: dict)
                SocketIOManager.sharedInstance.onChatList(user_id: Cookies.userInfo()!.user_id) { (chatListResp) in
                    
                    print("Chat List Response : \(chatListResp)")
                    
                    if let body = ChatListIncoming(dict: chatListResp).body {
                        self.chatLists = body
                        self.chatStoredLists = self.chatLists
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
}


extension ChatViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (chatLists.count) == 0 {
            tableView.setErrorMessage(withMessage: "No User Found.")
        } else {
            tableView.removeErrorMessage()
        }
        
        return chatLists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatListCell", for: indexPath) as! chatListCell
        cell.configure(chat: chatLists[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ChatVC") as! ChatVC
        vc.chatId = chatLists[indexPath.row].chat_id.leoSafe()
        vc.chatList = chatLists[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class chatListCell: UITableViewCell {
    @IBOutlet var chatImage: UIImageView!
    @IBOutlet var chatTitleLbl: UILabel!
    @IBOutlet var chatTimeLbl: UILabel!
    @IBOutlet var userNameLbl: UILabel!
    @IBOutlet var chatMessageLbl: UILabel!
    @IBOutlet var lastMsgCountLbl: AnimatableButton!
    
    func configure(chat: ChatListIncoming.Body) {
        chatTitleLbl.text = chat.title.leoSafe()
        userNameLbl.text = chat.username.leoSafe()
        chatMessageLbl.text = chat.last_msg.leoSafe()
        lastMsgCountLbl.titleLabel?.text = "\(chat.unread_msg.leoSafe())"
        chatImage.showImageUsingURL(urlEndPointStr: chat.product_image.leoSafe())
    }
}
