//
//  RentDetailVC.swift
//  seebzStore
//
//  Created by Deep Baath on 20/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire

class RentDetailVC: UIViewController {
    
    //MARK:- OUTLETS

    @IBOutlet var topLBl: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var dayTitleLbl: UILabel!
    @IBOutlet var dayTypeLbl: UILabel!
    @IBOutlet var dayAddressLbl: UILabel!
    @IBOutlet var remainingdayLbl: UILabel!
    @IBOutlet var dayAMountLbl: UILabel!
    @IBOutlet var dayRentLbl: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var rentUserNameLbl: UILabel!
    @IBOutlet var rentUserEmailLbl: UILabel!
    @IBOutlet var rentDateLbl: UILabel!
    @IBOutlet var seeProfileBtn: AnimatableButton!
    @IBOutlet weak var rentFavouriteBttn: UIButton!
    @IBOutlet weak var offerTopLabel: UILabel!
    @IBOutlet weak var offerDiscriptionLbl: UILabel!
    @IBOutlet weak var offerPriceLbl: UILabel!
    @IBOutlet var rentRightBtn: UIButton!
    @IBOutlet var rentLeftBtn: UIButton!
    @IBOutlet weak var acceptBttn: AnimatableButton!
    @IBOutlet weak var rejectBttn: AnimatableButton!
    @IBOutlet weak var proceedPaymentBttn: UIButton!
    @IBOutlet weak var paymentView: AnimatableView!
    @IBOutlet weak var rejectedLabel: UILabel!
    
    var selectedIndex = Int()
    weak var homeVC: HomeViewController?
    weak var myWishlistVC: MyWishlistVC?
    var product : ProductListIncoming.Body!
    var notification = [NotificationListModel.Body]()
    
    var favoriteProduct : FavoriteListIncoming.Body!
    var categoryList = [CategoryListIncoming.Body]()
    var productListIncoming = [ProductListIncoming.Body]()
    var ProductDetailList: HomeScreenProductDetail?
    var notificationProductDetail: NotificationInnerProductDetailModel?
    var buttonTap: String = ""
//    var noti : NotificationListModel!
//     var notification_id = Int()
   
    override func viewDidLoad() {
        super.viewDidLoad()
         self.pageControl.isHidden = false
//        if self.notification != nil {
//        //
//            let params = ["notification_id": notification[0].notification_id ]
//            self.gettingScreenDetail(params:params as [String : Any] )
//        //            self.gettingScreenDetail()
//                } else if self.favoriteProduct != nil {
//                    self.showFavDataOnUI()
//                }
        
        self.rentFavouriteBttn.isHidden = true
//
//
        if notification[0].status == 2 {
            acceptBttn.isHidden = true
            rejectBttn.isHidden = true
            proceedPaymentBttn.isHidden = false
            paymentView.isHidden = false
            rejectedLabel.isHidden = true
            
        }else if notification[0].status == 3{
            acceptBttn.isHidden = true
            rejectBttn.isHidden = true
            proceedPaymentBttn.isHidden = true
            paymentView.isHidden = false
            rejectedLabel.isHidden = false
        }else if notification[0].status == 4{
            acceptBttn.isHidden = true
            rejectBttn.isHidden = true
            proceedPaymentBttn.isHidden = true
            paymentView.isHidden = false
            rejectedLabel.text = "You accepted the request"
        }else if notification[0].status == 5 {
            acceptBttn.isHidden = true
                       rejectBttn.isHidden = true
                       proceedPaymentBttn.isHidden = true
                       paymentView.isHidden = false
                       rejectedLabel.text = "You rejected the request"
        }
        else{
            acceptBttn.isHidden = false
            rejectBttn.isHidden = false
            
        }
      
              
          let params = ["notification_id": notification[0].notification_id ]
                  self.gettingScreenDetail(params:params as [String : Any] )
        
      
    }
//    private func showFavDataOnUI() {
//          if let product = self.favoriteProduct {
//              dayTitleLbl.text = product.title.leoSafe()
//              dayAddressLbl.text = product.location.leoSafe()
//              descriptionTextView.text = product.description.leoSafe()
//              dayAMountLbl.text = "$\(product.amount.leoSafe())"
//              rentUserNameLbl.text = product.username.leoSafe().capitalized
//              rentUserEmailLbl.text = product.email.leoSafe()
//              rentDateLbl.text = product.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
//
//              for cat in categoryList {
//                  if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
//                      dayTypeLbl.text = cat.name.leoSafe()
//                      break
//                  }
//              }
//
//              if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//                  rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//              } else {
//                  rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//              }
//              if product.ad_type.leoSafe() == 1 {
//                  self.remainingdayLbl.isHidden = true
//                  dayRentLbl.text = "Sell"
//              } else {
//                  self.remainingdayLbl.isHidden = false
//                  self.remainingdayLbl.text = "\(product.ad_days.leoSafe()) Days"
//                  dayRentLbl.text = "Rent/day"
//              }
//          }
//      }
//
    
    
    // MARK:- API IMPLEMENTATION
    
    func gettingScreenDetail(params: [String:Any]) {
        Hud.show(message: "Loading...", view: self.view)
        self.screenDetailApi(url: .notification_Product_Detail, jsonObject: params)

    }
        
        
        
    func screenDetailApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post) {

            WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
                     print(dict)
                     Hud.hide(view: self.view)
                     
                     if isSuccess(json: dict) {
                         // SUCCESS
                         
                         if url == .notification_Product_Detail {
                            
                            
                            self.notificationProductDetail =  NotificationInnerProductDetailModel(dict: dict)

                   if let info = self.notificationProductDetail?.body?[0] {


                    self.collectionView.reloadData()

                    self.dayTitleLbl.text = info.product_ads?.title.leoSafe()
                    self.dayAddressLbl.text = info.product_ads?.location.leoSafe()
                    self.descriptionTextView.text = info.product_ads?.pro_description.leoSafe()
                    self.dayAMountLbl.text = "$" + "\(info.product_ads?.real_price.leoSafe() ?? 0)"
                    self.rentUserNameLbl.text = info.product_ads?.username.leoSafe().capitalized
                    self.rentUserEmailLbl.text = info.product_ads?.email.leoSafe()
                    self.offerPriceLbl.text = "$" + "\(info.product_ads?.set_amount.leoSafe() ?? 0)"
                    self.rentDateLbl.text = info.product_ads?.created_at?.leoDateString(toFormat: "dd/MM/yyyy")
                    self.offerDiscriptionLbl.text = info.product_ads?.description.leoSafe()
                    
//                    for cat in self.categoryList {
//                        if cat.cat_id.leoSafe() == self.product.cat_id.leoSafe() {
//                            self.dayTypeLbl.text = cat.name.leoSafe()
//                                       break
//                                   }
//                               }
//
//                    if self.product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//                        self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//                               } else {
//                        self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                               }
                    if info.product_ads?.ad_type.leoSafe() == 1 {
                                   self.remainingdayLbl.isHidden = true
                        self.dayRentLbl.text = "Sell"
                               } else {
                                   self.remainingdayLbl.isHidden = false
//                        self.remainingdayLbl.text = "\(info.product_ads?.ad_days.leoSafe()) Days"
                        self.dayRentLbl.text = "Rent/day"
                               }
                    
                    if info.product_images?.count == 1{
                        self.pageControl.isHidden = true
                        self.rentRightBtn.isHidden = true
                        self.rentLeftBtn.isHidden = true
                        
                        
                    }else{
                        self.pageControl.isHidden = false
                        self.rentRightBtn.isHidden = false
                        self.rentLeftBtn.isHidden = false
                    }
                    self.pageControl.numberOfPages = (self.notificationProductDetail?.body?[0].product_images!.count)!

       }
                         }
                     }
                     else {
                         // FAILURE
                         Alert.showSimple(message(json: dict))
                     }
                 }) { (errorJson) in
                     Hud.hide(view: self.view)
                     print(errorJson)
                 }
        }
    
    
    

       func AcceptRejectApi(){
        
         guard let product = self.notificationProductDetail?.body?[0] else { return }
            var params = [String: Any]()
        
        if buttonTap == "acceptBtn"{
            
            params["booking_by"] = product.product_ads?.booking_by
            params["product_id"] = product.product_ads?.product_ad_id.leoSafe()
            params["amount"] = product.product_ads?.set_amount.leoSafe()
            params["description"] = product.product_ads?.description.leoSafe()
            params["start_date"] = product.product_ads?.start_date.leoSafe()
            params["ad_type"] = product.product_ads?.ad_type.leoSafe()
            params["user_id"] = product.product_ads?.user_id.leoSafe()
            params["end_date"] = product.product_ads?.end_date.leoSafe()
            params["ad_id"] = product.product_ads?.ad_id.leoSafe()
            params["status"] = 2
            params["notification_id"] = notification[0].notification_id
            
        }else{
            
            
            params["booking_by"] = product.product_ads?.booking_by
            params["product_id"] = product.product_ads?.product_ad_id.leoSafe()
            
            params["status"] = 3
            params["notification_id"] = notification[0].notification_id
            
            
        }
              
             
                    Hud.show(message: "Loading...", view: self.view)
                   WebServices.post(url: .user_Purchase_Product_Requset_Accepet_Or_Reject, jsonObject: params, method: .post, completionHandler: { (dict) in
                       print("Response")
                         Hud.hide(view: self.view)
                       
                           if let success = dict["success"] as? Int {
                               if success == 200 {
                                   
                       let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                                           
                                   
                       self.navigationController?.pushViewController(tabBarVC, animated: true)
           //
                               }
                           } else
                       {
                           Alert.showSimple(message(json: dict))
                       }
                   
                   }) { (error) in
                         Hud.hide(view: self.view)
                       
                   }
                   
               }
    
    
    
    
//    func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
//        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
//            print(dict)
//            Hud.hide(view: self.view)
//
//            if isSuccess(json: dict) {
//                // SUCCESS
//
//                if url == .addFavroite {
//
//                    if let homeVC = self.homeVC {
//                        if homeVC.productListIncoming[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
//                            homeVC.productListIncoming[self.selectedIndex].is_favroite = 1
//                            self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        } else {
//                            homeVC.productListIncoming[self.selectedIndex].is_favroite = 0
//                            self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//                        }
//                        homeVC.tableview.reloadData()
//                    }
//                    if let myWishlistVC = self.myWishlistVC {
//                        if myWishlistVC.favoriteList[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
//                            myWishlistVC.favoriteList[self.selectedIndex].is_favroite = 1
//                            self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//                        } else {
//                            myWishlistVC.favoriteList[self.selectedIndex].is_favroite = 0
//                            self.rentFavouriteBttn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//                        }
//                        myWishlistVC.tableView.reloadData()
//
//                    }
//
//                }
//            }
//            else {
//                // FAILURE
//                Alert.showSimple(message(json: dict))
//            }
//        }) { (errorJson) in
//            Hud.hide(view: self.view)
//            print(errorJson)
//        }
//    }
//
//
    
    
    
    
    //MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareAction(_ sender: Any) {
        let adTitle = notificationProductDetail?.body?[0].product_ads?.title.leoSafe()
                  let activityViewController = UIActivityViewController(activityItems: [adTitle], applicationActivities: nil)
                  activityViewController.popoverPresentationController?.sourceView = self.view
                  self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func actionAcceptBttn(_ sender: Any) {
        buttonTap = "acceptBtn"
        AcceptRejectApi()
        
    }
    
    
    @IBAction func actionRejectBttn(_ sender: Any) {
        buttonTap = "rejectBtn"
        AcceptRejectApi()
    }
    
    
    
    @IBAction func seeProfileAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "RentProfileVC") as! RentProfileVC
        
        VC.notificationProductDetail = notificationProductDetail
        VC.productListIncoming = productListIncoming
        VC.categoryList = categoryList
        VC.ProductDetailList = ProductDetailList
              
        navigationController?.pushViewController(VC, animated: true)
        
        
    }
    @IBAction func rightBttnAction(_ sender: Any) {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
               let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
               let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
               if nextItem.row < (self.notificationProductDetail?.body?[0].product_images![0].product_image?.count)! {
                   self.collectionView.scrollToItem(at: nextItem, at: .left, animated: true)

               }
        
    }
    
    @IBAction func leftBttnAction(_ sender: Any) {
        let visibleItems: NSArray = self.collectionView.indexPathsForVisibleItems as NSArray
                  let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
                  let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
                  if nextItem.row < (self.notificationProductDetail?.body?[0].product_images![0].product_image?.count)! && nextItem.row >= 0{
                      self.collectionView.scrollToItem(at: nextItem, at: .right, animated: true)

                  }
        
    }
    
     
    @IBAction func chatAction(_ sender: Any) {
    }
    
    
    @IBAction func paymentAction(_ sender: Any) {
        
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryInfoVC") as! DeliveryInfoVC
               
            
                     
               navigationController?.pushViewController(VC, animated: true)
        
        
    }
    
    
    
    
}

//MARK:- EXTENSION
extension RentDetailVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        
        return CGSize(width: width, height: height)
        
    }
    
   // func numberOfSections(in collectionView: UICollectionView) -> Int {
  ////      return 1
  //  }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationProductDetail?.body?[0].product_images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rentDetailCell", for: indexPath) as! rentDetailCell
        
        if self.notificationProductDetail?.body?[0].product_images != nil {
                   cell.configure(product: (self.notificationProductDetail?.body?[0].product_images![indexPath.row])!)
               }
               if self.favoriteProduct != nil {
                   cell.configure(product: (self.notificationProductDetail?.body?[0].product_images![indexPath.row])!, favProduct: favoriteProduct)
               }
           
           return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
           pageControl.currentPage = indexPath.row
       }
       
}

//MARK:- OUTLETS

class rentDetailCell: UICollectionViewCell {
    
    @IBOutlet var rentBackImg: UIImageView!
    @IBOutlet var rentRightBtn: UIButton!
    @IBOutlet var rentLeftBtn: UIButton!
    @IBOutlet var rentFavBtn: UIButton!
    
    
    
    

        func configure(product: NotificationInnerProductDetailModel.Body.Product_Images, favProduct: FavoriteListIncoming.Body? = nil) {
    //        let productt = product.body?[0]
            if product != nil {
                rentBackImg.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
                
                
                
                
            } else if favProduct != nil {
                rentBackImg.showImageUsingURL(urlEndPointStr: favProduct!.product_image.leoSafe())
            }

        }
    

}
    
 
