//
//  MyWishlistVC.swift
//  seebzStore
//
//  Created by Deep Baath on 20/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import Alamofire

class MyWishlistVC: UIViewController {
    
    //MARK:- OUTLETS
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var categoryList = [CategoryListIncoming.Body]()
    var favoriteList = [FavoriteListIncoming.Body]()
    var favoriteStoredList = [FavoriteListIncoming.Body]()
    var selectedIndex = Int()
    var isScreenComeFrom : ScreenComeFrom = .none

    
    var searchtextField: UITextField = {
        let textField = UITextField()
        
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 3.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        
        textField.textAlignment = .center
        textField.placeholder = "Type Product Name..."
        textField.textColor = .black
        textField.layer.cornerRadius = 20
        
        return textField
    }()
    
    
     let crossBtnImage = UIImage(named: "cross_grey")
     let searchBtnImage = UIImage(named: "Search")
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let userInfo = Cookies.userInfo() else { return }
        let params = ["user_id": userInfo.user_id]
        self.apiCalled(url: .favroiteList, jsonObject: params, method: .post)
        searchtextField.addTarget(self, action: #selector(searchingStarted), for: .editingChanged)
    }
    override func viewWillAppear(_ animated: Bool) {
        guard let userInfo = Cookies.userInfo() else { return }
        let params = ["user_id": userInfo.user_id]
        self.apiCalled(url: .favroiteList, jsonObject: params, method: .post)
    }
    
    @objc
    func searchingStarted(_ sender: UITextField) {
        self.favoriteList = self.favoriteStoredList
        
        self.favoriteList = self.favoriteList.filter({($0.title ?? "").lowercased().contains((sender.text ?? "").lowercased())})
        
        if (sender.text ?? "") == "" {
            self.favoriteList = self.favoriteStoredList
        }
        
        self.tableView.reloadData()
    }
    
    func showSearchtextField() {
        searchtextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(searchtextField)
        self.view.bringSubviewToFront(searchtextField)
        searchtextField.alpha = 0
        
        searchtextField.backgroundColor = UIColor.white
        searchtextField.topAnchor.constraint(equalToSystemSpacingBelow: self.view.topAnchor, multiplier: 12).isActive = true
        searchtextField.centerXAnchor.constraint(equalToSystemSpacingAfter: self.view.centerXAnchor, multiplier: 0).isActive = true
        searchtextField.widthAnchor.constraint(equalToConstant: (self.view.bounds.width / 1.2)).isActive = true
        self.searchtextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        searchtextField.layoutIfNeeded()
        
        searchtextField.becomeFirstResponder()
        
        UIView.animate(withDuration: 1) {
            self.searchtextField.alpha = 1
        }
    }
    
    func hidetextField() {
        self.searchtextField.text = ""
        UIView.animate(withDuration: 1, animations: {
            self.searchtextField.alpha = 0
        }) { (isCmpleted) in
            if isCmpleted {
                self.searchtextField.resignFirstResponder()
                self.searchtextField.removeFromSuperview()
            }
        }
    }
    
    // MARK:- API IMPLEMENTATION
    func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
        Hud.show(message: "Loading...", view: self.view)
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                
                if url == .favroiteList {
                    if let body = FavoriteListIncoming(dict: dict).body {
                        self.favoriteList = body
                        self.favoriteStoredList = self.favoriteList
                        self.tableView.reloadData()
                    }
                } else if url == .addFavroite {
                    if self.favoriteList[self.selectedIndex].is_favroite == FavoriteStatus.isUnfavorite.rawValue {
                        self.favoriteList[self.selectedIndex].is_favroite = 1
                    } else {
                        self.favoriteList[self.selectedIndex].is_favroite = 0
                    }
                    self.tableView.reloadData()
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
    
    
    //MARK:- ACTION
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: UIButton){
        if sender.imageView?.image == searchBtnImage {
            showSearchtextField()
            print("search")
            sender.setImage(crossBtnImage, for: .normal)
        } else {
            sender.setImage(searchBtnImage, for: .normal)
            print("hide")
            hidetextField()
        }
    }
}

//MARK:- EXTENSION CLASS
extension MyWishlistVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (favoriteList.count) == 0 {
            tableView.setErrorMessage(withMessage: "No Data Found.")
        } else {
            tableView.removeErrorMessage()
        }
        
        
        return favoriteList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wishlistCell", for: indexPath) as! wishlistCell
        cell.configure(product: favoriteList[indexPath.row], categoryList: categoryList)
        cell.clickWishBtn.tag = indexPath.row
        cell.clickWishBtn.addTarget(self, action: #selector(favoriteBtnTapped(button:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        //detailVC.homeVC = self
        detailVC.selectedIndex = indexPath.row
        detailVC.categoryList = self.categoryList
        detailVC.favoriteProduct = self.favoriteList[indexPath.row]
        detailVC.product_id = self.favoriteList[indexPath.row].product_id ?? 0
        detailVC.isScreenComeFrom = .wishList
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @objc func favoriteBtnTapped(button: UIButton) {
        
        guard let userInfo = Cookies.userInfo() else { return }
        let index = button.tag
        let selectedProduct = self.favoriteList[index]
        
        var params = [String:Any]()
        
        if selectedProduct.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
            params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": FavoriteStatus.isFavorite.rawValue] as [String : Any]
            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
        } else {
            params = ["user_id": userInfo.user_id, "ad_id": selectedProduct.ad_id.leoSafe(), "status": FavoriteStatus.isUnfavorite.rawValue] as [String : Any]
            self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
        }
        
        self.selectedIndex = index
        //        self.apiCalled(url: .addFavroite, jsonObject: params, method: .post)
    }
}

// MARK:- OUTLET CELL
class wishlistCell: UITableViewCell {
    @IBOutlet weak var wishImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var wishTypeLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var clickWishBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    func configure(product: FavoriteListIncoming.Body, categoryList: [CategoryListIncoming.Body]) {
        titleLbl.text = product.title.leoSafe().firstUppercased
        addressLbl.text = product.location.leoSafe()
        userNameLbl.text = product.username
        wishImage.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
        
        for cat in categoryList {
            if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                wishTypeLbl.text = cat.name.leoSafe()
                break
            }
        }
        
        if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
            clickWishBtn.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
        } else {
            clickWishBtn.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
        }
    }
}


