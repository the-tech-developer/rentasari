//
//  NotificationVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire

class NotificationVC: UIViewController {
    
//MARK:- OUTLETS
    @IBOutlet var tableView: UITableView!
    
    var notification = [NotificationListModel.Body]()
    var selectedIndex = Int()
    var product : ProductListIncoming.Body!
    var categoryList = [CategoryListIncoming.Body]()
    var productListIncoming = [ProductListIncoming.Body]()
    var ProductDetailList: HomeScreenProductDetail?
    var isScreenComeFrom : ScreenComeFrom = .none

    override func viewDidLoad() {
        super.viewDidLoad()
        self.gettingMyNotificationDetail()
        tableView.separatorStyle = .none
    }
    
    
    
    func gettingMyNotificationDetail() {
                 if let userInfo = Cookies.userInfo() {
                     Hud.show(message: "Loading...", view: self.view)
                     let params = ["user_id": userInfo.user_id]
                     self.addAPI(url: .notification, jsonObject: params)
                 }
             }
    
    func addAPI(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
             Hud.hide(view: self.view)
            
              if isSuccess(json: dict) {
                                   // SUCCESS
                                   
                                   if url == .notification {
                                       
//                                    self.tableView.reloadData()
//
                                       if let body = NotificationListModel(dict: dict).body {
                                    
                                            self.notification = body
                                            self.tableView.reloadData()

                                          
                                       } else {

                                       }
                                   }
                               }else {
                                   // FAILURE
                                   Alert.showSimple(message(json: dict))
                               }
            
        }) { (error) in
            
             Hud.hide(view: self.view)
        }
    }
    
    
    
    //MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
    }
}
    

//MARK:- EXTENSION CLASS
extension NotificationVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (notification.count) == 0 {
            tableView.setErrorMessage(withMessage: "No Notifications found.")
        } else {
            tableView.removeErrorMessage()
        }
        
        return notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        cell.configure(myNotification: notification[indexPath.row])
        cell.notiNextBttn.isHidden = false
         return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "RentDetailVC") as! RentDetailVC
              //detailVC.homeVC = self
              detailVC.selectedIndex = indexPath.row
        detailVC.product = product
        detailVC.productListIncoming = productListIncoming
        detailVC.categoryList = categoryList
        detailVC.ProductDetailList = ProductDetailList
//        detailVC.iss
        detailVC.notification = [self.notification[indexPath.row]]
//              detailVC.favoriteProduct = self.favoriteList[indexPath.row]
              self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

class notificationCell: UITableViewCell {
    
    //MARK:- OUTLETS
    @IBOutlet var notiLeftImg: AnimatableImageView!
    @IBOutlet var notiTitleLbl: UILabel!
    @IBOutlet var notiAmountBtn: UIButton!
    @IBOutlet var notiSellBuyLbl: UILabel!
    @IBOutlet var notiTypeLbl: UILabel!
    @IBOutlet var notiAddressLbl: UILabel!
    @IBOutlet var notiAcceptBtn: UIButton!
    @IBOutlet var notiNextBttn: UIButton!
    
    @IBOutlet weak var notiProductName: UILabel!
    
    
    
    var notification: NotificationListModel!
    
    
    
    
    func configure(myNotification: NotificationListModel.Body) {
        self.notification?.body? = [myNotification]
//        notiProductName.text = "\(myNotification.title.leoSafe().capitalized)"
        notiTitleLbl.text = "\(myNotification.name.leoSafe().capitalized)"
        notiLeftImg.showImageUsingURL(urlEndPointStr: myNotification.image.leoSafe())
        
        
        
          
           
       }
    
    
}
