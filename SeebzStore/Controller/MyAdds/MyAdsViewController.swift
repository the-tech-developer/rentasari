//
//  MyAdsViewController.swift
//  seebzStore
//
//  Created by IOS on 22/01/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation
import UIKit
import IBAnimatable
import Alamofire
import DropDown

class MyAdsViewController: UIViewController {
    
    //MARK:- Outlets
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var noAdsView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    
    //search textField
    var searchtextField: UITextField = {
        let textField = UITextField()
        
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 3.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        textField.layer.shadowOpacity = 1.0
        
        textField.textAlignment = .center
        textField.placeholder = "Type Product Name..."
        textField.textColor = .black
        textField.layer.cornerRadius = 20
        
        return textField
    }()
    
    
    //MARK:- variables
    
    let crossBtnImage = UIImage(named: "cross_grey")
    let searchBtnImage = UIImage(named: "Search")
    
    
    var myAds: MyAds? = []
    var myStoredAds: MyAds? = []
    var product = [ProductListIncoming.Body]()

    var anchorView: AnchorView?
    var refreshControl = UIRefreshControl()
    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBtn.addTarget(self, action: #selector(tappedSearchBtn), for: .touchUpInside)
        searchBtn.setImage(searchBtnImage, for: .normal)
        
        searchtextField.addTarget(self, action: #selector(startSearching), for: .editingChanged)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        getAllMyAds()
        tableview.addSubview(refreshControl)
    }
    
    // MARK:- @OBJC FUNCTIONS
    @objc func refresh(sender:AnyObject) {
        //self.gettingMyAds()
        getAllMyAds()
    }
    
    @objc
    func startSearching(_ sender: UITextField) {
        self.myAds = self.myStoredAds
        self.myAds = self.myAds?.filter({($0.title ?? "").lowercased().contains((sender.text ?? "").lowercased())})
        
        if (sender.text ?? "" ) == "" {
            self.myAds = self.myStoredAds
        }
        
        self.tableview.reloadData()
    }
    
    @objc
    func tappedSearchBtn(_ sender: UIButton) {
        if sender.imageView?.image == searchBtnImage {
            showSearchtextField()
            print("search")
            sender.setImage(crossBtnImage, for: .normal)
        } else {
            sender.setImage(searchBtnImage, for: .normal)
            print("hide")
            hidetextField()
        }
    }
    
    //MARK:- Custom Functions
    
    func gettingMyAds() {
        if let userInfo = Cookies.userInfo() {
            Hud.show(message: "Loading...", view: self.view)
            let params = ["user_id": userInfo.user_id]
            self.apiCalled(url: .MYads, jsonObject: params)
        }
    }
    
    // MARK:- IBACTIONS
    @IBAction func startSellingAction(sender:AnimatableButton) {
        /* if let createVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateViewController") as? CreateViewController {
         createVC.categoryList = categoriess
         self.navigationController?.pushViewController(createVC, animated: true)
         }*/
    }
    
    //    @IBAction func reload(sender:AnimatableButton) {
    //        self.gettingMyAds()
    //
    //    }
    //
    
    
    func publishDisableApi(url: Api, jsonObject: [String:Any], method: HTTPMethod) {
        Hud.show(message: "Loading...", view: self.view)
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                // SUCCESS
                if url == .MYads {
                    let model = ProductListIncoming(dict: dict).body
                        if let body = model{

                            self.product = body
                            Singleton.shared.product = self.product
                            
                        self.tableview.reloadData()
                        }
                    
                } else if url == .disable_OR_publish_MyAds {
                    
                    self.gettingMyAds()
                    self.tableview.reloadData()
                    
                }
            }
            else {
                // FAILURE
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
    
    func showSearchtextField() {
        searchtextField.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(searchtextField)
        self.view.bringSubviewToFront(searchtextField)
        searchtextField.alpha = 0
        
        searchtextField.backgroundColor = UIColor.white
        searchtextField.topAnchor.constraint(equalToSystemSpacingBelow: self.view.topAnchor, multiplier: 7).isActive = true
        searchtextField.centerXAnchor.constraint(equalToSystemSpacingAfter: self.view.centerXAnchor, multiplier: 0).isActive = true
        searchtextField.widthAnchor.constraint(equalToConstant: (self.view.bounds.width / 2)).isActive = true
        self.searchtextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        searchtextField.layoutIfNeeded()
        
        searchtextField.becomeFirstResponder()
        
        UIView.animate(withDuration: 1) {
            self.searchtextField.alpha = 1
        }
    }
    
    func hidetextField() {
        self.searchtextField.text = ""
        UIView.animate(withDuration: 1, animations: {
            self.searchtextField.alpha = 0
        }) { (isCmpleted) in
            if isCmpleted {
                self.searchtextField.resignFirstResponder()
                self.searchtextField.removeFromSuperview()
            }
        }
    }
    
    func showPermissionAlertWithBtn(title:String = "",message:String,preferredStyle: UIAlertController.Style? = .alert , senderTag: Int , productId: Int) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            print("Ok button tapped")
            let param = ["product_id" : productId]
            
            self.deleteAddApi(url: .Delete_Create_Add, jsonObject: param as [String : Any])
        }
        alertController.addAction(OKAction)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped")
        }
        alertController.addAction(cancelAction)
        
        // Present Dialog message
        UIApplication.topViewController()?.present(alertController, animated: true, completion:nil)
    }

    func deleteAddApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            if let success = dict["success"] as? Int {
                if success == 200 {
                    self.gettingMyAds()
                }else{
                    Alert.showSimple(message(json: dict))
                }
            }
        }) { (error) in
            Hud.hide(view: self.view)
        }
    }
    
    // MARK:- API IMPLEMENTATION
    func apiCalled(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post) {
        Hud.show(message: "Loading..", view: self.view)
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            
            if isSuccess(json: dict) {
                self.getAllMyAds()
            }
            else {
                // FAILURE
                self.refreshControl.endRefreshing()
                self.noAdsView.isHidden = false
                Alert.showSimple(message(json: dict))
            }
        }) { (errorJson) in
            self.refreshControl.endRefreshing()
            self.noAdsView.isHidden = false
            Hud.hide(view: self.view)
            print(errorJson)
        }
    }
}


// MARK:- TABLE VIEW DATA SOURCE & DELEGATE METHODS
extension MyAdsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 141
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (myAds?.count ?? 0)  == 0 {
            tableView.setErrorMessage(withMessage: "No Product found.")
        } else {
            tableView.removeErrorMessage()
        }
        
        return (myAds?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddsTableViewCell", for: indexPath) as! MyAddsTableViewCell
        
        cell.add = myAds?[indexPath.row]
        cell.delegate = self
        
        return cell
    }
}


extension MyAdsViewController {
    
    func getAllMyAds() {
        
        if let userInfo = Cookies.userInfo() {
//            Hud.show(message: "Loading...", view: self.view)
            self.refreshControl.beginRefreshing()
            let params = ["user_id": userInfo.user_id]
            WebServices.post(url: .MYads, jsonObject: params, method: .post, completionHandler: { (dict) in
                self.refreshControl.endRefreshing()
                //Hud.hide(view: self.view)
                if isSuccess(json: dict) {
                // SUCCESS
                        
                    if let body = ProductListIncoming(dict: dict).body {
                        self.refreshControl.endRefreshing()
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: dict["body"] as Any, options: .prettyPrinted)
                            let decodedJson = try JSONDecoder().decode(MyAds.self, from: jsonData)
                            self.myAds = decodedJson
                            self.myStoredAds = decodedJson
                            if self.myAds?.count == 0 {
                                self.noAdsView.isHidden = false
                            } else {
                                self.noAdsView.isHidden = true
                            }
                            
                            self.tableview.reloadData()
                            
                            Singleton.shared.product = body
                            
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                } else {
                    Alert.showSimple((dict["message"] as? String ?? ""))
                }
            }) { (error) in
                self.refreshControl.endRefreshing()
                //Hud.hide(view: self.view)
                Alert.showSimple(message(json: error))
            }
        }
    }
}

extension MyAdsViewController: Deleteable {
    func didTappedDeleteBtn(withData data: Any, andType type: String) {
        
        if type == "Delete" {
            guard let productId = data as? Int else { return }
            self.showPermissionAlertWithBtn(title: "RentASari", message: "Are you sure you want to delete this product?", preferredStyle: .alert, senderTag: productId, productId: productId)
        }
    }
}
