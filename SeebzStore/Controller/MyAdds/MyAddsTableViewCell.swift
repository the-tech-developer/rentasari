//
//  MyAddsTableViewCell.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import UIKit
import IBAnimatable
import Alamofire
import DropDown


protocol Deleteable: class {
    func didTappedDeleteBtn(withData data: Any , andType type: String)
}

class MyAddsTableViewCell: UITableViewCell {
    
    //MARK:- OUTLETS
    @IBOutlet weak var adsImg: AnimatableImageView!
    @IBOutlet weak var AdnameLbl: UILabel!
    @IBOutlet weak var AdcategoryLbl: UILabel!
    @IBOutlet weak var AdpriceBtn: UIButton!
    @IBOutlet weak var AdpublishBtn: UIButton!
    @IBOutlet weak var AdthreeDotBtn: UIButton!
    @IBOutlet weak var AdlocationLbl: UILabel!
    @IBOutlet weak var AdsellrentLbl: UILabel!
    
    weak var myAdsVC: MyAdsViewController?
    var product : ProductListIncoming.Body!

    weak var delegate: Deleteable?
    var dropDown = DropDown()
    var add: MyAd? {
        didSet {
            
            adsImg.showImageUsingURL(urlEndPointStr: (add?.productImage ?? ""))
            AdnameLbl.text = (add?.title ?? "").capitalized
            AdlocationLbl.text = (add?.location ?? "")
            AdcategoryLbl.text = (add?.catname ?? "")
            AdpriceBtn.setTitle("$ \(add?.price ?? 0)", for: .normal)
            
            if (add?.adType ?? 0) == 1 {
                AdsellrentLbl.text = "Sell"
            } else {
                AdsellrentLbl.text = "Rent"
            }
            
            if (add?.status ?? 0) == PublishDisableStatus.Disasble.rawValue {
                AdpublishBtn.setTitle("Disable", for: .normal)
                AdpublishBtn.backgroundColor? = .gray
            } else {
                AdpublishBtn.setTitle("Publish", for: .normal)
                AdpublishBtn.backgroundColor? = COLORS.MAIN_BLUE
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        AdthreeDotBtn.addTarget(self, action: #selector(tappedThreeDotsBtn), for: .touchUpInside)
        AdpublishBtn.addTarget(self, action: #selector(tapppedPublishedBtn), for: .touchUpInside)
        
        dropDown.anchorView = AdthreeDotBtn
        dropDown.dataSource = ["Edit", "Delete"]
        dropDown.width = 200
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0 {
                //Edit
                self?.dropDown.show()
                let createVC = self?.viewContainingController()?.storyboard?.instantiateViewController(withIdentifier: "CreateViewController") as! CreateViewController
                
                createVC.isScreenComeFrom = .editAd
                createVC.product = Singleton.shared.product
                createVC.categoryList = categoriess
                createVC.index = index
                self?.viewContainingController()?.navigationController?.pushViewController(createVC, animated: true)
            }
            else{
                self?.delegate?.didTappedDeleteBtn(withData: (self?.add?.productID ?? 0), andType: "Delete")
            }
        }
    }
    
    
    //MARK:- OBJC Functions
    @objc
    func tappedThreeDotsBtn(_ sender: UIButton) {
        dropDown.show()
    }
    
    @objc
    func tapppedPublishedBtn(_ sender: UIButton) {
        print("tapped")
        guard let contorllerRef = self.viewContainingController() as? MyAdsViewController else { return }
        
        var param = ["ad_id" : (add?.adID ?? 0)]
        if (add?.status ?? 0) == PublishDisableStatus.Disasble.rawValue {
            param.updateValue(0, forKey: "status")
        } else {
            param.updateValue(1, forKey: "status")
        }
        
        contorllerRef.apiCalled(url: .disable_OR_publish_MyAds, jsonObject: param)
    }
}
