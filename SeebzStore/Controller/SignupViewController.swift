//
//  SignupViewController.swift
//  seebzStore
//
//  Created by Deep Baath on 18/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import FlagPhoneNumber

class SignupViewController: UIViewController {
    
    // MARK:- OUTLETS
    @IBOutlet weak var bottomPrivacyLbl: UILabel!
    @IBOutlet weak var bottomLoginLbl: UILabel!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var mobileNumTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var passwordShowBtn: UIButton!
    @IBOutlet weak var clickTermBtn: UIButton!
    
    // MARK:- VARIABLES
    var currentAddress : RASAddress!
    var mobileNumber = ""
    var isTermsChecked: Bool = false

    
    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileNumTF.delegate = self
    }
   
    func bottomTitles(){
        let strSignup = "New here? Sign Up Instead"
        bottomPrivacyLbl.font = UIFont.boldSystemFont(ofSize: 17.0)
        let attributedString  = NSMutableAttributedString(string: "Login here?", attributes: [NSAttributedString.Key.underlineStyle : true])

        self.bottomPrivacyLbl.attributedText = attributedString
          
//          tf_firstName.placeholderColor(color: UIColor.lightGray)
//          tf_lastName.placeholderColor(color: UIColor.lightGray)
//          tf_password.placeholderColor(color: UIColor.lightGray)
//          tf_email.placeholderColor(color: UIColor.lightGray)
//          tf_confirmPassword.placeholderColor(color: UIColor.lightGray)
      }
    
    // MARK:- ACTION
    @IBAction func clickSignupAction(_ sender: UIButton){        
        self.signupAPI()
    }
    
    @IBAction func clikAgreeAction(_ sender: UIButton){
        if isTermsChecked {
            self.isTermsChecked = false
            clickTermBtn.setImage(#imageLiteral(resourceName: "uncheck"), for: .normal)
        } else {
            self.isTermsChecked = true
            clickTermBtn.setImage(#imageLiteral(resourceName: "check"), for: .normal)
        }
    }
    
    @IBAction func clickTermLabelAction(_ sender: UIButton){
        
    }
    
    @IBAction func goToLoginAction(_ sender: UIButton){ self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSecurePassword(_ sender: UIButton) {
        if passwordTF.isSecureTextEntry {
            passwordTF.isSecureTextEntry = false
        } else {
            passwordTF.isSecureTextEntry = true
        }
    }
    
    @IBAction func actionLocation(_ sender: UIButton) {
        isScreenComeFrom = .signUp
        let locationVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationVC") as! LocationVC
        locationVC.signupVCDelagate = self
        locationVC.isScreenComeFrom = .signUp
        locationVC.closureDidGetAddress = { currentAddress in
            self.currentAddress = currentAddress
            self.addressTF.text = currentAddress.address
        }
        self.navigationController?.pushViewController(locationVC, animated: true)
    }
    
    // MARK:- API IMPLEMENTATION
    func signupAPI(){
        if !userNameTF!.length(){
            Alert.showSimple("Please enter username.")
            return
        }
        if !mobileNumTF!.length(){
            Alert.showSimple("Please enter phone number.")
            return
        }
        if !emailTF!.length(){
            Alert.showSimple("Please enter Email Address.")
            return
        }
        if !passwordTF!.length(){
            Alert.showSimple("Password length should be in greater than 6.")
            return
        }
        if !addressTF!.length() {
            Alert.showSimple("Please enter the address.")
            return
        }
        if clickTermBtn.imageView?.image == UIImage(named:"uncheck") {
            Alert.showSimple("Please accept the terms & conditions.")
            return
        }
        
        Hud.show(message: "", view: self.view)
        var SignupParams = [String: Any]()
        SignupParams["first_name"] = userNameTF.text.leoSafe()
        SignupParams["last_name"] =  userNameTF.text.leoSafe()
        SignupParams["email"] = emailTF.text.leoSafe()
        SignupParams["mobile"] = mobileNumTF.text.leoSafe()
        SignupParams["password"] = passwordTF.text.leoSafe()
        SignupParams["country_code"] = +91 as Any
        SignupParams["address"] = addressTF.text.leoSafe()
        SignupParams["device_type"] = "ios"
        SignupParams["device_token"] = "123"
        
        Hud.show(message: "Loading...", view: self.view)
        
        WebServices.post(url: .signup, jsonObject: SignupParams, method: .post, completionHandler: { (dict) in
            print("Result",dict)
            
              Hud.hide(view: self.view)
              if let success = dict["success"] as? Int {
                  if success == 200 {
                      let userDict = UserLogin(dict: dict)
                      Cookies.userInfoSave(dict: userDict.user?.first?.serverData)
                      
                      let tabBarVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                    tabBarVC.currentAddress = self.currentAddress
                      self.navigationController?.pushViewController(tabBarVC, animated: true)
                  }
              } else {
                  Alert.showSimple(message(json: dict))
              }
        }) { (error) in
             Hud.hide(view: self.view)
        }
    }
}
    

extension SignupViewController : FPNTextFieldDelegate {
    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code) // Output "France", "+33", "FR"
    }
    
    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        if isValid {
            // Do something...
            self.mobileNumber = textField.getFormattedPhoneNumber(format: .E164)!
            textField.getFormattedPhoneNumber(format: .E164)        // Output "+33600000001"
            textField.getFormattedPhoneNumber(format: .International)  // Output "+33 6 00 00 00 01"
            textField.getFormattedPhoneNumber(format: .National)       // Output "06 00 00 00 01"
            textField.getFormattedPhoneNumber(format: .RFC3966)        // Output "tel:+33-6-00-00-00-01"
            textField.getRawPhoneNumber()                            // Output "600000001"
        } else {
            // Do something...
            print("Number not valid")
        }
    }
}

