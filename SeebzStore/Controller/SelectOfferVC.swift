//
//  SelectOfferVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit

import IBAnimatable

class SelectOfferVC: UIViewController {
    
    
    
       weak var homeVC: HomeViewController?
       weak var myWishlistVC: MyWishlistVC?
       var categoryList = [CategoryListIncoming.Body]()
       var product : ProductListIncoming.Body!
       var favoriteProduct : FavoriteListIncoming.Body!
       var selectedIndex = Int()
        var productListIncoming = [ProductListIncoming.Body]()
    //MARK:- OUTLETS

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsMultipleSelection = true
          

        // Do any additional setup after loading the view.
    }
    
   //MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "OfferDetailRentVC") as! OfferDetailRentVC
               
                vc.homeVC = self.homeVC
               //            vc.selectedIndex = indexPath.row
                           vc.categoryList = self.categoryList
                           vc.product = self.product
        self.navigationController?.popViewController(animated: true)
//               self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

//MARK:- EXTENSION
extension SelectOfferVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return productListIncoming.count
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "selectOfferCell", for: indexPath) as! selectOfferCell
        
        if cell.isSelected{
            cell.contentView.superview?.backgroundColor = UIColor.blue
        }else{
            cell.contentView.superview?.backgroundColor = UIColor.clear
        }
        
//         cell.configure(product: productListIncoming[indexPath.row], categoryList: self.categoryList)
         return cell
     }
    
     
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let celll = tableView.cellForRow(at: indexPath) as! selectOfferCell
//        celll.contentView.superview.backgroundColor = UIColor.gray
//
        
        if celll.isSelected{
            celll.tickImage.isHidden = false

        }else{
          celll.tickImage.isHidden = true

        }
    }
    
       
        
        

}

class selectOfferCell: UITableViewCell {
    
    // MARK:- OUTLETS
    @IBOutlet var offerImage: AnimatableImageView!
    @IBOutlet var offerTitleLbl: UILabel!
    @IBOutlet var offerNameLbl: UILabel!
    @IBOutlet var offerTypeLbl: UILabel!
    @IBOutlet var offerAddressLbl: UILabel!
    @IBOutlet var offerAmountLbl: UILabel!
    @IBOutlet var offerRentSellLbl: UILabel!
    @IBOutlet weak var tickImage: UIImageView!
    
     override func awakeFromNib() {
          super.awakeFromNib()
          tickImage.isHidden = true
      }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            contentView.backgroundColor = UIColor.green
        } else {
            contentView.backgroundColor = UIColor.blue
        }
    }
    
    func configure(product: ProductListIncoming.Body, categoryList: [CategoryListIncoming.Body]) {
           offerTitleLbl.text = product.title.leoSafe().firstUppercased
           offerAddressLbl.text = product.location.leoSafe()
           offerAmountLbl.text = "$\(product.price.leoSafe())"
           
           
           self.offerImage.showImageUsingURL(urlEndPointStr: product.product_image.leoSafe())
//
//           if product.is_favroite.leoSafe() == FavoriteStatus.isUnfavorite.rawValue {
//               favoriteButton.setImage(#imageLiteral(resourceName: "Heart"), for: .normal)
//           } else {
//               favoriteButton.setImage(#imageLiteral(resourceName: "Heart-fill"), for: .normal)
//           }
//           if product.ad_type.leoSafe() == 1 {
//               SellLbl.text = "Buy"
//           } else {
//               SellLbl.text = "Rent"
//           }
//
           for cat in categoryList {
               if cat.cat_id.leoSafe() == product.cat_id.leoSafe() {
                   offerTypeLbl.text = cat.name.leoSafe()
                   break
               }
           }
       }
    
}
