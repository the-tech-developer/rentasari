//
//  AboutMeVC.swift
//  seebzStore
//
//  Created by Deep Baath on 19/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit

class AboutMeVC: UIViewController, UITextFieldDelegate {
    
//  MARK:- OUTLETS
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var mobileTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var descriptionTF: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
     var datePicker: UIDatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileTF.keyboardType = .numberPad
                datePicker = UIDatePicker()
                     datePicker?.datePickerMode = .date
                     dobTF.delegate = self
                     let tap = UITapGestureRecognizer(target: self, action: #selector(ViewTapped(GestureRecoganizer:)))
                     view.addGestureRecognizer(tap)
                    
                     dobTF.inputView = datePicker
                     datePicker?.addTarget(self, action: #selector(yearChanged(datePicker:)), for: .valueChanged)
        
        if Cookies.userInfo() != nil{
            firstNameTF.text = "\(Cookies.userInfo()!.firstaname.capitalized)"
            lastNameTF.text = "\(Cookies.userInfo()!.lastname.capitalized)"
            emailTF.text = "\(Cookies.userInfo()!.email)"
            dobTF.text = "\(Cookies.userInfo()!.dateofbirth.leoDateString(toFormat: "dd-MM-yyyy"))"
            mobileTF.text = "\(Cookies.userInfo()!.mobile)"
            genderTF.text = "\(Cookies.userInfo()!.gender.capitalized)"
            descriptionTF.text = "\(Cookies.userInfo()!.userDescription)"
        }
        
//        genderTF.addTarget(self, action: #selector(performAlertSheet(_:)), for:.editingChanged)
        
       

    }
    
  
    //MARK:- ACTION
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func genderAction(_ sender: Any) {
        
        performAlertSheet()
//        genderTF.text =
        
    }
    
   
    
    @IBAction func saveInfoAction(_ sender: UIButton){
        self.AboutMeAPI()
    }
    
    @objc func ViewTapped(GestureRecoganizer: UITapGestureRecognizer){
           view.endEditing(true)
       }
       
       @objc func yearChanged(datePicker: UIDatePicker){
       var dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "dd-MM-yyyy"
       
       dobTF.text = dateFormatter.string(from: datePicker.date)
       view.endEditing(true)
       }
    
    
    
    
    @objc func performAlertSheet(){
        let actionSheet = UIAlertController(title: "Choose your Gender", message: "", preferredStyle: .actionSheet)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.left
//        let messageText = NSAttributedString(string: "Action", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.font : UIFont(name: "OpenSans-Bold", size: 30.0)!])
//        actionSheet.setValue(messageText, forKey: "attributedMessage")
        
//        actionSheet.setAlertActionIcon1(title: "Male", iconName: "ic_edit_godfather")
//        actionSheet.setAlertActionIcon1(title: "Female", iconName: "ic_change_pwd")
        
        let MaleActionButton = UIAlertAction(title: "Male", style: .default) { action -> Void in
                   print("Male")
            self.genderTF.text = "Male"
                 
               }
        actionSheet.addAction(MaleActionButton)
        let FemaleActionButton = UIAlertAction(title: "Female", style: .default) { action -> Void in
                          print("Female")
            self.genderTF.text = "Female"
                        
                      }
               actionSheet.addAction(FemaleActionButton)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    
    
    
    //MARK:- API IMPLEMENTATION
    func AboutMeAPI(){
     if !firstNameTF!.length(){
        Alert.showSimple("Please enter first Name.")
            return
        }
        
        if !lastNameTF!.length(){
            Alert.showSimple("Please enter last Name.")
            return
        }
        
        if !emailTF.isValidEmailAddress() {
            Alert.showSimple("Please enter valid Email Address.")
            return
        }

        if !dobTF!.length(){
                    Alert.showSimple("Please enter your dob"){
                    }
                    return
                }
        if !mobileTF!.length(){
                    Alert.showSimple("Please enter  your Mobile Number."){
                    }
                    return
                }
//        if !mobileTF.isValidPhoneNumber() {
//            Alert.showSimple("Please enter valid mobile Number")
//            return
//        }

        if !genderTF!.length(){
                    Alert.showSimple("Please enter gender."){
                    }
                    return
                }
        if !descriptionTF!.length(){
                    Alert.showSimple("Please enter first Name."){
                    }
                    return
                }
        
        
         if !mobileTF.isValidPhoneNumber() {
             Alert.showSimple("Please enter valid mobile Number")
             return
         }
        
        
        guard let userInfo = Cookies.userInfo() else { return }

        var params = [String: Any]()
        params["user_id"] = userInfo.user_id
        params["first_name"] = firstNameTF.text.leoSafe()
        params["last_name"] = lastNameTF.text.leoSafe()
        params["email"] =  emailTF.text.leoSafe()
        params["mobile_num"] = mobileTF.text.leoSafe()
        params["DateOfBirth"] = dobTF.text.leoSafe()
        params["gender"] =  genderTF.text.leoSafe()
        params["description"] = descriptionTF.text.leoSafe()
      
        
         Hud.show(message: "Loading...", view: self.view)
        WebServices.post(url: .about_me, jsonObject: params, method: .post, completionHandler: { (dict) in
            print("Response")
              Hud.hide(view: self.view)
            
          //  if isSuccess(json: dict){
                if let success = dict["success"] as? Int {
                    if success == 200 {
                        
                 let userDict = UserLogin(dict: dict)
                Cookies.userInfoSave(dict: userDict.user?.first?.serverData)
                        
                        
                         self.navigationController?.popToSpecificVC(viewController: TabBarVC.self)
//                        
                    }
                } else
            {
                Alert.showSimple(message(json: dict))
            }
        
        }) { (error) in
              Hud.hide(view: self.view)
            
        }
        
    }
      
}
extension UIAlertController {
    func setAlertActionIcon1(title: String, iconName: String) {
        let action = UIAlertAction(title: title, style: .default, handler: nil)
        let image = UIImage(named: iconName)
        action.setValue(image?.withRenderingMode(.alwaysOriginal), forKey: "image")
        self.addAction(action)
    }
}
