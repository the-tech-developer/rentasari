//
//  SavedCardVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import Alamofire
import DropDown
import IBAnimatable


enum CardComingFrom {
    case fromProfile
    case fromPayment
}


class savedCardCell: UITableViewCell{
    
    //MARK:- OUTLETS
    
    
    @IBOutlet var cardNameTF: UILabel!
    @IBOutlet var roundedBttn: AnimatableButton!
    @IBOutlet var cardNumberTF: UITextField!
    @IBOutlet weak var saveCardExpiryTF: UITextField!
    @IBOutlet var cardCVVTF: UITextField!
    var myCardDetail : CardDetailModel?
    @IBOutlet var editCardBttn: UIButton!
    weak var saveCard : SavedCardVC?
    var card: CardDetailModel.Body!
    var dropDown = DropDown()
    

    weak var SaveVC: SavedCardVC?
    var myCardDetail1 = [CardDetailModel.Body?]()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        dropDown.anchorView = editCardBttn
        dropDown.dataSource = ["Edit", "Delete"]
        dropDown.width = 170
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0 {
                //Edit
                print(index)
                self.dropDown.show()
                let vc = self.SaveVC?.storyboard?.instantiateViewController(withIdentifier: "SavedCardDetailVC")  as! SavedCardDetailVC
                vc.comeFrom = "Edit_Cardinfo"
                vc.myCardDetail = [self.card]
                //                    vc.myCardDetail =  [self.myCardDetail1[0]]321
                self.SaveVC?.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                print(index)
                
                self.SaveVC?.showPermissionAlertWithBtn(title: "RentASari", message: "Are you sure want to delete", preferredStyle: .alert, senderTag: delete_indexPath)
            }
        }
    }
    
    
    
    func configure(myCard: CardDetailModel.Body) {
        self.card = myCard
        
        cardNameTF.text = "\(myCard.card_holder_name.leoSafe())"
        cardNumberTF.text = "\(myCard.cart_no.leoSafe())"
        //        cardNumberTF.text = "\(myCard.cart_no.leoSafe())"
        saveCardExpiryTF.text = "\(myCard.expire_month.leoSafe())"
        cardCVVTF.text = "\(myCard.cvv.leoSafe())"
    }
    
  
}

class SavedCardVC: UIViewController , UITextFieldDelegate{
    
    //MARK:- OUTLETS
    @IBOutlet var cardHolderNameLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nextButton: UIButton!

    
    
    
    var datePicker: UIDatePicker?
    var card: CardDetailModel.Body!
    var myCardDetail : CardDetailModel?
    weak var SaveVC: SavedCardVC?
    
    var comingFromCard: CardComingFrom = .fromPayment
    var selectedAddressIndex = 0
var selectedSaveCardIndex = 0
    var addressSelectedROundButton : [Int] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.addTarget(self, action: #selector(tappedNextButton), for: .touchUpInside)
        self.gettingMyCardDetail()
        self.tableView.reloadData()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        self.gettingMyCardDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.gettingMyCardDetail()
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        let cell = tableView.dequeueReusableCell(withIdentifier: "savedCardCell") as! savedCardCell
        //        if textField == cell.saveCardExpiryTF {
        AKMonthYearPickerView.sharedInstance.barTintColor =  #colorLiteral(red: 0, green: 0.7294117647, blue: 0.7176470588, alpha: 1)
        
        AKMonthYearPickerView.sharedInstance.previousYear = 4
        
        AKMonthYearPickerView.sharedInstance.show(vc: self, doneHandler: doneHandler, completetionalHandler: completetionalHandler)
        
        //        }
        
    }
    
    //    @objc func editBttnTap(sender: UIButton){
    //          let vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardDetailVC")  as! SavedCardDetailVC
    //          vc.comeFrom = "Edit_Cardinfo"
    //        vc.myCardDetail = [myCardDetail?.body?[sender.tag]]
    //       self.navigationController?.pushViewController(vc, animated: true)
    //      }
    //
    func gettingMyCardDetail() {
        if let userInfo = Cookies.userInfo() {
            Hud.show(message: "Loading...", view: self.view)
            let params = ["user_id": userInfo.user_id]
            self.cardAPI(url: .CardListing, jsonObject: params)
        }
    }
    
    @objc
      func tappedNextButton(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShipFromViewController")  as! ShipFromViewController
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmDetailOrderVC")  as! ConfirmDetailOrderVC
//        vc.selectedSaveCardIndex = self.selectedSaveCardIndex
//        vc.selectedAddressIndex = self.selectedAddressIndex
//        self.navigationController?.pushViewController(vc, animated: true)
      }
    
    
    //MARK: ACTION
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addCardAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardDetailVC")  as! SavedCardDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func editCardAction(_ sender: Any) {
    }
    
    @IBAction func cardInfoAction(_ sender: Any) {
    }
    
    
    
    func showPermissionAlertWithBtn(title:String = "",message:String,preferredStyle: UIAlertController.Style? = .alert , senderTag: Int)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: "Yes", style: .default) { (action:UIAlertAction!) in
            
            print("Ok button tapped")
            
            let param = ["user_id" : self.myCardDetail?.body?[senderTag].user_id , "card_id": self.myCardDetail?.body?[senderTag].card_id]
            self.deleteCardApi(url: .delete_card, jsonObject: param as [String : Any])
            
        }
        alertController.addAction(OKAction)
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped")
            
        }
        alertController.addAction(cancelAction)
        
        // Present Dialog message
        UIApplication.topViewController()?.present(alertController, animated: true, completion:nil)
    }
    func deleteCardApi(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (dict) in
            print(dict)
            Hud.hide(view: self.view)
            if let success = dict["success"] as? Int {
                if success == 200 {
                    self.gettingMyCardDetail()
                    
                }else{
                    Alert.showSimple(message(json: dict))
                }
                
            }
        }) { (error) in
            
            Hud.hide(view: self.view)
        }
        
    }
    
    
    func cardAPI(url: Api, jsonObject: [String:Any], method: HTTPMethod = .post){
        WebServices.post(url: url, jsonObject: jsonObject, method: method, completionHandler: { (json) in
            print(json)
            Hud.hide(view: self.view)
            if isSuccess(json: json) {
                // SUCCESS
                
                if url == .CardListing {
                    //                            let leo = LeoSwiftCoder()
                    //                        leo.leoClassMake(withName: "CardDetailModel", json: json)
                    self.myCardDetail = CardDetailModel(dict: json)
                    print(self.myCardDetail)
                    Singleton.shared.myCardDetail = self.myCardDetail
                    self.tableView.reloadData()
                    
                    /*
                     if let body = CardDetailModel(dict: json).body {
                     if body.count != 0 {
                     self.myCardDetail = body
                     self.tableView.reloadData()
                     
                     } else {
                     
                     }
                     } else {
                     
                     }
                     */
                }
            }else {
                // FAILURE
                Alert.showSimple(message(json: json))
            }
            
            
            //            if let success = dict["success"] as? Int {
            //                               if success == 200 {
            //
            ////                                let leo = LeoSwiftCoder()
            ////                                leo.leoClassMake(withName: "mm", json: dict)
            ////
            ////                            let userDict = UserLogin(dict: dict)
            ////                           Cookies.userInfoSave(dict: userDict.user?.first?.serverData)
            //
            ////                            self.navigationController?.popToSpecificVC(viewController: TabBarVC.self)
            //
            //                        }
            //                } else
            //            {
            //            Alert.showSimple(message(json: dict))
            //        }
        }) { (error) in
            Hud.hide(view: self.view)
            
        }
    }
}


//MARK:- EXTENSION CLASS

extension SavedCardVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myCardDetail?.body?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "savedCardCell", for: indexPath) as! savedCardCell
        
        
        switch self.comingFromCard {
        case .fromProfile:
            cell.roundedBttn.isHidden = true
        default:
            cell.roundedBttn.isHidden = false
        }
        
        cell.configure(myCard: ((myCardDetail?.body?[indexPath.row])!))
        cell.editCardBttn.tag = indexPath.row
        cell.SaveVC = self
        cell.editCardBttn.addTarget(self, action: #selector(action3DotBtn(sender:)), for: .touchUpInside)
        //        cell.editCardBttn.addTarget(self, action: #selector(editBttnTap(sender:)), for: .touchUpInside)
        //        cell.editCardBttn.tag = indexPath.row
        
        let  Address_Id =  self.myCardDetail?.body?[indexPath.row].card_id ?? 0
        //                        self.delivery?.body?[indexPath.row].user_id ?? 0
        if self.addressSelectedROundButton.contains(Address_Id){
            
            cell.roundedBttn.backgroundColor = .black
        }else{
            cell.roundedBttn.backgroundColor = .white
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _ = tableView.cellForRow(at: indexPath) {
            if self.myCardDetail?.body?.count ?? 0 > 0 {
                
                //  cell.btnRadioSelectedOrNot.addTarget(self, action: #selector(self.tapped_selectedAddressBtn(sender:)), for: .touchUpInside)
                
                //                          cell.btnRadioSelectedOrNot.tag = indexPath.row
                //                         let  Address_Id =   self.customerAddressIncoming?.body?[indexPath.row].id ?? 0
                //                  if self.addressSelectedBtn_Id.contains(Address_Id){
                //
                //                                     cell.btnRadioSelectedOrNot.backgroundColor = .black
                //                      }else{
                //                                  cell.btnRadioSelectedOrNot.backgroundColor = .white
                //                  }
                
                
                
                
                
                
                
                if let exer_Info = myCardDetail?.body?[indexPath.row] {
                    
                    let  Address_Id =   exer_Info.card_id
                    
                    //   addressSelectedROundButton.append(Address_Id!)
                    
                    if addressSelectedROundButton.contains(Address_Id ?? 0){
                        
                        addressSelectedROundButton.remove(at: addressSelectedROundButton.firstIndex(of: Address_Id!)!)
                        // cell.btnRadioSelectedOrNot.backgroundColor = .black
                    }else{
                        // cell.btnRadioSelectedOrNot.backgroundColor = .white
                        addressSelectedROundButton.removeAll()
                        addressSelectedROundButton.append(Address_Id!)
                    }
                    self.tableView.reloadData()
                    
                    
                    
                    //                        myDeliveryDetail.removeAll()
                    //                        myDeliveryDetail.append(exer_Info)
                    
                    //                        if exer_Info.isSelected == true {
                    //                            myDeliveryDetail.removeAll()
                    //                            for item in self.myDeliveryDetail {
                    //                                item.isSelected = false
                    //                            }
                    //                            exer_Info.isSelected = false
                    //                        } else {
                    //                            for item in self.myDeliveryDetail {
                    //                                item.isSelected = false
                    //                            }
                    //                            exer_Info.isSelected = true
                    //
                    //                        }
                    //                        print("selectDefault",myDeliveryDetail)
                    //
                    //                    }
                    
                }
                tableView.reloadData()
            }
        }
    }
    
    @objc func action3DotBtn(sender: UIButton) {
        let  indexPath = IndexPath(row: sender.tag, section: 0)
        if let selectedCell1 = tableView.cellForRow(at: indexPath) as? savedCardCell {
            delete_indexPath =  sender.tag
            selectedCell1.dropDown.show()
            
        }
    }
    private func doneHandler() {
        print("Month picker Done button action")
    }
    
    private func completetionalHandler(month: Int, year: Int) {
        print( "month = ", month, " year = ", year )
    }
    
}
