//
//  ConfirmYourAddVC.swift
//  seebzStore
//
//  Created by Deep Baath on 19/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit

class ConfirmYourAddVC: UIViewController {
    // MARK:- OUTLETS
    @IBOutlet weak var prductImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var publishNotButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var sellLabel: UILabel!
    
    // MARK:- VARIABLES
    var addAdList : AddAdList!
    var product = [ProductListIncoming.Body]()
    var myAdsListIncoming = [ProductListIncoming.Body]()
    var isScreenComeFrom : ScreenComeFrom = .none

    // MARK:- VIEW LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showUserDetailsOnUI()
        if selectedImagesFromGallary.count != 0{
            prductImageView.image = selectedImagesFromGallary[0] 
        }
        
            
//        if isScreenComeFrom == .createAd{
//            callApiForCreateAd()
//        }else if isScreenComeFrom == .editAd{
//        self.editCreateAPI()
//    }
    }
    
    private func showUserDetailsOnUI() {
        guard let userInfo = Cookies.userInfo() else { return }
        if let addAdList = self.addAdList {
            if addAdList.ad_type == "1" {
                sellLabel.text = "Sell"
            } else {
                sellLabel.text = "Rent"
            }
            titleLabel.text = addAdList.title
            descriptionLabel.text = addAdList.description
            userNameLabel.text = userInfo.firstaname.capitalized
            addressLabel.text = addAdList.address
            priceLabel.text = "\(addAdList.price)"
            emailLabel.text = userInfo.email
            categoryLabel.text = addAdList.cat_name
            if addAdList.images?.count ?? 0 > 0 {
                prductImageView.image = addAdList.images![0]

            }
        }
        if isScreenComeFrom == .createAd {
            publishNotButton.setTitle("PUBLISH NOW", for: .normal)
        } else if isScreenComeFrom == .editAd{
            publishNotButton.setTitle("REPUBLISH NOW", for: .normal)
        }
    }
    
    
    func editCreateAPI(){
        guard let userInfo = Cookies.userInfo() else { return }
        
         var sample = [String: Any]()
              
        sample["username"] = "\(userInfo.firstaname.capitalized) \(userInfo.lastname.capitalized)"
        sample["product_id"] =  product[0].product_id ?? 0
        sample["ad_type"] = addAdList.ad_type
        sample["cat_id"] = addAdList.cat_id
        sample["title"] = addAdList.title
        sample["description"] = addAdList.description
        sample["price"] = addAdList.price
        sample["is_extend"] = addAdList.isExtend
        if addAdList.days == ""{
            sample["days"] = "0"

        }else{
            sample["days"] = addAdList.days

        }
        sample["location_address"] = addAdList.address
        sample["latiude"] = addAdList.latitude
        sample["longitude"] = addAdList.longitude
//        sample["upload_image"] = addAdList.images

//       sample["product_id"] = "16"
//        sample["ad_type"] = "1"
//        sample["cat_id"] = "17"
//        sample[""] = "new jewellery"
//        sample["description"] = "abc..."
//        sample["price"] = "500"
//        sample["is_extend"] = "1"
//        sample["days"] = "12"
//        sample["location_address"] = "chd"
//        sample["latiude"] = "34567"
//        sample["longitude"] = "2222222"
//        sample["upload_image"] = ""
//
                   
        
        
         Hud.show(message: "Loading...", view: self.view)
      WebServices.uploadFiles(url: .edit_Create_Add, jsonObject: sample, files: (key: "upload_image", images: addAdList.images!), headers: nil, completionHandler: { (json) in
                       Hud.hide(view: self.view)
                       print("Success JSOn: \(json)")
                                       
                       if isSuccess(json: json) {
                           Alert.showSimple("Ad created successfully.", completionHandler: {
                               self.navigationController?.popToSpecificVC(viewController: TabBarVC.self)
                           })
                       }
            else {
                Alert.showSimple(message(json: json))
            }
        }) { (error) in
            Hud.hide(view: self.view)
            print(error)
        }
//        }
    }
        
    // MARK:- CORE FUNCTIONALITIES
    func callApiForCreateAd() {
        if !isConnectedToInternet(){
            Alert.showSimple("Please connect to internet")
            return
        }
        
        var params = [String:Any]()
        if let addAdList = self.addAdList {
            params["ad_type"] = addAdList.ad_type
            params["user_id"] = addAdList.user_id
            params["cat_id"] = addAdList.cat_id
            params["title"] = addAdList.title
            params["description"] = addAdList.description
            params["price"] = addAdList.price
            params["days"] =  addAdList.days
            params["is_extend"] = addAdList.isExtend
            params["location_address"] = addAdList.address
            params["latiude"] = addAdList.latitude
            params["longitude"] = addAdList.longitude
            params["email"] = addAdList.email
            
            guard let userInfo = Cookies.userInfo() else { return }
            params["username"] = "\(userInfo.firstaname.capitalized) \(userInfo.lastname.capitalized)"
            
            Hud.show(message: "Loading...", view: self.view)
            WebServices.uploadFiles(url: .Create_Add, jsonObject: params, files: (key: "upload_image", images: addAdList.images!), headers: nil, completionHandler: { (json) in
                Hud.hide(view: self.view)
                print("Success JSOn: \(json)")
                                
                if isSuccess(json: json) {
                    Alert.showSimple("Ad created successfully.", completionHandler: {
                        self.navigationController?.popToSpecificVC(viewController: TabBarVC.self)
                    })
                }
                else {
                    Alert.showSimple(message(json: json))
                }
            }) { (errorJson) in
                print(errorJson)
            }
        }
    }
    
   
    
    
    
    // MARK:- ACTION
    @IBAction func publishAddAction(_ sender: UIButton){
        if self.isScreenComeFrom == .createAd{
         self.callApiForCreateAd()
        }else if self.isScreenComeFrom == .editAd{
        self.editCreateAPI()
    }
    }
    
    @IBAction func backAction(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
  }

