//
//  SavedCardDetailVC.swift
//  seebzStore
//
//  Created by Deep Baath on 21/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import MonthYearPicker
import CreditCardValidator



class SavedCardDetailVC: UIViewController, UITextFieldDelegate{

    //MARK:- OUTLETS
    @IBOutlet var totalAmountLbl: UILabel!
    @IBOutlet var cardHolderNameTF: UITextField!
    @IBOutlet var cardNumberTF: UITextField!
    @IBOutlet var cardExpiryTF: UITextField!
    @IBOutlet var cardCVVTF: UITextField!
    @IBOutlet weak var viewTotalAmount: UIView!
    var cardNumberCursorPreviousPosition = 0
    var datePicker: UIPickerView?
    var myCardDetail = [CardDetailModel.Body?]()
//    var myCarddetail : CardDetailModel?
    var comeFrom = ""
    var card: CardDetailModel.Body!
    
    private var previousTextFieldContent: String?
    private var previousSelection: UITextRange?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewTotalAmount.isHidden = true
        
        
        
        cardNumberTF.addTarget(self, action: #selector(reformatAsCardNumber), for: .editingChanged)

    if comeFrom == "Edit_Cardinfo" {
                         
        
            
        cardHolderNameTF.text = "\(myCardDetail[0]?.card_holder_name ?? "")"
        cardNumberTF.text = "\(myCardDetail[0]?.cart_no ?? "")"
        cardExpiryTF.text = "\(myCardDetail[0]?.expire_month ?? "" )"
        cardCVVTF.text = "\(myCardDetail[0]?.cvv ?? 0)"
            

         }else{
             
         }

     
    }
    

   

   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

  
    
       previousTextFieldContent = textField.text;
       previousSelection = textField.selectedTextRange;
       return true
   }
    
    @objc func reformatAsCardNumber(textField: UITextField) {
        var targetCursorPosition = 0
        if let startPosition = textField.selectedTextRange?.start {
            targetCursorPosition = textField.offset(from: textField.beginningOfDocument, to: startPosition)
        }

        var cardNumberWithoutSpaces = ""
        if let text = textField.text {
            cardNumberWithoutSpaces = self.removeNonDigits(string: text, andPreserveCursorPosition: &targetCursorPosition)
        }

        if cardNumberWithoutSpaces.count > 16 {
            textField.text = previousTextFieldContent
            textField.selectedTextRange = previousSelection
            return
        }

        let cardNumberWithSpaces = self.insertCreditCardSpaces(cardNumberWithoutSpaces, preserveCursorPosition: &targetCursorPosition)
        textField.text = cardNumberWithSpaces

        if let targetPosition = textField.position(from: textField.beginningOfDocument, offset: targetCursorPosition) {
            textField.selectedTextRange = textField.textRange(from: targetPosition, to: targetPosition)
        }
    }
    func removeNonDigits(string: String, andPreserveCursorPosition cursorPosition: inout Int) -> String {
           var digitsOnlyString = ""
           let originalCursorPosition = cursorPosition

           for i in Swift.stride(from: 0, to: string.count, by: 1) {
               let characterToAdd = string[string.index(string.startIndex, offsetBy: i)]
               if characterToAdd >= "0" && characterToAdd <= "9" {
                   digitsOnlyString.append(characterToAdd)
               }
               else if i < originalCursorPosition {
                   cursorPosition -= 1
               }
           }

           return digitsOnlyString
       }

       func insertCreditCardSpaces(_ string: String, preserveCursorPosition cursorPosition: inout Int) -> String {
           
           let is4444 = string.hasPrefix("1")
//

           var stringWithAddedSpaces = ""
           let cursorPositionInSpacelessString = cursorPosition

           for i in 0..<string.count {
//
               let needs4444Spacing = (is4444 && i > 0 && (i % 16) == 0)

//              r
                if needs4444Spacing {
                   stringWithAddedSpaces.append(" ")

                   if i < cursorPositionInSpacelessString {
                       cursorPosition += 1
                   }
               }

               let characterToAdd = string[string.index(string.startIndex, offsetBy:i)]
               stringWithAddedSpaces.append(characterToAdd)
           }

           return stringWithAddedSpaces
       }

    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    
          textField.resignFirstResponder()
        if textField == cardExpiryTF{
            AKMonthYearPickerView.sharedInstance.barTintColor =  #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)

                      AKMonthYearPickerView.sharedInstance.previousYear = 4
                      
                      AKMonthYearPickerView.sharedInstance.show(vc: self, doneHandler: doneHandler, completetionalHandler: completetionalHandler)
                   
        }else if textField == cardNumberTF{
//            let number = "1234 5678 9123 4567"
//
//            let v = CreditCardValidator
//
//            if v.validateString(number) {
//              // Card number is valid
//            } else {
//              // Card number is invalid
//            }
            
        }
        
   
        
    }
  
    
    private func doneHandler() {
           print("Month picker Done button action")
       }
       
       private func completetionalHandler(month: Int, year: Int) {
        cardExpiryTF.text = "\(month)" + "/" + "\(year)"
           print( "month = ", month, " year = ", year )
       }
  
   // MARK:- ACTION
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
     
    @IBAction func saveCardAction(_ sender: Any) {
        self.cardApi()
    }

       
    func cardApi(){
        
        
          guard let userInfo = Cookies.userInfo() else { return }
        
              var cardParams = [String: Any]()
        cardParams["cvv"] = cardCVVTF.text.leoSafe()
        cardParams["expire_year"] = cardExpiryTF.text.leoSafe()
        cardParams["expire_month"] = cardExpiryTF.text.leoSafe()
        cardParams["card_number"] = cardNumberTF.text.leoSafe()
        cardParams["holder_name"] = cardHolderNameTF.text
        cardParams["user_id"] = userInfo.user_id
        
        
        var url : Api?
               if comeFrom == "Edit_Cardinfo"{
                   url = .edit_card
                cardParams["card_id"] = myCardDetail[0]?.card_id

               }else{
                   url = .AddNewCard
               }
        
           Hud.show(message: "Loading...", view: self.view)
            
        WebServices.post(url: url!, jsonObject: cardParams, method: .post, completionHandler: { (dict) in
                
                print("Response")
                    Hud.hide(view: self.view)
                
                if let success = dict["success"] as? Int {
                                   if success == 200 {
                                       
//                                let userDict = UserLogin(dict: dict)
//                               Cookies.userInfoSave(dict: userDict.user?.first?.serverData)
                                       self.navigationController?.popToSpecificVC(viewController: SavedCardVC.self)
//                                       let vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardVC")  as! SavedCardVC
//                                       self.navigationController?.pushViewController(vc, animated: true)
                                   }
                               }else  {
                                              Alert.showSimple(message(json: dict))
                                          }
                
            }) { (error) in
                    Hud.hide(view: self.view)
                
            }
        }

    }
    

