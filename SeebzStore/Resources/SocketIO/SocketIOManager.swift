//
//  SocketIOManager.swift
//  BUDO
//
//  Created by tecH on 22/08/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import SocketIO
import Starscream

extension SocketIOManager : WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        //https://github.com/PerfectExamples/Perfect-WebSocketsServer
        print("🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
             print("🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈",error)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
             print("🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈")
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
             print("🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈🎈")
    }
    
    
}
    


class SocketIOManager: NSObject {

    static let sharedInstance = SocketIOManager()
    var manager : SocketManager!
    var socket: SocketIOClient?
    var webSocket : WebSocket?
    var closuresDidConnect : [(()-> Void)] = []
    
    
    
    override init() {
        super.init()
        initialized()
        //   some()
    }
    
    func some(){
        webSocket = WebSocket(url: URL(string: "ws://localhost:8181/api/v1/chat")!, protocols: ["chat"])
        webSocket?.delegate = self
        webSocket?.connect()
        
        webSocket?.onConnect = {
            print("websocket is connected")
        }
        //websocketDidDisconnect
        webSocket?.onDisconnect = { (error: Error?) in
            print("websocket is disconnected: \(error?.localizedDescription)")
        }
        //websocketDidReceiveMessage
        webSocket?.onText = { (text: String) in
            print("got some text: \(text)")
        }
        //websocketDidReceiveData
        webSocket?.onData = { (data: Data) in
            print("got some data: \(data.count)")
        }
    }
    
    
    func connectDone( callback :  @escaping (()-> Void) ) {
        if self.socket?.status.rawValue == 3 {
            callback()
        }
        closuresDidConnect.append(callback)
    }
    
    func initialized() {
         // print("🛠🛠🛠🛠🛠🛠",#function,#line  )
        // socket?.disconnect()
       socket?.disconnect()
        // print("Soemk")
        //http://localhost:8181
        //"http://budo.staging.co.in:5000"
        manager = SocketManager(socketURL: URL(string: "http://rentasari.staging.co.in:4019")!, config: [.log(false), .compress, .reconnects(true),])
        socket = manager.defaultSocket
     
        
        socket?.on(clientEvent: .connect) {data, ack in
            print("🔌:Event connect: ", data, self.socket?.status.rawValue)
            //30.7046° N, 76.7179° E
  
            self.socket?.on(clientEvent: .statusChange) {data, ack in
                print("🔌:Event:statusChange: " , data , self.socket?.status.rawValue)
                //  self.socket.disconnect()
            }
          
            for closure in self.closuresDidConnect {
                closure()
            }
            //    Method emit = returnChatRoom{caseID}
            //    Method on = chatRoom
            //    Parameters =   [ “caseId” : “123”  , offset: 0 ] // 25 messages
            
//            self.emitSendMessage(dict: ["case_id" :"243" ,
//                                        "sender" : "1" ,
//                                        "msg_type" : "1" ,
//                                        "message" : "dsasdas Workd"
//                                         ])
//            self.onceSendMessage(caseId: "243", callback: { (dict) in
//
//                print(dict)
//            })
            /*
             Parameters:
             case_id
             sender_id
             msg_type(1-msg,2-image,3-video)
             Message
             1-msg : message = {“someText”}
             2-image : message = {“Url”} : Data?
             3-video  : message = {“Url”} : Data?
             
//             */
//            func emitSendMessage(dict: [String:Any]) {
//                print("🛫🛫🛫🛫🛫🛫:\(#function): ",dict)
//                socket?.emit("sendMessage", dict)
//            }
//
//            func onceSendMessage(caseId: String , callback : @escaping (([String : Any])-> Void)) {
        }
        socket?.connect(timeoutAfter: 120, withHandler: {
            print("NotconnecttimeoutAfter")
        })
    }
    
    func establishConnection() {
         //some()
        print("🛠🛠🛠🛠🛠🛠",#function,#line  )
        socket?.connect(timeoutAfter: 10){
            
              print("🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖")
            print("NOT CONNECTED")
            self.socket?.connect()
            print("CONNECTING AGAIN")
        }
    }
    
    func connectedToSocketWithCompletionHandler( completion: @escaping (_ result: [[String: AnyObject]]?) -> Void) {
        socket?.once("SocketConnected") {data, ack in
            completion(data[0] as? [[String: AnyObject]])
            //      completion(userList: dataArray[0] as! [[String: AnyObject]])
            print("socket Manager connected")
              print("🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖🤖")
        }
    }
    
    func closeConnection() {
            print("🛠🛠🛠🛠🛠🛠",#function,#line  )
        socket?.disconnect()
    }
    
    func emitTimerFxn(dict: [String:Any]) {
        print("🛠🛠🛠🛠🛠🛠",#function,#line  )
        print("Status" , self.socket?.status.rawValue)
        print("👨‍🌾👨‍🌾👨‍🌾👨‍🌾👨‍🌾:task_listing_function",dict)
        socket?.emit("task_listing_function", dict)
    }
   
    func emitTaskStatus(dict: [String:Any]) {
        print("🛠🛠🛠🛠🛠🛠",#function,#line  )
        print("Status" , self.socket?.status.rawValue , dict)
        print("👨‍🌾👨‍🌾👨‍🌾👨‍🌾👨‍🌾:task_time_log",dict)
        socket?.emit("task_time_log", dict)
    }
    
    
    
    func emitChatListingRequest(dict: [String:Any]) {
         print("🛠🛠🛠🛠🛠🛠",#function,#line  )
         print("Status" , self.socket?.status.rawValue)
         print("👨‍🌾👨‍🌾👨‍🌾👨‍🌾👨‍🌾:Chat_Listing",dict)
         socket?.emit("Chat_Listing", dict)
     }
     
     func onChatListingRequest(user_id: String , callback : @escaping (([String : Any])-> Void)) {
         print("🛠🛠🛠🛠🛠🛠",#function,#line  )
         
         socket?.off("returnChat_Listing\(user_id)")
         
         socket?.on("returnChat_Listing\(user_id)", callback: { (dataArray, ack) in
             print("-----😽😽😽😽😽😽😽😽😽😽----------")
             print(dataArray)
             
             if let dict =  dataArray[0] as? [String : Any]{
                 callback(dict)
             }
             
             print("*************")
             print(print(dataArray))
             print("---------------")
         })
     }
    
    
    
    
}

