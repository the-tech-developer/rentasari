//
//  SocketChat.swift
//  BUDO
//
//  Created by tecH on 16/09/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import Foundation

extension SocketIOManager {
    
    func emitChatRoom(dict: [String:Any]) {
        print("🛫🛫🛫🛫🛫🛫:\(#function) : ChatRoom : ",dict)
        socket?.emit("ChatRoom", dict)
    }
    func onceChatRoom(chatId: String , callback : @escaping (([String : Any])-> Void)) {
        print("🛰🛰🛰🛰🛰🛰🛰🛰 \(#function): returnChatRoom\(chatId) ",chatId)
        socket?.off("returnChatRoom\(chatId)")
        socket?.once("returnChatRoom\(chatId)") { (dataArray, ack) in
            if let dict =  dataArray[0] as? [String : Any]{
                print("🛬🛬🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dict)
                callback(dict)
            }
        }
    }
    
    //__________________________________________________________________________
    /*
     Parameters:
     case_id
     sender_id
     msg_type(1-msg,2-image,3-video)
     Message
     1-msg : message = {“someText”}
     2-image : message = {“Url”} : Data?
     3-video  : message = {“Url”} : Data?
     
     */
    func emitSendMessage(dict: [String:Any]) {
        print("🛫🛫🛫🛫🛫🛫:\(#function): Send_Message ",dict , self.socket?.status.rawValue)
        socket?.emit("Send_Message", dict)
    }
    
    func onSendMessage(chatId: String , callback : @escaping (([String : Any])-> Void)) {
        print("🛰🛰🛰🛰🛰🛰🛰🛰 \(#function):returnSendMessage\(chatId)",chatId , self.socket?.status.rawValue)
        socket?.off("returnSendMessage\(chatId)")
        socket?.on("returnSendMessage\(chatId)") { (dataArray, ack) in
            
            print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dataArray)
            if let dict =  dataArray[0] as? [String : Any]{
                print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dict)
                callback(dict)
            }
        }
        
    }
    
    func emitCreateChatId(dict: [String:Any]) {
        print("🛫🛫🛫🛫🛫🛫:\(#function): createChatId ",dict , self.socket?.status.rawValue)
        socket?.emit("createChatId", dict)
    }
    
    func onCreateChatId(user_id: String , callback : @escaping (([String : Any])-> Void)) {
        print("🛰🛰🛰🛰🛰🛰🛰🛰 \(#function):returnCreateChatId\(user_id)",user_id , self.socket?.status.rawValue)
        
        socket?.off("returnCreateChatId\(user_id)")
        socket?.on("returnCreateChatId\(user_id)") { (dataArray, ack) in
            
            print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dataArray)
            if let dict =  dataArray[0] as? [String : Any]{
                
                print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dict)
                callback(dict)
                
            }
        }
    }
    
    func emitChatList(dict: [String:Any]) {
        print("🛫🛫🛫🛫🛫🛫:\(#function): Chat_Listing ",dict , self.socket?.status.rawValue)
        socket?.emit("Chat_Listing", dict)
    }
    
    func onChatList(user_id: String , callback : @escaping (([String : Any])-> Void)) {
        
        print("🛰🛰🛰🛰🛰🛰🛰🛰 \(#function):returnChat_Listing\(user_id)",user_id , self.socket?.status.rawValue)
      
        socket?.off("returnChat_Listing\(user_id)")
        socket?.on("returnChat_Listing\(user_id)") { (dataArray, ack) in
            
            print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dataArray)
            if let dict =  dataArray[0] as? [String : Any]{
                
                print("🛬🛬🛬🛬🛬🛬🛬🛬🛬:\(#function)",dict)
                callback(dict)
                
            }
        }
    }
}
