//
//  userdefaultcookies.swift
//  seebzStore
//
//  Created by MAC on 24/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    var email : String{
        return self["email"] as? String ?? ""
    }
    
    var firstaname : String{
        return self["first_name"] as? String ?? ""
    }
    
    var lastname : String{
        return self["last_name"]  as? String ?? ""
    }
    
    var dateofbirth : String{
        return self["dob"]  as? String ?? ""
    }
    
    var title : String{
        return self["title"]  as? String ?? ""
    }
    
    var userDescription : String{
        return self["description"]  as? String ?? ""
    }
    
    var gender : String{
        return self["gender"]  as? String ?? ""
    }
    
    var country_code : Int{
        return self["country_code"] as? Int ?? 0
    }
    var address_line_1 : String{
          return self["address_line_1"] as? String ?? ""
      }
    var address_line_2 : String{
          return self["address_line_2"] as? String ?? ""
      }
    var state : String{
          return self["state"] as? String ?? ""
      }
    var city : String{
          return self["city"] as? String ?? ""
      }
    var product_id : Int{
           return self["product_id"] as? Int ?? 0
       }
    
    
    
    
    
    
 
    var mobile: String{
        if let mobile = self["mobile"] as? String {
            return mobile
        }
        if let mobile = self["mobile"] as? Int {
            return "\(mobile)"
        }
        return ""
//        if let some = self["mobile"]  as? Int{
//            return "\(some)"
//        }
//
//        return ""
    }
    
    var device_type : String{
        return self["device_type"] as!String
    }
    var device_token : String{
        return self["device_token"] as!String
    }
    
    var user_id : String{
        
        if let some = self["user_id"]  as? Int{
            return "\(some)"
        }
        if let some = self["user_id"]  as? String{
            return "\(some)"
        }
        
        return ""
    }
    // "user_id" = 12
    var user_status: Int{
        return self["user_status"] as! Int
    }
    
}



class Cookies {
    class func userInfoSave( dict : [String : Any]? = nil){
        UserDefaults.standard.set( dict, forKey: "userInfoSave")
        UserDefaults.standard.synchronize()
    }
    class func userInfo() -> NSDictionary? {
        if let   some =  UserDefaults.standard.object(forKey: "userInfoSave") as? NSDictionary {
            return some
            
            
        }
        return nil
    }
    
    class func removeUserInfo() {
        UserDefaults.standard.removeObject(forKey: "userInfoSave")
    }
}

var currentAccessToken :String? {
    
    get {
        return  UserDefaults.standard.currentAccessToken()
    }
    set {
        UserDefaults.standard.currentAccessToken(newValue)
    }
    
    
}




extension UserDefaults {
    
    
    /// Private key for persisting the active Theme in UserDefaults
    private static let currentAccessTokenKey = "api.iamgds.com/ota/Auth"
    
    /// Retreive theme identifer from UserDefaults
    public func currentAccessToken() -> String? {
        return self.string(forKey: UserDefaults.currentAccessTokenKey)
    }
    
    /// Save theme identifer to UserDefaults
    public func currentAccessToken(_ identifier: String?) {
        self.set(identifier, forKey: UserDefaults.currentAccessTokenKey)
    }
    
}


extension UserDefaults {
    
    /// Private key for persisting the active Theme in UserDefaults
    private static let currentOffsetContactlistKey = "api.currentOffsetContactlistKey.u"
    
    /// Retreive theme identifer from UserDefaults
    public var currentOffsetContactlist : Int? {
        return self.integer(forKey: UserDefaults.currentOffsetContactlistKey)
    }
    
    /// Save theme identifer to UserDefaults
    public func currentOffsetContactlist(_ identifier: Int?) {
        self.set(identifier, forKey: UserDefaults.currentOffsetContactlistKey)
    }
    
}

extension UserDefaults {
    
    /// Private key for persisting the active Theme in UserDefaults
    private static let currentOffsetContactinfoKey = "api.currentOffsetContactinfoKey.u"
    
    /// Retreive theme identifer from UserDefaults
    public var currentOffsetContactinfo : Int? {
        return self.integer(forKey: UserDefaults.currentOffsetContactinfoKey)
    }
    
    /// Save theme identifer to UserDefaults
    public func currentOffsetContactinfo(_ identifier: Int?) {
        self.set(identifier, forKey: UserDefaults.currentOffsetContactinfoKey)
    }
    
}



