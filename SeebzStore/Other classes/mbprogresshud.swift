//
//  mbprogresshud.swift
//  seebzStore
//
//  Created by MAC on 24/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import MBProgressHUD
class Hud: NSObject {
    static let shareInstance = Hud()
    class func show(message:String, view:UIView) {
        let hud  = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = message
    }
    class func hide(view:UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}


