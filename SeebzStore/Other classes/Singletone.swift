//
//  Singletone.swift
//  TakeChargeNxt
//
//  Created by IOS on 14/01/20.
//  Copyright © 2020 tecHangouts. All rights reserved.
//

import Foundation
import CoreLocation

class Singleton: NSObject {
   
    static let shared = Singleton()
    
    private override init() {
        super.init()
    }
    
    // MARK:- Variables
    var product = [ProductListIncoming.Body]()
    var myDeliveryDetail = [DeliveryListIncoming.Body?]()
    var myCardDetail : CardDetailModel?
    var product1 : ProductListIncoming.Body!
    var ProductDetailList: HomeScreenProductDetail?


    
}
