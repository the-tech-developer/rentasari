//
//  UINavigationController.swift
//  seebzStore
//
//  Created by IOS on 12/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit
import Nuke

extension UINavigationController {
    // POP TO SPECIFIC VIEW CONTROLLER
    func popToSpecificVC(viewController: AnyClass, animation: Bool? = true) {
        let vcs = self.viewControllers
        
        vcs.forEach { (vc) in
            if vc.isKind(of: viewController) {
                print("Removed Specific View Controller from Nav Stack")
                self.popToViewController(vc, animated: animation!)
            }
        }
    }
    
    // CHANGE STATUS BAR STYLE FOR A PARTICULAR SCREEN
    /*open override var preferredStatusBarStyle: UIStatusBarStyle {
        if let topVC = self.topViewController {
            if topVC.isKind(of: SignInVC.self) || topVC.isKind(of: SignUpVC.self) {
                return .default
            }
        }
        return .lightContent
    }*/
}

extension UIViewController {
    var previousViewController: UIViewController? {
        if let controllersOnNavStack = self.navigationController?.viewControllers, controllersOnNavStack.count >= 2 {
            let n = controllersOnNavStack.count
            return controllersOnNavStack[n - 2]
        }
        return nil
    }
}

extension UIImageView {
    func showImageUsingURL(urlEndPointStr: String) {
        if let imageURL = URL(string: imageBaseUrl + urlEndPointStr) {
            Nuke.loadImage(with: imageURL, options: .shared, into: self, progress: nil) { (response) in
                do {
                    let resp = try response.get()
                    self.image = resp.image
                } catch {
                    print(error.localizedDescription)
                    self.image = #imageLiteral(resourceName: "jewellry-img")
                }
            }
        }
    }
}
