//
//  Location.swift
//  seebzStore
//
//  Created by IOS on 11/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import GooglePlaces

class Location : NSObject {
    
    static let sharedInstance = Location()
    var locationManager = CLLocationManager()
    var userCurrentLocation: CLLocation!
    let placesClient = GMSPlacesClient.shared()
    var closureDidGetLocation : (() -> Void)?
    
    override init() {
        super.init()
        print("Location INIT Called")
        checkLocationAuth()
        locationManager.delegate = self
    }
    
    
    private func checkLocationAuth() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("User authorized")
            guard let location = locationManager.location else { return }
            self.userCurrentLocation = location
            
        case .notDetermined, .denied:
            print("Denied")
            locationManager.requestWhenInUseAuthorization()
            
        case .restricted:
            print("User restricted")
            break
        }
    }
    
    private func openSettingsURL() {
        print("Location Service Not Enabled")
        let actionOpenSetting = UIAlertAction(title: "Open Settings", style: .default) { (_) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        Alert.showAlertWithBtn(title: "Location Service Disabled", message: "To re-enable, please go to Settings and turn on Location Service for this app.", buttons: [actionOpenSetting, actionCancel])
    }
    
    private func reverseGeocoderCoordinate(_ coordinate: CLLocationCoordinate2D, completion: @escaping (RASAddress) -> ()){
        let geoCoder = GMSGeocoder()
        geoCoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            if let err = error{
                print("Geocoder Error : \(err.localizedDescription)")
                return
            }
            guard let address = response?.firstResult(), let lines = address.lines else{
                return
            }
            let addresss = self.setCurrentAddressFromGeoCoderAddress(address: address)
            
            let completeAddress = lines.reduce(emptyString, { (result, line) -> String in
                return result + line
            })
            addresss.address = completeAddress
            completion(addresss)
        }
    }
    
    // Method Description: Set these details: country,state, city, place.
    private func setCurrentAddressFromGeoCoderAddress(address: GMSAddress) -> RASAddress {
        print("Address: \(address)")
        let rasAddress = RASAddress()
        rasAddress.location = address.coordinate
        
        if let country = address.country{
            rasAddress.country = country
            if let state = address.administrativeArea{
                rasAddress.state = state
                if let city = address.locality{
                    rasAddress.city = city
                    if let place = address.subLocality{
                        rasAddress.place = place
                    }else{
                        rasAddress.place = city
                    }
                }else{
                    rasAddress.city = state
                    if let place = address.subLocality{
                        rasAddress.place = place
                    }else{
                        rasAddress.place = state
                    }
                }
            }else{
                rasAddress.state = country
                if let city = address.locality{
                    rasAddress.city = city
                    if let place = address.subLocality{
                        rasAddress.place = place
                    }else{
                        rasAddress.place = city
                    }
                }else{
                    rasAddress.city = address.subLocality.leoSafe()
                    if let place = address.subLocality{
                        rasAddress.place = place
                    }else{
                        rasAddress.place = address.subLocality.leoSafe()
                    }
                }
            }
        }
        return rasAddress
    }
    
    
    
    func getCurrentAddress(completion: @escaping((Bool, RASAddress) -> ())) {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                //self.openSettingsURL()
                self.locationManager.requestWhenInUseAuthorization()
                
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
                
                self.closureDidGetLocation = {
                    if let userCurrentLocation = self.userCurrentLocation {
                        self.reverseGeocoderCoordinate(userCurrentLocation.coordinate, completion: { address in
                            print("***********")
                            print("Address")
                            print(address.address)
                            print(address.city)
                            print(address.state)
                            print(address.country)
                            
                            completion(true, address)
                        })
                    }
                }
                
                
            @unknown default:
                print("Unknown location status values")
            }
        }
        else {
            print("Location services are not enabled")
        }
    }
    
    func searchLocation(text: String, completion: @escaping(([GMSAutocompletePrediction]) -> ())) {
        let token = GMSAutocompleteSessionToken()
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        
        placesClient.findAutocompletePredictions(fromQuery: text, bounds: nil, boundsMode: .bias, filter: filter, sessionToken: token) { (results, error) in
            if let err = error {
                print(err.localizedDescription)
                //Alert.showSimple(err.localizedDescription)
                return
            }
            
            if let results = results {
                var predictions = [GMSAutocompletePrediction]()
                for result in results {
                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                    predictions.append(result)
                }
                completion(predictions)
            }
        }
    }
    
    func getSelectedLocationAddress(coordinate: CLLocationCoordinate2D, completion: @escaping((Bool, RASAddress) -> ())) {
        reverseGeocoderCoordinate(coordinate, completion: { address in
            print("***********")
            print("Address")
            print(address.address)
            print(address.city)
            print(address.state)
            print(address.country)
            
            completion(true, address)
        })
    }
    
    // GET COORDINATES USING PLACE_ID WHICH COMES FROM GMS_AUTOCOMPLETE_PREDICTION OBJECT
    func convertPlaceIntoCoordinates(prediction: GMSAutocompletePrediction, completion: @escaping((GMSPlace?, Error?) -> ())) {
        placesClient.lookUpPlaceID(prediction.placeID) { [unowned self] (place, error) in
            if let err = error {
                //Alert.showSimple(err.localizedDescription)
                completion(nil, err)
                return
            }
            completion(place, nil)
        }
    }
    
}


// MARK:- CLLOCATION MANAGER DELEGATE METHODS
extension Location: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        locationManager.stopUpdatingLocation()
        self.userCurrentLocation = location
        if self.closureDidGetLocation != nil {
            self.closureDidGetLocation!()
        }
    }
}
