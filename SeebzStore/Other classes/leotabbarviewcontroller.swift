//
//  leotabbarviewcontroller.swift
//  seebzStore
//
//  Created by MAC on 24/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IBAnimatable


extension UIImage {
    
    func maskWithColor(color: UIColor) -> UIImage? {
        let maskImage = cgImage!
        
        let width = size.width
        let height = size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)!
        
        context.clip(to: bounds, mask: maskImage)
        context.setFillColor(color.cgColor)
        context.fill(bounds)
        
        if let cgImage = context.makeImage() {
            let coloredImage = UIImage(cgImage: cgImage)
            return coloredImage
        } else {
            return nil
        }
    }
    
}
class LeoTabButton: UIButton {
    
    @IBInspectable var selectedColor  : UIColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    @IBInspectable var normalColor  : UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

    @IBInspectable var normalBorderColor  : UIColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    @IBInspectable var selectedBorderColor  : UIColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    
    
    @IBInspectable var backgroundColorL  : UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    @IBInspectable var  selectedImage : UIImage? =  nil
   
    @IBOutlet weak var imageview: UIImageView?
    
    @IBOutlet weak var lbltitle: UILabel?
    
    
    
    @IBOutlet weak var viewBackGround: AnimatableView?
    
    var btnBackgroundView: UIView?
    
    @IBOutlet weak var btnView: UIView?
    
    
    @IBInspectable var identifierVC : String = ""
    
    //@IBOutlet weak var viewContiner: UIView?
    override func awakeFromNib() {
        lbltitle?.textColor = normalColor
        self.tintColor = selectedColor
        viewBackGround?.backgroundColor = backgroundColorL
    }
    
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.btnView?.backgroundColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
              //  imageview?.image = normalImage.maskWithColor(color: selectedColor)
                                 lbltitle?.textColor = selectedColor
            }
            else {
               // imageview?.image = selectedImage!.maskWithColor(color: selectedColor)
                                   lbltitle?.textColor = selectedColor
                self.btnView?.backgroundColor = #colorLiteral(red: 0.07843137255, green: 0.07843137255, blue: 0.07843137255, alpha: 1)
            }
        }
    }
    

}
class LeoTabBarViewController: UIViewController {
    
    
    @IBOutlet var btnViewa: [UIView]!
    
    
    
    @IBOutlet var btnsTabBar: [LeoTabButton]! = []
    
    
    
    @IBOutlet weak var containerView: UIView!
    
   // var isMultipleSelected: Bool?
    
    
    var viewControllers : [UIViewController] = []
    
    static let initailIndex = 0
    
    @IBInspectable var previouslySelectedButtonTag : Int = initailIndex
    
//    override func viewWillLayoutSubviews() {
//        super.viewWillLayoutSubviews()
//        self.modalViewController.invalidateIntrinsicContentSize()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabDestinations()

        let selectedVC = viewControllers[previouslySelectedButtonTag]
        self.addChild(selectedVC)
        selectedVC.view.frame = containerView.bounds
        containerView.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)
        
        _  =    btnsTabBar.map { (button) -> Void in
            button.isSelected = false
          
        }
        
        
        btnsTabBar.forEach { (button) in
            button.isSelected = false
            button.btnView = btnViewa[button.tag]
        }
        
        
        
        if btnsTabBar.count > 0 {
             btnsTabBar[0].isSelected = true
        }
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTabDestinations() {
        
        let storyboard =  self.storyboard

        for vc in btnsTabBar {
            if let navMain7 = storyboard?.instantiateViewController(withIdentifier: vc.identifierVC ) {
                viewControllers.append(navMain7)
            }
        }
    }
    
    
    
    @IBAction func actionTabButton(_ sender: LeoTabButton) {
        
        _ =  btnsTabBar.map { (button) -> Void in
            button.isSelected = false
        }
        sender.isSelected = true
     
        //self.selectedIndex = sender.tag
        
        
        let previousVC = viewControllers[previouslySelectedButtonTag]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        
        // Select new tab
        previouslySelectedButtonTag = sender.tag
        // Add new tab VC
        let selectedVC = viewControllers[sender.tag]
        self.addChild(selectedVC)
        selectedVC.view.frame = containerView.bounds
        containerView.addSubview(selectedVC.view)
        selectedVC.didMove(toParent: self)

    }
    

    @IBAction func pop(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
