//
//  CustomExtensions.swift
//  seebzStore
//
//  Created by Chandan on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func setErrorMessage(withMessage message: String) {
        let label = UILabel()
        
        label.frame = self.backgroundView?.frame ?? CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        label.text = message
        label.textAlignment = .center
    
        self.backgroundView = label
    }
    
    func removeErrorMessage() {
        self.backgroundView = nil
    }
    
}
