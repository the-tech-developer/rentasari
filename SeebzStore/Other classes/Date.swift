//
//  Date.swift
//  seebzStore
//
//  Created by IOS on 12/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

extension DateComponentsFormatter {
    func difference(from fromDate: Date, to toDate: Date) -> String? {
        self.allowedUnits = [.day]
        self.maximumUnitCount = 1
        self.unitsStyle = .full
        return self.string(from: fromDate, to: toDate)?.replacingOccurrences(of: " days", with: "")
    }
}

extension UIAlertController {
    func setAlertActionIcon(title: String, iconName: String) {
        let action = UIAlertAction(title: title, style: .default, handler: nil)
        let image = UIImage(named: iconName)
        action.setValue(image?.withRenderingMode(.alwaysOriginal), forKey: "image")
        self.addAction(action)
    }
}

extension String {
    func convertUTCDateIntoStr(format: String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
        df.locale = Locale(identifier: "en_US_POSIX")
        df.timeZone = TimeZone.current
        let str = df.date(from: self)
        df.dateFormat = format
        let dateStr = df.string(from: str!)
        return dateStr
    }
}
