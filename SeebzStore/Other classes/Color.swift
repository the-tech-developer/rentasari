//
//  Color.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

struct GradienColorConstant {
    
    struct CreateAdView {
        static let kLeft = UIColor.rbg(r: 115, g: 253, b: 255)
        static let kRight = UIColor.rbg(r: 30, g: 200, b: 215)
    }
}
