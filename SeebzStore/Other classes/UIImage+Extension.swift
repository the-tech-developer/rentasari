//
//  UIImage+Extension.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit


extension UIImage {
    
    enum Theme {
        case triangle
        
        var name: String {
            switch self {
            case .triangle: return "triangle"
            }
        }
        
        var image: UIImage {
            return UIImage(named: self.name)!
        }
    }
}
