//
//  UIView+Extension.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func constraintsPinTo(leading: NSLayoutXAxisAnchor, trailing: NSLayoutXAxisAnchor, top: NSLayoutYAxisAnchor, bottom: NSLayoutYAxisAnchor) {
        self.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.leadingAnchor.constraint(equalTo: leading),
            self.trailingAnchor.constraint(equalTo: trailing),
            self.topAnchor.constraint(equalTo: top),
            self.bottomAnchor.constraint(equalTo: bottom)
            ])
    }
    
}


extension UITextField {
    func placeholderColor( _ text : String? = "" ,   color :UIColor? = .gray  ) {
        self.attributedPlaceholder = NSAttributedString(string: text!,
                                                        attributes: [NSAttributedString.Key.foregroundColor:color!])
    }
}

extension UITableView {
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                                      section: self.numberOfSections - 1)
            if indexPath.row > 0 {
                self.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
}
