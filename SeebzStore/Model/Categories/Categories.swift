//
//  Categories.swift
//  seebzStore
//
//  Created by MAC on 20/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - Category
class Category: Codable {
    let catID: Int?
    let name, catImg: String?
    let catStatus: Int?
    let createdAt: String?

    enum CodingKeys: String, CodingKey {
        case catID = "cat_id"
        case name
        case catImg = "cat_img"
        case catStatus = "cat_status"
        case createdAt = "created_at"
    }
}

typealias Categories = [Category]
