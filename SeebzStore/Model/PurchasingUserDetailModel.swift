//
//  PurchasingUserDetailModel.swift
//  seebzStore
//
//  Created by IOS on 07/02/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

class PurchasingUserDetailModel {
    var serverData : [String: Any] = [:]
    var message : String?
    var body : [Body]?
    var success : Int?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
            }
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var user_Detail : User_Detail?
        var product_Detail : [Product_Detail]?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let object = dict["user_Detail"] as? [String : Any] {
                let some =  User_Detail(dict: object)
                self.user_Detail = some }
            if let product_Detail = dict["product_Detail"] as? [[String : Any]] {
                self.product_Detail = []
                for object in product_Detail {
                    let some =  Product_Detail(dict: object)
                    self.product_Detail?.append(some)
                    
                }
            }
        }
        class User_Detail {
            var serverData : [String: Any] = [:]
            var description : String?
            var username : String?
            var user_id : Int?
            var email : String?
            var created_at: String?
            init(dict: [String: Any]){
                self.serverData = dict
                
                if let description = dict["description"] as? String {
                    self.description = description
                }
                if let username = dict["username"] as? String {
                    self.username = username
                }
                if let user_id = dict["user_id"] as? Int {
                    self.user_id = user_id
                }
                if let email = dict["email"] as? String {
                    self.email = email
                }
                if let created_at = dict["created_at"] as? String {
                    self.created_at = created_at
                }
                
                
                
            }
        }
        class Product_Detail {
            var serverData : [String: Any] = [:]
            var longitude : String?
            var ad_type : Int?
            var title : String?
            var user_id : Int?
            var name : String?
            var location : String?
            var latiude : String?
            var product_image : String?
            var product_id : Int?
            var username : String?
            var price : Int?
            init(dict: [String: Any]){
                self.serverData = dict
                
                if let longitude = dict["longitude"] as? String {
                    self.longitude = longitude
                }
                if let ad_type = dict["ad_type"] as? Int {
                    self.ad_type = ad_type
                }
                if let title = dict["title"] as? String {
                    self.title = title
                }
                if let user_id = dict["user_id"] as? Int {
                    self.user_id = user_id
                }
                if let name = dict["name"] as? String {
                    self.name = name
                }
                if let location = dict["location"] as? String {
                    self.location = location
                }
                if let latiude = dict["latiude"] as? String {
                    self.latiude = latiude
                }
                if let product_image = dict["product_image"] as? String {
                    self.product_image = product_image
                }
                if let product_id = dict["product_id"] as? Int {
                    self.product_id = product_id
                }
                if let username = dict["username"] as? String {
                    self.username = username
                }
                if let price = dict["price"] as? Int {
                    self.price = price
                }
            }
        }
    }
}
