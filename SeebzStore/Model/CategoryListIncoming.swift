//
//  CategoryListIncoming.swift
//  seebzStore
//
//  Created by IOS on 03/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation

class CategoryListIncoming  {
    var serverData : [String: Any] = [:]
    var body : [Body]?
    var message : String?
    var success : Int?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
            }
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
    }
    class Body  {
        var serverData : [String: Any] = [:]
        var name : String?
        var cat_id : Int?
        var cat_img : String?
        var created_at : String?
        var cat_status : Int?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let name = dict["name"] as? String {
                self.name = name
            }
            if let cat_id = dict["cat_id"] as? Int {
                self.cat_id = cat_id
            }
            if let cat_img = dict["cat_img"] as? String {
                self.cat_img = cat_img
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let cat_status = dict["cat_status"] as? Int {
                self.cat_status = cat_status
            }
        }
    }
}
