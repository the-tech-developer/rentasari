//
//  TrackPackage.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - TrackPackage
class TrackPackage: Codable {
    let trackingNumber, statusCode, statusDescription, carrierStatusCode: String?
    let carrierStatusDescription, shipDate: String?
    let estimatedDeliveryDate: String?
    let actualDeliveryDate: String?
    let exceptionDescription: String?
    let events: [Event]?

    enum CodingKeys: String, CodingKey {
        case trackingNumber = "tracking_number"
        case statusCode = "status_code"
        case statusDescription = "status_description"
        case carrierStatusCode = "carrier_status_code"
        case carrierStatusDescription = "carrier_status_description"
        case shipDate = "ship_date"
        case estimatedDeliveryDate = "estimated_delivery_date"
        case actualDeliveryDate = "actual_delivery_date"
        case exceptionDescription = "exception_description"
        case events
    }
}

// MARK: - Event
class Event: Codable {
    let occurredAt: Date?
    let carrierOccurredAt, eventDescription, cityLocality, stateProvince: String?
    let postalCode, countryCode, companyName, signer: String?
    let eventCode: String?

    enum CodingKeys: String, CodingKey {
        case occurredAt = "occurred_at"
        case carrierOccurredAt = "carrier_occurred_at"
        case eventDescription = "description"
        case cityLocality = "city_locality"
        case stateProvince = "state_province"
        case postalCode = "postal_code"
        case countryCode = "country_code"
        case companyName = "company_name"
        case signer
        case eventCode = "event_code"
    }
}
