//
//  GetShippingTypes.swift
//  seebzStore
//
//  Created by MAC on 23/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - GetShippingType
struct GetShippingType: Codable {
    let carrierID: String?
    let carrierCode: String?
    let accountNumber: String?
    let requiresFundedAmount: Bool?
    let balance: Double?
    let nickname, friendlyName: String?
    let primary, hasMultiPackageSupportingServices, supportsLabelMessages: Bool?
    let services: [Service]?
    let packages: [Package]?
    let options: [Option]?

    enum CodingKeys: String, CodingKey {
        case carrierID = "carrier_id"
        case carrierCode = "carrier_code"
        case accountNumber = "account_number"
        case requiresFundedAmount = "requires_funded_amount"
        case balance, nickname
        case friendlyName = "friendly_name"
        case primary
        case hasMultiPackageSupportingServices = "has_multi_package_supporting_services"
        case supportsLabelMessages = "supports_label_messages"
        case services, packages, options
    }
}

//enum CarrierCode: String, Codable {
//    case fedex = "fedex"
//   // case stampsCOM = "stamps_com"
//   // case ups = "ups"
//}

//enum CarrierID: String, Codable {
//    case se199763 = "se-199763"
////    case se199764 = "se-199764"
//    case se199765 = "se-199765" //for fedex
//}

// MARK: - Option
struct Option: Codable {
    let name, defaultValue, optionDescription: String?

    enum CodingKeys: String, CodingKey {
        case name
        case defaultValue = "default_value"
        case optionDescription = "description"
    }
}

// MARK: - Package
struct Package: Codable {
    let packageID: String?
    let packageCode, name, packageDescription: String?

    enum CodingKeys: String, CodingKey {
        case packageID = "package_id"
        case packageCode = "package_code"
        case name
        case packageDescription = "description"
    }
}

// MARK: - Service
struct Service: Codable {
    let carrierID: String?
    let carrierCode: String?
    let serviceCode, name: String?
    let domestic, international, isMultiPackageSupported: Bool?

    enum CodingKeys: String, CodingKey {
        case carrierID = "carrier_id"
        case carrierCode = "carrier_code"
        case serviceCode = "service_code"
        case name, domestic, international
        case isMultiPackageSupported = "is_multi_package_supported"
    }
}

typealias GetShippingTypes = [GetShippingType]
