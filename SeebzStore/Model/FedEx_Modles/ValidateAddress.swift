//
//  ValidateAddress.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - ValidateAddressElement
class ValidateAddressElement: Codable {
    let status: String?
    let originalAddress, matchedAddress: Address?
    let messages: [String]?
    
    enum CodingKeys: String, CodingKey {
        case status
        case originalAddress = "original_address"
        case matchedAddress = "matched_address"
        case messages
    }
}

// MARK: - Address
class Address: Codable {
    let name, phone, companyName: String?
    let addressLine1: String?
    let addressLine2: String?
    let addressLine3: String?
    let cityLocality, stateProvince, postalCode, countryCode: String?
    let addressResidentialIndicator: String?
    
    enum CodingKeys: String, CodingKey {
        case name, phone
        case companyName = "company_name"
        case addressLine1 = "address_line1"
        case addressLine2 = "address_line2"
        case addressLine3 = "address_line3"
        case cityLocality = "city_locality"
        case stateProvince = "state_province"
        case postalCode = "postal_code"
        case countryCode = "country_code"
        case addressResidentialIndicator = "address_residential_indicator"
    }
}

typealias ValidateAddress = [ValidateAddressElement]
