//
//  GetRates.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - GetShippingRates
class GetShippingRates: Codable {
    let rateResponse: RateResponse?
    let shipmentID, carrierID: String?
    let externalShipmentID: String?
    let shipDate, createdAt, modifiedAt, shipmentStatus: String?
    let shipTo, shipFrom, returnTo: ReturnTo?
    let confirmation: String?
    let advancedOptions: AdvancedOptions?
    let insuranceProvider: String?
    let tags: [String]?
    //let totalWeight: Weight?
    let packages: [Package]?

    enum CodingKeys: String, CodingKey {
        case rateResponse = "rate_response"
        case shipmentID = "shipment_id"
        case carrierID = "carrier_id"
        case externalShipmentID = "external_shipment_id"
        case shipDate = "ship_date"
        case createdAt = "created_at"
        case modifiedAt = "modified_at"
        case shipmentStatus = "shipment_status"
        case shipTo = "ship_to"
        case shipFrom = "ship_from"
        case returnTo = "return_to"
        case confirmation
        case advancedOptions = "advanced_options"
        case insuranceProvider = "insurance_provider"
        case tags
       // case totalWeight = "total_weight"
        case packages
    }
}

// MARK: - AdvancedOptions
class AdvancedOptions: Codable {
    let billToAccount, billToCountryCode, billToParty, billToPostalCode: String?
    let containsAlcohol: Bool?
    let customField1, customField2, customField3: String?
    let nonMachinable, saturdayDelivery: Bool?

    enum CodingKeys: String, CodingKey {
        case billToAccount = "bill_to_account"
        case billToCountryCode = "bill_to_country_code"
        case billToParty = "bill_to_party"
        case billToPostalCode = "bill_to_postal_code"
        case containsAlcohol = "contains_alcohol"
        case customField1 = "custom_field1"
        case customField2 = "custom_field2"
        case customField3 = "custom_field3"
        case nonMachinable = "non_machinable"
        case saturdayDelivery = "saturday_delivery"
    }
}



// MARK: - Dimensions
class Dimensions: Codable {
    let unit: String?
    let length, width, height: Int?
    
    init(unit: String , length: Int , width: Int , height: Int) {
        self.unit = unit
        self.length = length
        self.width = width
        self.height = height
    }
}

// MARK: - InsuredValue
class InsuredValue: Codable {
    let currency: String?
    let amount: Double?
}

// MARK: - RateResponse
class RateResponse: Codable {
    let rateRequestID: Int?
    let shipmentID, status, createdAt: String?
    let rates: [Rate]?
   // let invalidRates: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case rateRequestID = "rate_request_id"
        case shipmentID = "shipment_id"
        case status
        case createdAt = "created_at"
        case rates
        //case invalidRates = "invalid_rates"
    }
}

// MARK: - Rate
class Rate: Codable {
    let rateID, rateType, carrierID: String?
    let shippingAmount, insuranceAmount, confirmationAmount, otherAmount: InsuredValue?
    let deliveryDays: Int?
    let guaranteedService: Bool?
    let estimatedDeliveryDate, carrierDeliveryDays, shipDate: String?
    let negotiatedRate: Bool?
    let serviceType, serviceCode: String?
    let trackable: Bool?
    let validationStatus: String?
    let warningMessages, errorMessages: [String]?
    let carrierCode, carrierNickname, carrierFriendlyName: String?

    enum CodingKeys: String, CodingKey {
        case rateID = "rate_id"
        case rateType = "rate_type"
        case carrierID = "carrier_id"
        case shippingAmount = "shipping_amount"
        case insuranceAmount = "insurance_amount"
        case confirmationAmount = "confirmation_amount"
        case otherAmount = "other_amount"
        case deliveryDays = "delivery_days"
        case guaranteedService = "guaranteed_service"
        case estimatedDeliveryDate = "estimated_delivery_date"
        case carrierDeliveryDays = "carrier_delivery_days"
        case shipDate = "ship_date"
        case negotiatedRate = "negotiated_rate"
        case serviceType = "service_type"
        case serviceCode = "service_code"
        case trackable
        case validationStatus = "validation_status"
        case warningMessages = "warning_messages"
        case errorMessages = "error_messages"
        case carrierCode = "carrier_code"
        case carrierNickname = "carrier_nickname"
        case carrierFriendlyName = "carrier_friendly_name"
    }
}

// MARK: - ReturnTo
class ReturnTo: Codable {
    let companyName, name, phone, addressLine1: String?
    let addressLine2, cityLocality, stateProvince, postalCode: String?
    let countryCode, addressResidentialIndicator: String?

    enum CodingKeys: String, CodingKey {
        case companyName = "company_name"
        case name, phone
        case addressLine1 = "address_line1"
        case addressLine2 = "address_line2"
        case cityLocality = "city_locality"
        case stateProvince = "state_province"
        case postalCode = "postal_code"
        case countryCode = "country_code"
        case addressResidentialIndicator = "address_residential_indicator"
    }
}

// MARK: - Encode/decode helpers


// MARK: - GetShippingRatesParams
struct GetShippingRatesParams: Codable {
    let shipmentID: String?
    let rateOptions: RateOptions?

    enum CodingKeys: String, CodingKey {
        case shipmentID = "shipment_id"
        case rateOptions = "rate_options"
    }
}

// MARK: - RateOptions
struct RateOptions: Codable {
    let carrierIDS: [String]?

    enum CodingKeys: String, CodingKey {
        case carrierIDS = "carrier_ids"
    }
}
