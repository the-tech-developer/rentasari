//
//  CreateLabel.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - CreateLabel
class CreateLabel: Codable {
    let labelID, status, shipmentID, shipDate: String?
    let createdAt: String?
    let shipmentCost, insuranceCost: InsuranceCost?
    let trackingNumber: String?
    let isReturnLabel, isInternational: Bool?
    let batchID, carrierID, serviceCode, packageCode: String?
    let voided: Bool?
    let labelFormat, labelLayout: String?
    let trackable: Bool?
    let carrierCode, trackingStatus: String?
    let labelDownload: LabelDownload?
    let formDownload, insuranceClaim: String?
    let packages: [Package]?
    
    enum CodingKeys: String, CodingKey {
        case labelID = "label_id"
        case status
        case shipmentID = "shipment_id"
        case shipDate = "ship_date"
        case createdAt = "created_at"
        case shipmentCost = "shipment_cost"
        case insuranceCost = "insurance_cost"
        case trackingNumber = "tracking_number"
        case isReturnLabel = "is_return_label"
        case isInternational = "is_international"
        case batchID = "batch_id"
        case carrierID = "carrier_id"
        case serviceCode = "service_code"
        case packageCode = "package_code"
        case voided
        case labelFormat = "label_format"
        case labelLayout = "label_layout"
        case trackable
        case carrierCode = "carrier_code"
        case trackingStatus = "tracking_status"
        case labelDownload = "label_download"
        case formDownload = "form_download"
        case insuranceClaim = "insurance_claim"
        case packages
    }
}

// MARK: - InsuranceCost
class InsuranceCost: Codable {
    let currency: String?
    let amount: Double?
}

// MARK: - LabelDownload
class LabelDownload: Codable {
    let pdf: String?
    let png: String?
    let zpl: String?
    let href: String?
}


// MARK: - Package
struct PackageWeight: Codable {
      let packageCode: String?
      let weight: Weight?
      let dimensions: Dimensions?

      enum CodingKeys: String, CodingKey {
          case packageCode = "package_code"
          case weight , dimensions
      }
}

// MARK: - Dimensions
struct DimensionsBox: Codable {
    let unit: String?
    let length: Int?
    let width: Double?
    let height: Int?
}

//// MARK: - GetShippingTypes
//struct FinalShipments: Codable {
//    let shipment: Shipment?
//}
//
//// MARK: - Shipment
//struct Shipment: Codable {
//    let serviceCode: String?
//    let shipTo, shipFrom: Ship?
//    let confirmation, insuranceProvider: String?
//    let packages: [PackageWeight]?
//
//    enum CodingKeys: String, CodingKey {
//        case serviceCode = "service_code"
//        case shipTo = "ship_to"
//        case shipFrom = "ship_from"
//        case confirmation
//        case insuranceProvider = "insurance_provider"
//        case packages
//    }
//}
//
//// MARK: - Weight
//struct Weight: Codable {
//    let value: Int?
//    let unit: String?
//}
//
//// MARK: - Ship
//struct Ship: Codable {
//    let companyName, name, phone, addressLine1: String?
//    let addressLine2, cityLocality, stateProvince, postalCode: String?
//    let countryCode, addressResidentialIndicator: String?
//
//    enum CodingKeys: String, CodingKey {
//        case companyName = "company_name"
//        case name, phone
//        case addressLine1 = "address_line1"
//        case addressLine2 = "address_line2"
//        case cityLocality = "city_locality"
//        case stateProvince = "state_province"
//        case postalCode = "postal_code"
//        case countryCode = "country_code"
//        case addressResidentialIndicator = "address_residential_indicator"
//    }
//}
//


// MARK: - GetShippingTypes
struct FinalShipment: Codable {
    let shipment: Shipment?
}

// MARK: - Shipment
struct Shipment: Codable {
    let serviceCode: String?
    let shipFrom, shipTo: Ship?
    let customs: Customs?
    let packages: [PackageWeight]?

    enum CodingKeys: String, CodingKey {
        case serviceCode = "service_code"
        case shipFrom = "ship_from"
        case shipTo = "ship_to"
        case customs, packages
    }
}

// MARK: - Customs
struct Customs: Codable {
    let contents: String?
    let customsItems: [CustomsItem]?
    let nonDelivery: String?

    enum CodingKeys: String, CodingKey {
        case contents
        case customsItems = "customs_items"
        case nonDelivery = "non_delivery"
    }
}

// MARK: - CustomsItem
struct CustomsItem: Codable {
    let customsItemDescription: String?
    let quantity, value: Int?
    let harmonizedTariffCode, countryOfOrigin: String?

    enum CodingKeys: String, CodingKey {
        case customsItemDescription = "description"
        case quantity, value
        case harmonizedTariffCode = "harmonized_tariff_code"
        case countryOfOrigin = "country_of_origin"
    }
}

// MARK: - Weight
struct Weight: Codable {
    let value: Int?
    let unit: String?
    let dimensions: Dimensions?
}

// MARK: - Ship
struct Ship: Codable {
    let companyName, name, phone, addressLine1: String?
    let stateProvince, postalCode, countryCode, addressResidentialIndicator: String?
    let cityLocality: String?

    enum CodingKeys: String, CodingKey {
        case companyName = "company_name"
        case name, phone
        case addressLine1 = "address_line1"
        case stateProvince = "state_province"
        case postalCode = "postal_code"
        case countryCode = "country_code"
        case addressResidentialIndicator = "address_residential_indicator"
        case cityLocality = "city_locality"
    }
}


/**
 
 {
   "shipment": {
     "service_code": "fedex_international_priority_freight",
     "ship_to": {
       "name": "Amanda Miller",
       "phone": "555-555-5555",
       "address_line1": "525 S Winchester Blvd",
       "city_locality": "San Jose",
       "state_province": "CA",
       "postal_code": "95128",
       "country_code": "US",
       "address_residential_indicator": "no"
     },
     "ship_from": {
       "company_name": "Example Corp.",
       "name": "John Doe",
       "phone": "111-111-1111",
       "address_line1": "4009 Marathon Blvd",
       "address_line2": "Suite 300",
       "city_locality": "Ludhiana",
       "state_province": "PB",
       "postal_code": "141010",
       "country_code": "IN",
       "address_residential_indicator": "no"
     },
     "confirmation": "none",
     "insurance_provider": "none",
     "packages": [
       {
         "package_code": "fedex_10kg_box",
         "weight": {
           "value": 1.0,
           "unit": "ounce"
         }
       }
     ],
     "customs": {
             "contents": "documents",
             "customs_items": [{
                 "description": "letter",
                 "quantity": 1,
                 "value": 1.0,
                 "harmonized_tariff_code": "4817.20",
                 "country_of_origin": "US"
             }],
             "non_delivery": "treat_as_abandoned"
         }
   }
 }
 
 */
