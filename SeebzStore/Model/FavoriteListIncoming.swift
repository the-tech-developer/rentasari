//
//  FavoriteListIncoming.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation

class FavoriteListIncoming {
    var serverData : [String: Any] = [:]
    var body : [Body]?
    var message : String?
    var success : Int?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var payment_id : Int?
        var longitude : String?
        var product_image : String?
        var booking_by : Int?
        var ad_days : Int?
        var return_address_line_2 : String?
        var created_at : String?
        var is_extend : Int?
        var title : String?
        var price : Int?
        var user_id : Int?
        var return_address_city : String?
        var status : Int?
        var latiude : String?
        var amount : Int?
        var product_id : Int?
        var ad_type : Int?
        var return_address_line_1 : String?
        var description : String?
        var ad_id : Int?
        var start_date : String?
        var location : String?
        var return_address_state : String?
        var cat_id : Int?
        var cart_id : Int?
        var payment_status : Int?
        var end_date : String?
        var username : String?
        var is_favroite : Int?
        var email : String?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let email = dict["email"] as? String {
                self.email = email
            }
            if let payment_id = dict["payment_id"] as? Int {
                self.payment_id = payment_id
            }
            if let longitude = dict["longitude"] as? String {
                self.longitude = longitude
            }
            if let product_image = dict["product_image"] as? String {
                self.product_image = product_image
            }
            if let booking_by = dict["booking_by"] as? Int {
                self.booking_by = booking_by
            }
            if let ad_days = dict["ad_days"] as? Int {
                self.ad_days = ad_days
            }
            if let return_address_line_2 = dict["return_address_line_2"] as? String {
                self.return_address_line_2 = return_address_line_2
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let is_extend = dict["is_extend"] as? Int {
                self.is_extend = is_extend
            }
            if let title = dict["title"] as? String {
                self.title = title
            }
            if let price = dict["price"] as? Int {
                self.price = price
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let return_address_city = dict["return_address_city"] as? String {
                self.return_address_city = return_address_city
            }
            if let status = dict["status"] as? Int {
                self.status = status
            }
            if let latiude = dict["latiude"] as? String {
                self.latiude = latiude
            }
            if let amount = dict["amount"] as? Int {
                self.amount = amount
            }
            if let product_id = dict["product_id"] as? Int {
                self.product_id = product_id
            }
            if let ad_type = dict["ad_type"] as? Int {
                self.ad_type = ad_type
            }
            if let return_address_line_1 = dict["return_address_line_1"] as? String {
                self.return_address_line_1 = return_address_line_1
            }
            if let description = dict["description"] as? String {
                self.description = description
            }
            if let ad_id = dict["ad_id"] as? Int {
                self.ad_id = ad_id
                
            }
            if let start_date = dict["start_date"] as? String {
                self.start_date = start_date
            }
            if let location = dict["location"] as? String {
                self.location = location
            }
            if let return_address_state = dict["return_address_state"] as? String {
                self.return_address_state = return_address_state
            }
            if let cat_id = dict["cat_id"] as? Int {
                self.cat_id = cat_id
            }
            if let cart_id = dict["cart_id"] as? Int {
                self.cart_id = cart_id
            }
            if let payment_status = dict["payment_status"] as? Int {
                self.payment_status = payment_status
            }
            if let end_date = dict["end_date"] as? String {
                self.end_date = end_date
            }
            if let username = dict["username"] as? String {
                self.username = username
            }
            if let is_favroite = dict["is_favroite"] as? Int {
                self.is_favroite = is_favroite
            }
        }
    }
}
