//
//  MessageListIncoming.swift
//  seebzStore
//
//  Created by MAC on 05/02/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

class MessageListIncoming {
    var serverData : [String: Any] = [:]
    var body : [Body]?
    var success : Int?
    var message : String?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var chat_id : String?
        var message : String?
        var user_id : Int?
        var created_at : String?
        var status : Int?
        var msg_id : Int?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let chat_id = dict["chat_id"] as? String {
                self.chat_id = chat_id
            }
            if let message = dict["message"] as? String {
                self.message = message
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let status = dict["status"] as? Int {
                self.status = status
            }
            if let msg_id = dict["msg_id"] as? Int {
                self.msg_id = msg_id
            }
        }
    }
}
