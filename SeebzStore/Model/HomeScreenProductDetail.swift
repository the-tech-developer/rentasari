//
//  HomeScreenProductDetail.swift
//  seebzStore
//
//  Created by IOS on 23/01/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

class HomeScreenProductDetail {
    
    var serverData : [String: Any] = [:]
    var success : Int?
    var body : [Body]?
    var message : String?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let success = dict["success"] as? Int {
            self.success = success
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var product_images : [Product_Images]?
        var product_ads : Product_Ads?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let product_images = dict["product_images"] as? [[String : Any]] {
                self.product_images = []
                for object in product_images {
                    let some =  Product_Images(dict: object)
                    self.product_images?.append(some)
                    
                }
            }
            if let object = dict["product_ads"] as? [String : Any] {
                let some =  Product_Ads(dict: object)
                self.product_ads = some }
        }
        class Product_Images {
            var serverData : [String: Any] = [:]
            var status : Int?
            var product_image : String?
            var product_image_id : Int?
            var created_at : String?
            var product_id : Int?
            var updated_at : String?
            init(dict: [String: Any]){
                self.serverData = dict
                
                if let status = dict["status"] as? Int {
                    self.status = status
                }
                if let product_image = dict["product_image"] as? String {
                    self.product_image = product_image
                }
                if let product_image_id = dict["product_image_id"] as? Int {
                    self.product_image_id = product_image_id
                }
                if let created_at = dict["created_at"] as? String {
                    self.created_at = created_at
                }
                if let product_id = dict["product_id"] as? Int {
                    self.product_id = product_id
                }
                if let updated_at = dict["updated_at"] as? String {
                    self.updated_at = updated_at
                }
            }
        }
        class Product_Ads {
            var serverData : [String: Any] = [:]
            var ad_id : Int?
            var user_id : Int?
            var amount : Int?
            var location : String?
            var ad_type : Int?
            var title : String?
            var latiude : String?
            var description : String?
            var longitude : String?
            var end_date : String?
            var start_date : String?
            var price : Int?
            var email : String?
            var username : String?
            var created_at : String?
            var product_id : Int?
            var category_name : String?
            init(dict: [String: Any]){
                self.serverData = dict
                
                if let ad_id = dict["ad_id"] as? Int {
                    self.ad_id = ad_id
                }
                if let user_id = dict["user_id"] as? Int {
                    self.user_id = user_id
                }
                if let amount = dict["amount"] as? Int {
                    self.amount = amount
                }
                if let location = dict["location"] as? String {
                    self.location = location
                }
                if let ad_type = dict["ad_type"] as? Int {
                    self.ad_type = ad_type
                }
                if let title = dict["title"] as? String {
                    self.title = title
                }
                if let latiude = dict["latiude"] as? String {
                    self.latiude = latiude
                }
                if let description = dict["description"] as? String {
                    self.description = description
                }
                if let longitude = dict["longitude"] as? String {
                    self.longitude = longitude
                }
                if let end_date = dict["end_date"] as? String {
                    self.end_date = end_date
                }
                if let start_date = dict["start_date"] as? String {
                    self.start_date = start_date
                }
                if let price = dict["price"] as? Int {
                    self.price = price
                }
                if let email = dict["email"] as? String {
                    self.email = email
                }
                if let username = dict["username"] as? String {
                    self.username = username
                }
                if let created_at = dict["created_at"] as? String {
                    self.created_at = created_at
                }
                if let product_id = dict["product_id"] as? Int {
                    self.product_id = product_id
                }
                if let category_name = dict["category_name"] as? String {
                    self.category_name = category_name
                }
            }
        }
    }
}
