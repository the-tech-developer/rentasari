//
//  OtherModels.swift
//  seebzStore
//
//  Created by IOS on 12/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

enum ScreenComeFrom {
    case none
    case signUp
    case homeVC
    case createAd
    case editAd
    case addressVC
    case wishList
    case bellIcon
}

enum FavoriteStatus : Int {
    case isFavorite = 1
    case isUnfavorite = 0
}
enum PublishDisableStatus : Int {
    case Publish = 0
    case Disasble = 1
}
