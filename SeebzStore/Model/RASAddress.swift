//
//  UserAddress.swift
//  seebzStore
//
//  Created by IOS on 11/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import CoreLocation
import UIKit

class RASAddress{
    
    init() {
        //print("UserAddress Object Created")
    }
    deinit {
        //print("UserAddress Object Destroyed")
    }
    
    private var _address: String?
    private var _country: String?
    private var _state: String?
    private var _city: String?
    private var _place: String?
    private var _location = CLLocationCoordinate2D()
    
    
    var location: CLLocationCoordinate2D{
        set{
            _location = newValue
        }
        get{
            return _location
        }
    }
    
    var place : String{
        set{
            _place = newValue
        }
        get{
            if _place == nil{
                _place = emptyString
            }
            return _place!
        }
    }
    
    var city: String{
        set{
            _city = newValue
        }
        get{
            if _city == nil{
                _city = emptyString
            }
            return _city!
        }
    }
    
    var state : String{
        set{
            _state = newValue
        }
        get{
            if _state == nil{
                _state = emptyString
            }
            return _state!
        }
    }
    
    var country: String{
        set{
            _country = newValue
        }
        get{
            if _country == nil{
                _country = emptyString
            }
            return _country!
        }
    }
    
    var address: String{
        set{
            _address = newValue
        }
        get{
            if _address == nil{
                _address = emptyString
            }
            return _address!
        }
    }
    
}
