//
//  MyAdsModle.swift
//  seebzStore
//
//  Created by MAC on 19/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - MyAd
class MyAd: Codable {
    let adID, productID, userID, adType: Int?
    let location, latiude, longitude: String?
    let adDays: Int?
    let startDate, endDate: String?
    let isExtend: Int?
    let returnAddressLine1, returnAddressLine2, returnAddressState, returnAddressCity: String?
    let bookingBy, amount: Int?
    let myAdDescription: String?
    let status: Int?
    let dealAccept1Reject2: String?
    let paymentID, cartID, paymentStatus: Int?
    let createdAt, title: String?
    let price: Int?
    let productDescription, catname, productImage: String?

    enum CodingKeys: String, CodingKey {
        case adID = "ad_id"
        case productID = "product_id"
        case userID = "user_id"
        case adType = "ad_type"
        case location, latiude, longitude
        case adDays = "ad_days"
        case startDate = "start_date"
        case endDate = "end_date"
        case isExtend = "is_extend"
        case returnAddressLine1 = "return_address_line_1"
        case returnAddressLine2 = "return_address_line_2"
        case returnAddressState = "return_address_state"
        case returnAddressCity = "return_address_city"
        case bookingBy = "booking_by"
        case amount
        case myAdDescription = "description"
        case status
        case dealAccept1Reject2 = "deal(accept-1,reject-2)"
        case paymentID = "payment_id"
        case cartID = "cart_id"
        case paymentStatus = "payment_status"
        case createdAt = "created_at"
        case title, price, productDescription, catname
        case productImage = "product_image"
    }
}

typealias MyAds = [MyAd]
