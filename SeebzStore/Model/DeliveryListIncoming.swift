//
//  DeliveryListIncoming.swift
//  seebzStore
//
//  Created by IOS on 17/01/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation
import UIKit

class DeliveryListIncoming {
    var serverData : [String: Any] = [:]
    var message : String?
    var body : [Body]?
    var success : Int?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var status : Int?
        var address_id : Int?
        var created_at : String?
        var address_line_1 : String?
        var state : String?
        var phone_no : Int?
        var address_line_2 : String?
        var user_id : Int?
        var name : String?
        var city : String?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let status = dict["status"] as? Int {
                self.status = status
            }
            if let address_id = dict["address_id"] as? Int {
                self.address_id = address_id
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let address_line_1 = dict["address_line_1"] as? String {
                self.address_line_1 = address_line_1
            }
            if let state = dict["state"] as? String {
                self.state = state
            }
            if let phone_no = dict["phone_no"] as? Int {
                self.phone_no = phone_no
            }
            if let address_line_2 = dict["address_line_2"] as? String {
                self.address_line_2 = address_line_2
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let name = dict["name"] as? String {
                self.name = name
            }
            if let city = dict["city"] as? String {
                self.city = city
            }
        }
    }
}
