//
//  NotificationListModel.swift
//  seebzStore
//
//  Created by IOS on 05/02/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

class NotificationListModel {
    var serverData : [String: Any] = [:]
    var body : [Body]?
    var success : Int?
    var message : String?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
            }
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var user_id : Int?
        var created_at : String?
        var notification_id : Int?
        var message : String?
        var image : String?
        var status: Int?
        var product_ad_id : Int?
        var title : String?
        var name: String?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let notification_id = dict["notification_id"] as? Int {
                self.notification_id = notification_id
            }
            if let message = dict["message"] as? String {
                self.message = message
            }
            if let image = dict["image"] as? String {
                self.image = image
            }
            if let product_ad_id = dict["product_ad_id"] as? Int {
                self.product_ad_id = product_ad_id
            }
            if let title = dict["title"] as? String {
                self.title = title
            }
            if let name = dict["name"] as? String {
                self.name = name
            }
            
            if let status = dict["status"] as? Int {
                self.status = status
            }
        }
    }
}
