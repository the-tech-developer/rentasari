//
//  ChatListIncoming.swift
//  seebzStore
//
//  Created by MAC on 05/02/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

class ChatListIncoming {
    var serverData : [String: Any] = [:]
    var success : Int?
    var body : [Body]?
    var message : String?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let success = dict["success"] as? Int {
            self.success = success
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var chat_id : String?
        var last_msg : String?
        var unread_msg : Int?
        var product_image : String?
        var username : String?
        var receiver_id : Int?
        var created_at : String?
        var product_id : Int?
        var title : String?
        var sender_id : Int?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let chat_id = dict["chat_id"] as? String {
                self.chat_id = chat_id
            }
            if let last_msg = dict["last_msg"] as? String {
                self.last_msg = last_msg
            }
            if let unread_msg = dict["unread_msg"] as? Int {
                self.unread_msg = unread_msg
            }
            if let product_image = dict["product_image"] as? String {
                self.product_image = product_image
            }
            if let username = dict["username"] as? String {
                self.username = username
            }
            if let receiver_id = dict["receiver_id"] as? Int {
                self.receiver_id = receiver_id
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let product_id = dict["product_id"] as? Int {
                self.product_id = product_id
            }
            if let title = dict["title"] as? String {
                self.title = title
            }
            if let sender_id = dict["sender_id"] as? Int {
                self.sender_id = sender_id
            }
        }
    }
}
