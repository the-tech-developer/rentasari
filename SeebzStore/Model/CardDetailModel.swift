//
//  CardDetailModel.swift
//  seebzStore
//
//  Created by IOS on 17/01/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation
import UIKit

class CardDetailModel {
    var serverData : [String: Any] = [:]
    var message : String?
    var body : [Body]?
    var success : Int?
    init(dict: [String: Any]){
        self.serverData = dict
        
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.body = []
            for object in body {
                let some =  Body(dict: object)
                self.body?.append(some)
                
            }
        }
        if let success = dict["success"] as? Int {
            self.success = success
        }
    }
    class Body {
        var serverData : [String: Any] = [:]
        var card_status : Int?
        var cvv : Int?
        var user_id : Int?
        var expire_month : String?
        var expire_year : String?
        var created_at : String?
        var cart_no : String?
        var card_holder_name : String?
        var card_id : Int?
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let card_status = dict["card_status"] as? Int {
                self.card_status = card_status
            }
            if let cvv = dict["cvv"] as? Int {
                self.cvv = cvv
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let expire_month = dict["expire_month"] as? String {
                self.expire_month = expire_month
            }
            if let expire_year = dict["expire_year"] as? String {
                self.expire_year = expire_year
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let cart_no = dict["cart_no"] as? String {
                self.cart_no = cart_no
            }
            if let card_holder_name = dict["card_holder_name"] as? String {
                self.card_holder_name = card_holder_name
            }
            if let card_id = dict["card_id"] as? Int {
                self.card_id = card_id
            }
        }
    }
}
