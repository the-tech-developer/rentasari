//
//  ProductDetails.swift
//  seebzStore
//
//  Created by MAC on 20/03/20.
//  Copyright © 2020 IOS. All rights reserved.
//

import Foundation

// MARK: - ProductDetail
class ProductDetail: Codable {
    let productAds: ProductAds?
    let productImages: [ProductImage]?

    enum CodingKeys: String, CodingKey {
        case productAds = "product_ads"
        case productImages = "product_images"
    }
}

// MARK: - ProductAds
class ProductAds: Codable {
    let username, createdAt: String?
    let productID: Int?
    let email: String?
    let adType: Int?
    let location: String?
    let adDays: Int?
    let latiude, longitude: String?
    let adID, amount: Int?
    let startDate, endDate, title: String?
    let userID: Int?
    let productAdsDescription: String?
    let price, catID: Int?
    let categoryName: String?

    enum CodingKeys: String, CodingKey {
        case username
        case createdAt = "created_at"
        case productID = "product_id"
        case email
        case adType = "ad_type"
        case location
        case adDays = "ad_days"
        case latiude, longitude
        case adID = "ad_id"
        case amount
        case startDate = "start_date"
        case endDate = "end_date"
        case title
        case userID = "user_id"
        case productAdsDescription = "description"
        case price
        case catID = "cat_id"
        case categoryName = "category_name"
    }
}

// MARK: - ProductImage
class ProductImage: Codable {
    let productImageID, productID: Int?
    let productImage: String?
    let status: Int?
    let createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case productImageID = "product_image_id"
        case productID = "product_id"
        case productImage = "product_image"
        case status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

typealias ProductDetails = [ProductDetail]
