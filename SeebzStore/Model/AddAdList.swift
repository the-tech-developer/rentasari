//
//  AddAdList.swift
//  seebzStore
//
//  Created by IOS on 12/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit


struct AddAdList {
    let ad_type : String
    let title : String
    let description : String
    let price : String
    let days : String
    let isExtend : String
    var images : [UIImage]?
    var address : String
    var latitude : Double
    var longitude : Double
    let user_id : String
    let cat_id : String
    let cat_name : String
    let email : String
    let product_id: String
   
}

