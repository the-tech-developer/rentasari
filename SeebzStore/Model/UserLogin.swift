//
//  UserLogin.swift
//  seebzStore
//
//  Created by IOS on 03/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

class UserLogin {
    var serverData : [String: Any] = [:]
    var user : [User]?
    var message : String?
    var success : Int?
    init(dict: [String: Any]){
        
        self.serverData = dict
        
        if let success = dict["success"] as? Int {
            self.success = success
        }
        if let message = dict["message"] as? String {
            self.message = message
        }
        if let body = dict["body"] as? [[String : Any]] {
            self.user = []
            for object in body {
                let some =  User(dict: object)
                self.user?.append(some)
            }
        }
    }
    
    class User {
        var serverData: [String: Any] = [:]
        var user_id: Int?
        var first_name: String?
        var last_name: String?
        var email: String?
        var mobile: Int?
        var password:String?
        var country_code: String?
        var dob: String?
        var gender: String?
        var description:String?
        var language: String?
        var device_type: String?
        var device_token: String?
        var created_at: String?
        var user_status: String?
        var notificaion_alert: Int?
        
        init(dict: [String: Any]){
            self.serverData = dict
            
            if let device_type = dict["device_type"] as? String {
                self.device_type = device_type
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let first_name = dict["first_name"] as? String {
                self.first_name = first_name
            }
            if let last_name = dict["last_name"] as? String {
                self.last_name = last_name
            }
            if let email = dict["email"] as? String {
                self.email = email
            }
            if let mobile = dict["mobile"] as? Int {
                self.mobile = mobile
            }
            if let gender = dict["gender"] as? String {
                self.gender = gender
            }
            if let password = dict["password"] as? String {
                self.password = password
            }
            if let country_code = dict["country_code"] as? String {
                self.country_code = country_code
            }
            if let device_token = dict["device_token"] as? String {
                self.device_token = device_token
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let mobile = dict["mobile"] as? Int {
                self.mobile = mobile
            }
            if let notificaion_alert = dict["notificaion_alert"] as? Int {
                self.notificaion_alert = notificaion_alert
            }
            if let description = dict["description"] as? String {
                self.description = description
            }
            if let language = dict["language"] as? String {
                self.language = language
            }
            if let user_id = dict["user_id"] as? Int {
                self.user_id = user_id
            }
            if let last_name = dict["last_name"] as? String {
                self.last_name = last_name
            }
            if let device_token = dict["device_token"] as? String {
                self.device_token = device_token
            }
            if let first_name = dict["first_name"] as? String {
                self.first_name = first_name
            }
            if let created_at = dict["created_at"] as? String {
                self.created_at = created_at
            }
            if let dob = dict["dob"] as? String {
                self.dob = dob
            }
            if let user_status = dict["user_status"] as? String {
                self.user_status = user_status
                
            }
        }
    }
}

