//
//  LeoDates.swift
//  BurnLive
//
//  Created by Office on 02/07/19.
//  Copyright © 2019 MandeepsinghBaath. All rights reserved.
//

import Foundation

class  LeoDates {
    var suggestedFormates : [String] =  ["yyyy-MM-dd hh:mm:ss", "yyyy-MM-dd HH:mm:ss z" ,
                                         "dd-MM-yyyy hh:mm:ss" ,
                                         "yyyy-MM-dd" , "yyyy-MM-dd'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "hh:mm:ss"]
    static let share = LeoDates()
    
    func addFormat(_ format :String) {
        LeoDates.share.suggestedFormates.append(format)
    }
}

extension String {
    
    func leoDate(toFormat : String? = nil  ) -> Date {
        for object in LeoDates.share.suggestedFormates {
            let formatter1 = DateFormatter()
            formatter1.timeZone = TimeZone(abbreviation: "UTC")
            formatter1.dateFormat = object
            if let dd1StartDate = formatter1.date(from: self){
                if toFormat == nil {
                    return dd1StartDate
                    
                } else {
                    formatter1.dateFormat = toFormat
                    let some = formatter1.string(from: dd1StartDate)
                    formatter1.dateFormat = toFormat
                    return formatter1.date(from: some)!
                }
            }
        }
        print("#warning : Not a proper Date")
        return Date()
    }
    
    func leoDateString(toFormat : String? = nil  ) -> String {
        for object in LeoDates.share.suggestedFormates {
            let formatter1 = DateFormatter()
            formatter1.timeZone = TimeZone(abbreviation: "UTC")
            formatter1.dateFormat = object
            
            if let dd1StartDate = formatter1.date(from: self){
                
                if toFormat == nil {
                    return "\(dd1StartDate)"
                } else {
                    
                    formatter1.dateFormat = toFormat
                    
                    let some = formatter1.string(from: dd1StartDate)
                    
                    
                    return some
                }
            }
        }
        print("#warning : Not a proper Date")
        return "\(Date())"
    }
    
    func leoTimeStringWithLocale(toFormat : String? = nil) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "HH:mm:ss"
        let date : Date? = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: date!)
    }
    
    func leoTimeString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        if let dateOk = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = format
            let dateStringOk = dateFormatter.string(from: dateOk)
            return dateStringOk
        }
        return ""
    }
    
    func convertUTCDateToLocal(format: String) -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = TimeZone.init(abbreviation: "UTC")
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
        
        let dateDate = dateFormatter.date(from: self)
        
        dateFormatter.timeZone = TimeZone.current
        
        dateFormatter.dateFormat = format
        
        if let dateD = dateDate {
            
            return dateFormatter.string(from: dateD)
            
        }
        
        return ""
        
    }
    
    
}

extension Date {
    
    
    func leoDate(toFormat : String? = nil  ) -> Date {
        if toFormat == nil {
            return self
        }
        let formatter1 = DateFormatter()
        
        formatter1.timeZone = TimeZone(abbreviation: "UTC")
        
        formatter1.dateFormat = toFormat
        
        let some = formatter1.string(from: self)
        
        formatter1.dateFormat = toFormat
        if let dd1StartDate = formatter1.date(from: some){
            return dd1StartDate
        }
        print("#warning : Not a proper Date")
        return self
    }
    
    
    func leoDateString(toFormat : String? = nil  ) -> String {
        if toFormat == nil {
            return "\(self)"
        }
        
        let formatter1 = DateFormatter()
        formatter1.timeZone = TimeZone(abbreviation: "UTC")
        formatter1.dateFormat = toFormat
        let some = formatter1.string(from: self)
        return some
    }
    
    
    
    func leoTimeString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let dateString = dateFormatter.string(from: self)
        let dateOk = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "HH:mm:ss"
        let dateStringOk = dateFormatter.string(from: dateOk!)
        return dateStringOk
    }
}

extension  LeoDates {
    enum Format {
        
        case mmDDYYYYYhhmma
        
        case yyyyMMddHHmmSSZ
        
        case yyyyMMddHHmmSS
        // "2017-11-08 10:18:03"
        
        case hhmma
        
        case mmDDYYYYY
        
        case ddMMYYYYYhhmma
        
        case yyyyMMdd
        
        var name: String {
            
            switch self {
                
            case .mmDDYYYYYhhmma: return "MM-dd-yyyy hh:mm a"
            case .yyyyMMddHHmmSS: return "yyyy-MM-dd HH:mm:ss "
                
                // "2017-11-08 10:18:03"
                
            case .mmDDYYYYY: return "MM-dd-yyyy"
                
            case .yyyyMMdd : return "yyyy-MM-dd"
                
            case .ddMMYYYYYhhmma: return "dd-MM-yyyy hh:mm a"
                
            case .yyyyMMddHHmmSSZ: return "yyyy-MM-dd HH:mm:ss Z"
                
                // "2019-03-30T06:21:28.000Z  ->   "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                
            // case . yyyy-MM-dd'T'HH:mm:ssZ return "yyyy-MM-dd'T'HH:mm:ssZ"
            case .hhmma: return "hh:mm a"
            }
        }
    }
}

extension Date {
    
    func dayDifference( format : String? = LeoDates.Format.yyyyMMddHHmmSSZ.name , to : String? = LeoDates.Format.mmDDYYYYY.name ) -> String {
        
        let calendar = NSCalendar.current
        
        if calendar.isDateInYesterday(self) { return "Yesterday" }
            
        else if calendar.isDateInToday(self) { return "Today" }
            
        else if calendar.isDateInTomorrow(self) { return "Tomorrow" }
            
        else {
            
            return    "\(self)".dateToDate(from: format!, to: to!)
            
        }
    }
    
    func smartFormat(fromDate : Date? = Date()) -> String {
        
        
        return self.timeIntervalSinceNow.smartFormat()
    }
}

extension Double {
    
    private func secondsToHoursMinutesSeconds () -> (Int?, Int?, Int?) {
        
        let hrs = self / 3600
        
        let mins = (self.truncatingRemainder(dividingBy: 3600)) / 60
        
        let seconds = (self.truncatingRemainder(dividingBy: 3600)).truncatingRemainder(dividingBy: 60)
        
        return (Int(hrs) > 0 ? Int(hrs) : nil, Int(mins) > 0 ? Int(mins) : nil, Int(seconds) > 0 ? Int(seconds) : nil)
    }
    
    func smartFormat () -> String {
        
        let time = self.secondsToHoursMinutesSeconds()
        
        switch time {
            
        case (nil, let x?, let y?):
            
            return "\(x) min \(y) sec"
            
        case (nil, let x?, nil):
            
            return "\(x) min"
            
        case (let x?, nil, nil):
            
            return "\(x) hr"
            
        case (nil, nil, let x?):
            
            return "\(x) sec"
            
        case (let x?, nil, let z?):
            
            return "\(x) hr \(z) sec"
            
        case (let x?, let y?, nil):
            
            return "\(x) hr \(y) min"
            
        case (let x?, let y?, let z?):
            
            return "\(x) hr \(y) min \(z) sec"
            
        default:
            
            return "\(self)"
        }
    }
}

extension String {
    
    func dateToDate(from: String, to: String) -> String {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = from
        
        dateFormatter.timeZone = NSTimeZone.local
        
        guard let date = dateFormatter.date(from: self) else {
            
            assert(false, "no date from string")
            
            return ""
            
        }
        dateFormatter.dateFormat = to
        
        dateFormatter.timeZone = NSTimeZone.local
        
        let timeStamp = dateFormatter.string(from: date)
        
        return timeStamp
    }
    
    func date(format: String) -> Date? {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        
        formatter.timeZone = NSTimeZone.local
        
        return formatter .date(from: self)
    }
    
    func stringtoDate(format: String) -> Date? {
        
        let formatter = DateFormatter()
        
        formatter.dateFormat = format
        
        formatter.timeZone = NSTimeZone.local
        
        return formatter .date(from: self)
    }
    
    
}
