//
//  TabButtons.swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import  UIKit

class TapButtons: UIButton{
    
    //Ui object
    @IBInspectable var identifierVC: String = ""
    var btnBackgroundView: UIView?
    var profileImg: UIImageView?
    var profileLbl: UILabel?
    
    @IBOutlet weak var btnView: UIView?
    @IBOutlet weak var btImg: UIImageView?
    @IBOutlet weak var btnLbl: UILabel?
    
    
    override var isSelected: Bool {
        
        didSet {
            if isSelected {
                self.btnView?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                //  self.btnView?.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
                
                self.btnLbl?.textColor = #colorLiteral(red: 0, green: 0.8069750667, blue: 0.8050286174, alpha: 1)
                self.btImg?.image? = (self.btImg?.image?.imageWithColor(color: #colorLiteral(red: 0, green: 0.8069750667, blue: 0.8050286174, alpha: 1)))!
            }
            else {
                self.btnView?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
                self.btnLbl?.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
                self.btImg?.image? = (self.btImg?.image?.imageWithColor(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)))!
            }
        }
    }
    
}

