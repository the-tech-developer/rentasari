//
//  Api .swift
//  seebzStore
//
//  Created by MAC on 13/12/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import Foundation
import UIKit

let baseUrl = "http://rentasari.staging.co.in:4019/"
let imageBaseUrl  = "http://rentasari.staging.co.in"

extension Api {
    
    func baseURl() -> String {
     return baseUrl + self.rawValued()
    
    }
}

enum Api{
   
    case signup
    case login
    case about_me
    case add_address
    case Create_Add
    case Category_Listing
    case AddNewCard
    case CardListing
    case MyAddress
    case AddLocation
    case home_screen
    case addFavroite
    case favroiteList
    case MYads
    case Search_home_screenAds
    case edit_Create_Add
    case Reset_password
    case disable_OR_publish_MyAds
    case forgot_password
    case edit_address
    case delete_card
    case delete_address
    case home_screen_product_details
    case Create_offer
    case Delete_Create_Add
    case notification
    case notification_Product_Detail
    case see_Profile_Of_Purchasing_User
    case user_Purchase_Product_Requset_Accepet_Or_Reject
    case edit_card
    
    func rawValued() -> String {
        switch self {
      
        case .signup:
            return "signup"
        case .login:
            return "login"
        case .about_me:
            return "about_me"
        case .add_address:
            return "add_address"
        case .Create_Add:
            return "Create_Add"
        case .Category_Listing:
            return "Category_Listing"
        case .AddNewCard:
            return "AddNewCard"
        case .CardListing:
            return "CardListing"
        case .MyAddress:
            return "MyAddress"
        case .AddLocation:
            return "AddLocation"
        case .home_screen:
            return "home_screen"
        case .addFavroite :
            return "addFavroite"
        case .favroiteList:
            return "favroiteList"
        case .MYads:
            return "MYads"
        case .Search_home_screenAds:
            return "Search_home_screenAds"
        case .edit_Create_Add:
            return "edit_Create_Add"
        case .Reset_password:
            return "Reset_password"
        case .disable_OR_publish_MyAds:
            return "disable_OR_publish_MyAds"
        case .forgot_password:
            return "forgot_password"
        case .edit_address:
            return "edit_address"
        case .delete_card:
            return "delete_card"
        case .delete_address:
            return "delete_address"
        case .home_screen_product_details:
            return "home_screen_product_details"
        case .Create_offer:
            return "Create_offer"
        case .Delete_Create_Add:
            return "Delete_Create_Add"
        case .notification:
            return "notification"
        case .notification_Product_Detail:
            return "notification_Product_Detail"
        case .see_Profile_Of_Purchasing_User:
            return "see_Profile_Of_Purchasing_User"
        case .user_Purchase_Product_Requset_Accepet_Or_Reject:
            return "user_Purchase_Product_Requset_Accepet_Or_Reject"
        case .edit_card:
               return "edit_card"

        }
    }
}


func isSuccess(json : [String : Any] , _ success : Int? = 200) -> Bool{
    
    if let isSucess = json["success"] as? Int {
        if isSucess == success{
            return true
        }
    }
    return false
}


func message(json : [String : Any]) -> String{
    if let isSucess = json["success"] as? Int {
        if isSucess == 400{
            return json["message"] as! String
        }
    }
    if let message = json["error_msg"] as? String {
        return message
    }
    return ""
}
