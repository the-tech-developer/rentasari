//
//  AppDelegate.swift
//  seebzStore
//
//  Created by IOS on 08/11/19.
//  Copyright © 2019 IOS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // SET IQKEYBOARDMANAGER   print("\u{1F97A}")
        IQKeyboardManager.shared.enable = true
        // SET GOOGLE MAPS KEY
        GMSServices.provideAPIKey(GoogleMapKey.kApiKey)
        GMSPlacesClient.provideAPIKey(GoogleMapKey.kApiKey)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }
}



//FedEx Params


/*

// MARK: - CreateLabel
struct CreateLabel: Codable {
    let shipment: Shipment?
}

// MARK: - Shipment
struct Shipment: Codable {
    let serviceCode: String?
    let shipTo, shipFrom: Ship?
    let packages: [Package]?

    enum CodingKeys: String, CodingKey {
        case serviceCode = "service_code"
        case shipTo = "ship_to"
        case shipFrom = "ship_from"
        case packages
    }
}

// MARK: - Package
struct Package: Codable {
    let weight: Weight?
}

// MARK: - Weight
struct Weight: Codable {
    let value: Int?
    let unit: String?
}

// MARK: - Ship
struct Ship: Codable {
    let name, phone, companyName, addressLine1: String?
    let addressLine2, cityLocality, stateProvince, postalCode: String?
    let countryCode, addressResidentialIndicator: String?

    enum CodingKeys: String, CodingKey {
        case name, phone
        case companyName = "company_name"
        case addressLine1 = "address_line1"
        case addressLine2 = "address_line2"
        case cityLocality = "city_locality"
        case stateProvince = "state_province"
        case postalCode = "postal_code"
        case countryCode = "country_code"
        case addressResidentialIndicator = "address_residential_indicator"
    }
}
 
 // MARK: - RateOptions
 struct RateOptions: Codable {
     let shipmentID: String?
     let rateOptions: RateOptionsClass?

     enum CodingKeys: String, CodingKey {
         case shipmentID = "shipment_id"
         case rateOptions = "rate_options"
     }
 }

 // MARK: - RateOptionsClass
 struct RateOptionsClass: Codable {
     let carrierIDS: [String]?

     enum CodingKeys: String, CodingKey {
         case carrierIDS = "carrier_ids"
     }
 }

 
 // MARK: - RateOptions
 struct RateOptions: Codable {
     let shipmentID: String?
     let rateOptions: RateOptionsClass?

     enum CodingKeys: String, CodingKey {
         case shipmentID = "shipment_id"
         case rateOptions = "rate_options"
     }
 }

 // MARK: - RateOptionsClass
 struct RateOptionsClass: Codable {
     let carrierIDS: [String]?
     let serviceCodes: [JSONAny]?
     let packageTypes: [String]?

     enum CodingKeys: String, CodingKey {
         case carrierIDS = "carrier_ids"
         case serviceCodes = "service_codes"
         case packageTypes = "package_types"
     }
 }

 */
